#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 12:17:32 2018

@author: franciscor
"""
#import os

import pandas as pd
from influxdb import DataFrameClient, InfluxDBClient



## connection to influx
HOST = '10.8.0.5'
PORT = 8086
DBNAME = 'default_test'

# Constant
MAX_LINES_UPDATE = 10000

def dataframe_connection(host=HOST, port=PORT, dbname=DBNAME, user='', password=''):
    client = DataFrameClient(host=host,
                             port=port,
                             username=user,
                             password=password,
                             database=dbname)
    return client

def connection(host=HOST, port=PORT, dbname=DBNAME, user='', password=''):
    client = InfluxDBClient(host=host,
                            port=port,
                            username=user,
                            database=dbname,
                            password=password)
    return client

def _check_time(_time):
    '''
    init default: not a dictionary -> nothing

    dictionary:
        time: now
        unit: hours
        shift: 1
    '''
    if isinstance(_time, str) == True:
        _time = {'time': _time,
                 'unit': None,
                 'amount': None}

    if isinstance(_time, dict) == False:
        _time = {'time': None,
                 'unit': None,
                 'amount': None}
    if 'time' not in _time.keys():
        _time = {'time': None,
                 'unit': None,
                 'amount': None}
    if _time['time'] not in [None, 'now()']:
        _time['time'] = "'%s'" % _time['time']
    # if any of the fields is not present: set them both to empty
    if ('unit' not in _time.keys()) or ('amount' not in _time.keys()):
        _time['unit'] = None
        _time['amount'] = None
    else:

        if _time['unit'] not in ['w', 'd', 'h', 'm', 's', None]:
            # default if error
            _time['unit'] = 'h'
            print ('unit time is wrong. Set default to hour')
        # check if amount is an integer larger than zero
        if _time['amount'] != None:
            try:
                _int = int(_time['amount'])
                if _int <= 0:
                    print('amount is a negative integer number')
                    _time['amount'] = None
            except:
                print('amount is not an integer')
                _time['amount'] = None
    return _time

def _get_period_string(_init_period, _end_period):
    '''
    write something in here!
    '''
    # _ini is updated according to check_ini default parameters
    _ini = _check_time(_init_period)
    # _end is updated according to check_ini default parameters
    _end = _check_time(_end_period)

    period_string = ''
    # if it is empty, just do nothing
    if (_ini['time'] != None):
        period_string = '%s%s ' % (period_string, _ini['time'])
        # if both fields are right, append them, otherwise do nothing
        if (_ini['amount'] != None) & (_ini['unit'] != None):
            period_string = '%s- %s%s ' % (period_string,
                                           _ini['amount'],
                                           _ini['unit'])
    if period_string != '':
        period_string = '%s< time AND ' %(period_string)

    # if it is empty, just do nothing
    if (_end['time'] != None):
        period_string = '%stime < %s ' % (period_string, _end['time'])
        # if both fields are right, append them, otherwise do nothing
        if (_end['amount'] != None) & (_end['unit'] != None):
            period_string = '%s- %s%s ' % (period_string,
                                           _end['amount'],
                                           _end['unit'])
    if (period_string != '') and (period_string[-4:]!='AND '):
        period_string = '%sAND ' %(period_string)
    return period_string

def get_sensors(_con,table='rawData'):
    '''tags in measurement'''
    _raw_data = _con.query('''SHOW TAG VALUES FROM "%s" WITH KEY = "device"''' % table).raw
    _data = _raw_data['series'][0]
    _df = pd.DataFrame(_data['values'],columns=_data['columns'])
    _df.rename(columns={'value':'device'},inplace=True)
    return _df.drop(['key'],axis=1)

def get_measures(_con,table='rawData'):
    ''' get different measures in BettAir database'''
    _raw_data = _con.query("SHOW MEASUREMENTS").raw['series'][0]
    return [_x[0].encode() for _x in _raw_data['values']]

def get_device(_con,_device, measure='rawData', ini_per=None, end_per=None, limit=100):
    '''
    _init = {'time': a timestamp or None
             'unit': days (d) or hours (h)
             'amount': and int}
    _end = {'time': a timestamp or None
             'unit': days (d) or hours (h)
             'amount': and int}
    '''
    period_string = _get_period_string(ini_per, end_per)
    _rs = _con.query('''SELECT * from "%s" WHERE %s"device"='%s' LIMIT %d''' % (measure,
                                                                                period_string,
                                                                                _device,
                                                                                limit))
    return _rs
# %%
def get_ini_end_influx_device(_con, device):
    query = '''SELECT * FROM "BettAir"."autogen"."rawData" WHERE "device"='%s' ORDER BY time DESC LIMIT 1''' % device
    _rs = _con.query(query)
    for _name, _last in zip(_rs.raw['series'][0]['columns'], _rs.raw['series'][0]['values'][0]):
        if 'time' == _name:
            break

    query = '''SELECT * FROM "BettAir"."autogen"."rawData" WHERE "device"='%s' ORDER BY time ASC LIMIT 1''' % device
    _rs = _con.query(query)
    for _name, _first in zip(_rs.raw['series'][0]['columns'], _rs.raw['series'][0]['values'][0]):
        if 'time' == _name:
            break
    return (_first, _last)
# %%
def get_tables_and_devices(_con):
    '''
    '''
    query1 = _con.get_list_series()[0]['tags']
    _list_tables = []
    _list_devices = []
    for _idx, item in enumerate(query1):
        _table = item['key'].split(',')[0]
        if _table not in _list_tables:
            _list_tables.append(_table)
        _dev_id = item['key'].split(',')[1].split('=')
        if _dev_id not in _list_devices:
            _list_devices.append(_dev_id[1])
    return _list_tables, _list_devices

def get_devices_from_measure(_con, db='BettAir', measure='rawData', tag='device'):
    _data = _con.query('SHOW TAG VALUES ON "%s" FROM "%s" WITH KEY = "%s"' % (db,
                                                                              measure,
                                                                              tag))
    return [_x[1] for _x in _data.raw['series'][0]['values']]
def get_timeposition(_colums):
    for _idx, _field in enumerate(_colums):
        if _field == 'time':
            break
    return _idx
# %%
def convertQuery2DataFrame(_rs, main_tags=[]):
    '''
    Convert the data result of an influx  query to a pandas dataframe
    ----
     18-2-19: Removes rows with duplicated time indexes.
        Since we don't know which of the rows is the correct one,
        we arbitrarily keep the last one
        (assuming that it comes from the latest update)
        It tags the status in the kept DF rows as 'DUPLICATE_TIME'
        to be able to detect the issue externally.
    3-2-2020: Adds main_tags in order to accept columns with different tags
     as valid duplicate time indexes
    '''
    df = pd.DataFrame(_rs.get_points())
    if not df.empty:
        col_subset = [xx for xx in main_tags if xx in df.keys()]
        if 'time' not in df.keys():
            print('Error: time not found in influx db.'
                  + 'Label has possibly changed')
        else:
            col_subset += ['time']
            df.loc[df.duplicated(subset=col_subset), 'STATUS'] = 'DUPLICATE_TIME'
            df = df[~df.duplicated(subset=col_subset, keep='last')]
            df.set_index('time', inplace=True)
            df.index = pd.to_datetime(df.index, utc=True)
    return df

#def writeDataframe(_con,df):
#

# %%
#df = {}
#for _dev in list_devices:
#    _rs = get_device(rawData.con, _dev,ini_per='2018-06-15T17:33:04Z', limit=10000)
#    df[_dev] = convertQuery2DataFrame(_rs)
# %%

