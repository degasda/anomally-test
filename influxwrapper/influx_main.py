#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 12:38:56 2018
H5_updater
@author: franciscor
"""
# inherit methods
# panda HDFStore object
#from data_connect.hdfs_config import FILESTORE
# influx connection object
from influx_tools import connection
#constant
from influx_tools import MAX_LINES_UPDATE
# methods
from influx_tools import get_device, convertQuery2DataFrame
from influx_tools import get_ini_end_influx_device, get_devices_from_measure

import pandas as pd
import numpy as np

def get_device_table_dict(_fStorage):
    _dict_tmp = {}
    _data_tab_list = [_x for _x in _fStorage.keys() if _x.split('/')[-1] == 'sensdata']
    for _tab in _data_tab_list:
        _tmp = _tab.split('/')[2]
        if _tmp != 'reference':
            _device_id = _tmp.split('_')[-1][-4:]
            _dict_tmp[_device_id] = _tab
    return _dict_tmp

def get_device_sector_dict(_fStorage):
    '''
    generates a dictionary: each key (sector id) is a list where the id's of the
    devices in the sector are enclosed
    '''
    _dict_tmp = {}
    _data_tab_list = [_x for _x in _fStorage.keys() if _x.split('/')[-1] == 'sensdata']
    for _tab in _data_tab_list:
        if _tab.split('/')[2] != 'reference':
            _sector_long_id = _tab.split('/')[1]
            _sector_code = _sector_long_id.split('_')[2]
            _init_string = 't_id_%s_' % _sector_code
            _sector_id = _sector_long_id.split(_init_string)[1]
            _device_id = _tab.split('/')[2].split('_')[-1][-4:]
            if _sector_id not in _dict_tmp.keys():
                _dict_tmp[_sector_id] = [_device_id]
            else:
                _dict_tmp[_sector_id].append(_device_id)
    return _dict_tmp

def get_sensor_sector_dict(_fStorage):
    '''
    generates a dictionary: each key (sector id) is a list where the id's of the
    devices in the sector are enclosed
    '''
    _dict_tmp = {}
    _data_tab_list = [_x for _x in _fStorage.keys() if _x.split('/')[-1] == 'sensdata']
    for _tab in _data_tab_list:
        if _tab.split('/')[2] != 'reference':
            _sector_long_id = _tab.split('/')[1]
            _sector_code = _sector_long_id.split('_')[2]
            _init_string = 't_id_%s_' % _sector_code
            _sector_id = _sector_long_id.split(_init_string)[1]
            _device_id = _tab.split('/')[2].split('_')[-1]
            if _sector_id not in _dict_tmp.keys():
                _dict_tmp[_sector_id] = [_device_id]
            else:
                _dict_tmp[_sector_id].append(_device_id)
    return _dict_tmp

# %%
def get_data_from_dt(_dateTime):
    return str(_dateTime).split('+')[0]

# %%

class update_h5_from_influx(object):
    '''
    class to update automatically h5 file from influx
    fileStorage: HDFStorage object from pandas
    influx_con: InfluxDBClient object from influxdb
    '''
    def __init__(self, fileStorage=None, influx_con=connection(), measure='rawData'):
        self.con = influx_con
        self.fileStore = fileStorage
        self.device_tables = get_device_table_dict(fileStorage)
        self.sector_device = get_device_sector_dict(fileStorage)
        self.influx_measure = measure
        self.get_device_table_info()

    def _get_dev_last_line(self,_device_id):
        '''
        return last index of the table in .h5 storage
        '''
        _dev_table = self.device_tables[_device_id]
        return get_data_from_dt(self.fileStore[_dev_table].index[-1])

    def _append_to_dev_table(self, _df2append, _device_id):
        _table = self.device_tables[_device_id]
        #try:
        #_columns_rename = {_x: _x.encode() for _x in _df2append.keys().tolist()}
        #_df2append.rename(columns=_columns_rename, inplace=True)
        self.fileStore.append(_table, _df2append)
        _flag = True
        #except:
        #    _flag = False
        return _flag
    def get_ini_end_influx(self, device):
        return get_ini_end_influx_device(self.con, device)

    def update_device(self,_device_id, lines=MAX_LINES_UPDATE,
                      dt_ini='2018-06-11 14:00:25+0000', insert=True):
        '''
        insert all missing data in dev table (datetime to now()) of
         the _device_id
        returns:
            True - > inserted data
            False -> No data inserted
        -------------------------------------------
        Input:  - _device: device identifier (string)
                -
                -
        Output: -
                -
                -
        '''
        # get last datetime in .h5
        try:
            _dateString = self._get_dev_last_line(_device_id)
        except:
            _dateString, _ = self.get_ini_end_influx(_device_id)
        _active_flag = True
        _init = True
        while _active_flag:
            # get data from influx db
            print(_dateString)
            _rs = get_device(self.con, _device_id,
                             ini_per=_dateString,
                             limit=MAX_LINES_UPDATE)
            # check that there is data
            _active_flag = 'series' in _rs.raw.keys()
            if _active_flag == True:
                # convert rs into a dataframe
                df_tmp = convertQuery2DataFrame(_rs)
                _dateString = get_data_from_dt(df_tmp.index[-1])
                #store it in tmp
                if _init == True:
                    _df = df_tmp
                    _init = False
                else:
                    _df = _df.append(df_tmp)
        # insert into .h5 table the requested data
        #remove unused fields of _df
        #_df.drop(columns=['device','time'], inplace=True)
        _mask = (_df.index > pd.to_datetime(dt_ini, utc=True))
        _df = _df[_mask]
        _df.drop(columns=['device','time'], inplace=True)
        if insert == True:
            self.add_device_table_info(_device_id, _df.head(n=200))
            #try:
            #reorder columns of _df according to table order
            _df = _df[self.info_table['columns'][_device_id]]
            #TODO set _df data type to match the ones of the table
            cols2fill = []
            for _col, _type in self.info_table['dtypes'][_device_id].iteritems():
                if _type != _df.dtypes[_col]:
                    cols2fill.append(_col)
            #_df[self.info_table['null_cols'][_device_id]] = np.nan
            _df[cols2fill] = np.nan
            #except:
            #    return False, _df.head(n=200)
            print('Appending stuff to table:')
            print(self.device_tables[_device_id])
            try:
                _flag = self._append_to_dev_table(_df, _device_id)
                print('stuff was appended')
            except:
                _flag = False
        else:
            _flag = False
        return _flag, _df

    def update_sector(self,_sector_id, lines=MAX_LINES_UPDATE):
        '''
        Inserts all missing data of any device table of the pre-defined sector
        '''
        _output_info = {}
        for _dev_id in self.sector_device[_sector_id]:
            _output_info[_dev_id] = {}
            try:
                _flag,_ = self.update_device(_dev_id)
                _output_info[_dev_id] = 'updated' if _flag == True else 'non updated'
            except:
                _output_info[_dev_id] = 'non updated'
        return _output_info

    def end_update(self):
        '''
        close the .h5 file
        '''
        self.fileStore.close()

    def get_ini_end_devices(self):
        '''
        Get initial and end datetime index from each sensdata table in the
        filestore
        -----------------------------------------------------
        Input:  - itself
                -
        Output: - A dataframe with as many rows as devices and two columns
                -
        '''
        dict_index = {'begin':{}, 'end':{}}
        for device in self.device_tables.keys():
            table = self.device_tables[device]
            dict_index['begin'][device] = self.fileStore[table].index[0]
            dict_index['end'][device] = self.fileStore[table].index[-1]
            print(device)
        return pd.DataFrame(dict_index)

    def _get_column_order(self, _table):
        return {_idx: _field for _idx, _field in enumerate(self.fileStore[_table])}

    def get_device_table_info(self):
        '''
        get information about the tables in datastore!
        -------------------------------------------
        Input:  -
                -
        Output: -
                -
        '''
        #TODO get this information from metadata!
        # this function must get information from metadata files!
        # get nan columns of .h5
        self.info_table = {'null_cols': {},
                           'columns': {},
                           'dtypes': {}}
        for _device in self.device_tables.keys():
            _tab = self.device_tables[_device]
            if _tab in self.fileStore.keys():
                _df = self.fileStore[_tab].head(n=200)
                #_df = _df[_df.index== _df.index[0]]
                self.info_table['null_cols'][_device] = []
                #for _field, _flag in _df.isnull().iteritems():
                #    if _flag.tolist()[0] == True:
                for _field in _df.keys():
                    if _df.dtypes[_field].type != np.float64:
                        self.info_table['null_cols'][_device].append(_field)
                self.info_table['columns'][_device] = _df.columns.tolist()
                self.info_table['dtypes'][_device] = _df.dtypes

    def add_device_table_info(self, _device, _df):
        if _device not in self.info_table['null_cols']:
            self.info_table['null_cols'][_device] = []
            #_df = _df[_df.index== _df.index[0]]
            for _field in _df.keys():
                if _df.dtypes[_field].type != np.float64:
                    self.info_table['null_cols'][_device].append(_field)
            self.info_table['columns'][_device] = _df.columns.tolist()
            self.info_table['dtypes'][_device] = _df.dtypes

    def update(self):
        '''
        get information about the tables in datastore!
        -------------------------------------------
        Input:  -
                -
        Output: -
                -
        '''
        _results = {}
        for _sector in self.sector_device.keys():
            _results[_sector] = self.update_sector(_sector)
        return _results

    def update_devices(self ,dev_list={}):
        _output_info = {}
        for _dev_id in dev_list.keys():
            _output_info[_dev_id] = {}
            if _dev_id not in self.device_tables.keys():
                _tab = '/t_id_000000_%s/s_mac_%s' % (dev_list[_dev_id],
                                                     _dev_id)
                self.device_tables[_dev_id] = '%s/raw/sensdata' % _tab
            try:
                _flag, _ = self.update_device(_dev_id)
                _output_info[_dev_id] = 'updated' if _flag == True else 'non updated'
            except:
                _output_info[_dev_id] = 'non updated'
            print('%s: %s' % (_dev_id ,_output_info[_dev_id]))
        return _output_info

    def get_device_influx(self):
        '''
        get information about the tables in datastore!
        -------------------------------------------
        Input:  -
                -
        Output: -
                -
        '''
        return get_devices_from_measure(self.con)

    def check_device_table(self ,dev_list={}):
        '''
        get information about the tables in datastore!
        -------------------------------------------
        Input:  -
                -
        Output: -
                -
        '''
        for _dev_id in dev_list.keys():
            if _dev_id not in self.device_tables.keys():
                _tab = '/t_id_000000_%s/s_mac_%s' % (dev_list[_dev_id],
                                                     _dev_id)
                self.device_tables[_dev_id] = '%s/raw/sensdata' % _tab

#############################################################################
#############################################################################
##                                                                         ##
#############################################################################
#############################################################################

class FileStoreConversor(object):
    '''
    adapts tables from old format to new format
    '''
    def __init__(self, originalPath, destinationPath):
        self.keys = {'ae_sn1': 'NO2_ae',
                     'ae_sn2': 'O3_ae',
                     'ae_sn3': 'NO_ae',
                     'ae_sn4': 'sn4_ae',
                     'device': 'none',
                     'lat': 'none',
                     'lon': 'none',
                     'noisel': 'none',
                     'noiser': 'none',
                     'pm1': 'pm1',
                     'pm10': 'pm10',
                     'pm2': 'pm2_5',
                     'pressure': 'none',
                     'relative_humidity': 'humidity',
                     'temperature0': 'temperature',
                     'temperature1': 'none',
                     'time': 'none',
                     'uv_index': 'none',
                     'we_sn1': 'NO2',
                     'we_sn2': 'O3',
                     'we_sn3': 'NO',
                     'we_sn4': 'sn4_we'}
        self.old2drop = ['pt1000']
        self.oldStore = pd.HDFStore(originalPath,mode='r')
        self.newStore = pd.HDFStore(destinationPath,mode='w')
        self.keys2rm = ['time', 'device']

    def add_row(self, _idx, _row):
        for _key in self.dict_tmp.iterkeys():
            if self.keys[_key] == 'none':
                self.dict_tmp[_key][_idx] = np.nan
            elif self.keys[_key] in self._oldkeys:
                self.dict_tmp[_key][_idx] = _row[self.keys[_key]]
            else:
                self.dict_tmp[_key][_idx] = np.nan

    def convert_row_table(self, _table):
        _count = 0
        self._oldkeys = self.oldStore[_table].keys().tolist()
        for idx, row in self.oldStore[_table].iterrows():
            if _count % 10000 == 0:
                self.dict_tmp = {_x: {} for _x in self.keys.keys() if _x not in self.keys2rm}
            # add row to tmp dict
            self.add_row(idx, row)
            _count += 1
            if _count % 10000 == 0:
                # append the dataframe (1000 rows) to h5 table
                df = pd.DataFrame(self.dict_tmp)
                self.newStore.append('/%s' % _table, df)

    def convert_df_table(self, _table,mode='auto'):
        df = self.oldStore[_table]
        new_column_name = {}
        new_column2append = []
        for new_key in self.keys.keys():
            old_key = self.keys[new_key]
            if old_key != 'none':
                new_column_name[old_key] = new_key
            else:
                new_column2append.append(new_key)
        df.drop(columns=self.old2drop,inplace=True)
        # rename old columns as new format
        df.rename(columns=new_column_name, inplace=True)
        # add new keys filled with np.nan
        for _new_key in new_column2append:
            if _new_key not in self.keys2rm:
                df[_new_key] = np.nan
        for _label, _dtype in df.dtypes.iteritems():
            if _dtype != np.dtype(np.float64):
                df[_label] = np.nan
        # store new dataframe into new .h5
        if mode == 'auto':
            self.newStore.append(_table, df)
            return None
        else:
            return df

    def adapt_filestorage(self, dropOldTable=False):
        for table in self.oldStore.keys():
            split_table = table.split('/')
            if (split_table[-1] == 'metadata') or (split_table[2] == 'reference'):
                print('Processing table: %s' % table)
                self.newStore[table] = self.oldStore[table]
            if (split_table[-1] == 'sensdata') and (split_table[2] != 'reference'):
                print('Adapting table: %s' % table)
                self.convert_df_table(table)
            if dropOldTable ==True:
                self.oldStore.remove('/%s' % table)

    def close_filestorages(self):
        self.newStore.close()
        self.oldStore.close()
# %%
#if __name__ == '__main__':
#    DATA_PATH = '/media/franciscor/simtmp/francisco/data/h5/tmp2/'
#
#    old_fname = '%ssensor_data_storage_raw_old.h5' % DATA_PATH
#    new_fname = '%ssensor_data_storage_raw_new.h5' % DATA_PATH
#
#    mess_obj = fileStore_conversor(old_fname, new_fname)
#
#    mess_obj.adapt_filestorage()
#    mess_obj.close_filestorages()
#    #%%
#    dict_old = {}
#    for _tt in mess_obj.oldStore.keys():
#        dict_old[_tt] = mess_obj.oldStore[_tt].dtypes
#    #%%
#    mess_obj.newStore.open()
#    dict_new = {}
#    for _tt in mess_obj.newStore.keys():
#        dict_new[_tt] = mess_obj.newStore[_tt].dtypes
#    mess_obj.newStore.close()
#    mess_obj.oldStore.close()
#%%
if __name__ == '__main__':
    DATA_PATH = '/home/franciscor/proyectos/deployment_bettair/'
    fstore_path =  '%sdata_raw.h5' % DATA_PATH
    rawData = update_h5_from_influx(fileStorage=pd.HDFStore(fstore_path,mode='r+'))

# %%
#    res = {}
#    for tt in RawCon.keys():
#        if tt[-8:] =='sensdata':
#            if isinstance(RawCon[tt],pd.DataFrame) ==True:
#                res[tt] = (RawCon[tt].index[0], RawCon[tt].index[-1])
#            else:
#                RawCon.remove(tt)
#%%
#    device = '8472'
#    flag, df_output = getData.update_device(device)
#
#    df_index = getData.get_ini_end_devices()
#    # %%
#    ff = getData._append_to_dev_table(df_output, device)
#
#    # %%
#    table = getData.device_tables[device]
#    df2append = df_output.drop(columns=['device','time'])
#    column_order = {_idx: _field for _idx, _field in enumerate(getData.fileStore[table])}
#    inv_col_order = {_field:_idx for _idx in column_order.keys()}
#    #%%
#    for _idx in column_order.keys():
#        _field = column_order[_idx]
#        if _idx == 0:
#            df_tmp = pd.DataFrame(df2append[_field], columns=['%d' % _idx])
#        else:
#            df_tmp = pd.concat((df_tmp,pd.DataFrame(df2append[_field], columns=['%d' % _idx])),axis=1)