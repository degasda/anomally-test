#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 12:17:32 2018

Wrapper for  easy influx database management with python API

@author: Francisco Ramirez Javega

"""
#import os
import math
import pandas as pd
import numpy as np
from influxdb import DataFrameClient, InfluxDBClient
#
#from .influx_tools import _get_period_string, get_timeposition
from .influx_tools import convertQuery2DataFrame # _check_time

## connection to influx
HOST = '10.8.0.5'
PORT = 8086
DBNAME = 'default_database'
#
# Constant
#MAX_LINES_UPDATE = 10000
TAGS = ['DEVICE_ID']
def _measure4query(database, r_policy, measure):
    ''' Description: concatenates stuff for measurement descriptor in influx
        -----------------------------------------------------------------------
        Input:  - database: (str) database name
                - r_policy: (str) retention policy name
                - measure:  (str) measurement name
        -----------------------------------------------------------------------
        Output: - msg: (str) with time string for query
    '''
    msg = '''"%s"."%s"."%s"''' % (database, r_policy, measure)
    return msg
#%%
def remove_field_with_string(points_in,string='nan'):
    string = '=' + string
    points_out = []
    for x in points_in:
        [header, body, timestamp] = x.split(' ')
        body_x = [field for field in body.split(',') if string not in field]
        if len(body_x) > 0:
            x_new = ','.join(body_x)
            points_out.append(' '.join([header,x_new,timestamp]))
    return points_out


def _time4query(_init, _end):
    ''' Description: concatenates the timestamps for queyr
        -----------------------------------------------------------------------
        Input:  - _init: localized Timestamp object (pandas)
                - _end: localizated Timestamp object (pandas)
        -----------------------------------------------------------------------
        Output: - msg: (str) with time string for query
    '''
    msg = ''
    flag_ini = 0
    flag_end = 0
    if isinstance(_init, pd.Timestamp):
        flag_ini = 1
        i_str = _init.isoformat()
    else:
        i_str = None
    if isinstance(_end, pd.Timestamp):
        flag_end = 2
        e_str = _end.isoformat()
    else:
        e_str = None
    flag = flag_ini + flag_end
    msg = {0: '',
           1: "time >= '%s'" % (i_str),
           2: "time < '%s'" % (e_str),
           3: "time >= '%s' AND time < '%s'" % (i_str, e_str)}[flag]
    return msg
#%%
def _tags4query(tags):
    ''' Description: concatenates the tags for the query
        -----------------------------------------------------------------------
        Input:  - tags: (dictionary) of tag_key: tag_value
        -----------------------------------------------------------------------
        Output: - tag_string: (str) with the concatenation of tags for query
    '''
    tag_string = ''
    for ntag, (tag, value) in enumerate(tags.items()):
        if ntag > 0:
            tag_string += 'AND '
        tag_string += '''"%s"='%s' ''' %(tag,value)
    if len(tag_string)> 2:
        tag_string = tag_string[:-1]
    return tag_string
#
class InfluxObject(object):
    #TODO add description of methods and attributes
    ''' Description of the class
        -----------------------------------------------------------------------
        Descriptions of methods:
            - dataframe_connection
            - connection
            - switch_database
            - create_database
            - drop_database
            - drop_current_database
            - get_databases
            - get_measures
            - drop_measurement
            - get_tag_values
            - get_tag_keys
            -
            -


        -----------------------------------------------------------------------
        Descriptions of attributes:
            - host:     (str)
            - port:     (int)
            - username: (str)
            - password: (str)
            - database: (str)
            - retention_policy: (str)
            - _autocommit: (bool)
            - _commit_size: (int)
            - _batch_size:  (int) Only taken into account for batching
                            queued data
            - _tag_names:    (dict)
            - _queue: (dataframe)
    '''
    def __init__(self, host=HOST, port=PORT, username='', password='',
                 database=DBNAME, autocommit=True, batch_size=100,
                 commit_size=200, retention_policy='autogen', main_tags=TAGS):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.database = database
        self.w_con = self.dataframe_connection()
        self.connection = self._connection()
        self.retention_policy = retention_policy
        self._autocommit = autocommit
        self._commit_size = commit_size
        self._batch_size = np.min((batch_size, commit_size))
        self._main_tags = main_tags

    ##########################################################################
    ###
    ###                management methods
    ###
    ##########################################################################
    def dataframe_connection(self):
        '''
        dataframe writting connection
        -----------------------------------------------------------------------
        Output: - A DataFrameClient object
        '''
        return DataFrameClient(host=self.host,
                               port=self.port,
                               username=self.username,
                               password=self.password,
                               database=self.database)

    def _connection(self):
        '''
        dataframe reading and management connection
        -----------------------------------------------------------------------
        Output: - A InfluxDBClient object
        '''
        return InfluxDBClient(host=self.host,
                              port=self.port,
                              username=self.username,
                              password=self.password,
                              database=self.database)

    def switch_database(self, _database):
        '''
        changes the pointer to the database of the writter and reading connect
        -----------------------------------------------------------------------
        Input: - Database name (string)
        '''
        self.connection.switch_database(_database)
        self.w_con.switch_database(_database)
        self.database = _database

    def create_database(self, _database, switch=True):
        ''' Description:
            creates the database "_database"
        -----------------------------------------------------------------------
        Input:  - Database name (string)
                - Switch (boolean). True if the connections switch also,
                  False for connections remain pointing same place
        '''
        self.connection.create_database(_database)
        self.w_con.create_database(_database)
        if switch == True:
            self.switch_database(_database)

    def drop_database(self, _database):
        ''' Description:
            drop the database "_database"
        -----------------------------------------------------------------------
        Input:  - Database name (string)
        '''
        self.connection.drop_database(_database)

    def drop_current_database(self):
        ''' Description:
            drop the database "_database"
        -----------------------------------------------------------------------
        '''
        self.drop_database(self.database)

    def get_databases(self):
        ''' Description:
            get databases in HOST
        -----------------------------------------------------------------------
        Output: - A list with the databases in the host (list of strings)
        '''
        _tmp = self.connection.query('SHOW DATABASES')
        _tmp = _tmp.raw['series'][0]['values']
        return [_x[0] for _x in _tmp]

    def get_measures(self):
        ''' Description:
            get different measures in current database
        -----------------------------------------------------------------------
        Output: - A list with the measurement names in the database
        '''
        _raw_data = self.connection.query("SHOW MEASUREMENTS").raw['series'][0]
        return [_x[0] for _x in _raw_data['values']]

    def drop_measurement(self, measure):
        ''' Description:
            drop a measure from current database
        -----------------------------------------------------------------------
        Output: - A list with the measurement names in the database
        '''
        _query = 'DROP MEASUREMENT "%s"' % measure
        self.connection.query(_query)
        return None

    def get_tag_values(self, measure, tag_key, tags={}, last=False):
        ''' Description:
                Get the different values of the tag in tag_key.
                The method requests all tag_values of the tag_key when the
                tags dict is empty . Otherwise, the method returns the tag
                value of the last row of the table indicated by the
                tags dictionary.

        -----------------------------------------------------------------------

            Input:
                - *measurement*: (str)The measurement name
                - *tag_key*: (str) the tag name whose values would
                        be retrieved.
                - *tags*: (dict) {tag_key:tag_value}
                - *last*: (bool) True: get tags last row, False, get all tags

        -----------------------------------------------------------------------

            Output:
                - {tag_key: pd.DataFrame}
        '''
        if isinstance(tag_key, list) == False:
            tag_key = [tag_key]
        if (last ==True) & (len(tags) > 0):
            _, _end = self.get_ini_end_measure(measure, tags)
            _ini = _end - pd.Timedelta('10000 s')
            output = self.getFrameFromDevice(measure,
                                             tags,
                                             _ini=_ini, _end=_end)
            output = {_key: output[[_key]] for _key in output.keys()
                                                if _key in tag_key}
        else:
            results = {}
            for _tag_key in tag_key:
                _msg = 'SHOW TAG VALUES ON "%s" FROM ' % self.database
                _msg += '''"%s" WITH KEY = "%s"''' % (measure, _tag_key)
                for _c, (_key, _val) in enumerate(tags.items()):
                    if _c == 0:
                        _msg += ' WHERE'
                        if last == True:
                            _, _end = self.get_ini_end_measure(measure, tags)
                            _ini = _end - pd.Timedelta('0.01s')
                            time_msg = _time4query(_ini, None)
                            _msg += ' %s' % time_msg
                    if last == False & _c == 0:
                        _msg += ' "%s" = ' % _key
                    else:
                        _msg += ' AND "%s" = ' % _key
                    _msg += "'%s'" % _val
                #print(_msg)
                _raw_data = self.connection.query(_msg)
                _data = _raw_data.raw['series'][0]['values']
                results[_tag_key] = {_c:v[1] for _c, v in enumerate(_data)}

            if set(tag_key).issubset(self._main_tags):
                output = pd.DataFrame(results)
            else:
                output = {}
                for _key in results.keys():
                    output[_key] = pd.DataFrame({_key:results[_key]})
        return output

    def get_tag_keys(self, measurement):
        ''' Description:
            get the different values of the tag in tag_key
        -----------------------------------------------------------------------
        Input:  - measurement: The measurement name (string)
        -----------------------------------------------------------------------
        Output: - a list of strings
        '''
        query = 'SHOW TAG KEYS ON "%s" FROM "%s"' % (self.database,
                                                     measurement)
        _rs = self.connection.query(query).raw['series'][0]
        return [_x[0] for _x in _rs['values']]

    def get_fields(self, measure):
        ''' Description:
            get the different field_keys of measurement
        -----------------------------------------------------------------------
        Input:  - measurement: The measurement name (string)
        -----------------------------------------------------------------------
        Output: - a list of strings
        '''
        query = 'SHOW FIELD KEYS ON "%s" FROM "%s"' % (self.database, measure)
        _rs = self.connection.query(query).raw['series'][0]
        return [_x[0] for _x in _rs['values']]
    ##########################################################################
    ###
    ###             write methods
    ###
    ##########################################################################
    def _remove_string(self,points,string='nan'):
        return remove_field_with_string(points,string=string)

    def writeDataframe(self, dataframe, measure, tags, w_batch_size=10000,
                       n_precision=4):
        ''' Description:
            writes a dataframe to the default database with the tags given in
            tags_series into the measurement in measure_name
        -----------------------------------------------------------------------
        Input:  - dataframe
                - tags_serie
                - measure_name
        -----------------------------------------------------------------------
        Output: - flag: True when writting process ended correctly.
                  Otherwise false.
        '''


        if w_batch_size:
            number_batches = int(math.ceil(len(dataframe) / float(w_batch_size)))
            for batch in range(number_batches):
                start_index = batch * w_batch_size
                end_index = (batch + 1) * w_batch_size
                if isinstance(tags, dict):
                    points = self.w_con._convert_dataframe_to_lines(
                                dataframe.iloc[start_index:end_index].copy(),
                                measure,
                                global_tags=tags,
                                numeric_precision=n_precision)
                else:
                    points = self.w_con._convert_dataframe_to_lines(
                                dataframe.iloc[start_index:end_index].copy(),
                                measure,
                                tag_columns=tags,
                                numeric_precision=n_precision)
                self.connection._write_points( self._remove_string(points),
                                              None, None, None,
                                              None,protocol='line')
        else:
            if isinstance(tags, dict):
                points = self.w_con._convert_dataframe_to_lines(
                            dataframe.iloc[start_index:end_index].copy(),
                            measure,
                            global_tags=tags,
                            numeric_precision=n_precision)
            else:
                points = self.w_con._convert_dataframe_to_lines(
                            dataframe.iloc[start_index:end_index].copy(),
                            measure,
                            tag_columns=tags,
                            numeric_precision=n_precision)
            self.connection._write_points( self._remove_string(points),
                                          None, None, None,
                                              None,protocol='line')
        return True

    def dropSeries(self, measure, tags, logic='AND'):

        msg2post = 'DROP SERIES FROM "%s" WHERE ' %( measure,)
        text = ' %s ' % logic
        msg2post+= text.join(
                    ['"%s" = \'%s\''%(key,value) for key,value in tags.items()]
                            )
        return self.connection.query(msg2post)

    def queueData(self, dataframe, tags, measure):
        ''' Description:
            queues the data to send in to the database into batches
            If tags if a dictionary, remove the fields of the frame and adds
            new fields (tag_keys) set to "tag_value". In this case, the other
            fields of the dataframe are converted to float64!
            When tags is a list, the frame is not modified
        -----------------------------------------------------------------------
        Input:  - dataframe: a dataframe with the data fields.
                - tags: a diccionary with the tags that will be added
                        to the frame (tag_key:tag_value)
                        or a list with the tag_keys
        '''
        # if first time
        if not hasattr(self, '_tag_names'):
            self._tag_names = {}
        if not hasattr(self, '_queue'):
            self._queue = {}
        # add tags
        if self._tag_names.keys().isdisjoint([measure]):
            if isinstance(tags, dict):
                self._tag_names[measure] = list(tags.keys())
            else:
                self._tag_names[measure] = tags
        # in case that tags are passed
        if isinstance(tags, dict):
            _df = dataframe.drop(columns=tags.keys())
            _df = _df.astype(np.float64)
            for tag_name in self._tag_names[measure]:
                _df[tag_name] = tags[tag_name]
        else:
            _df = dataframe
        # create queue or append data to queue
        if self._queue.keys().isdisjoint([measure]):
            self._queue[measure] = _df
        else:
            self._queue[measure] = pd.concat((self._queue[measure], _df),
                                             axis=0)
        # commit when conditions are True
        condition1 = (self._autocommit == True)
        condition2 = (self._queue[measure].shape[0] > self._commit_size)
        if condition1 & condition2:
            return self._commitData(measure)
        else:
            return True

    def _commitData(self, measure):
        ''' Description:
            writes the queued data into the database. The all-nan columns are
            removed, so empty measures could be committed (only with tags)
        -----------------------------------------------------------------------
        Input:  - measure: (str) name of the measurement queue to commit()
        -----------------------------------------------------------------------
        Output: - flag: True when writting process ended correctly.
                        Otherwise false.
        '''
        try:
            # remove empty columns!
            _df_queue = self._queue[measure].copy(deep=True)
            mask = (_df_queue.isnull().values == 0).sum(axis=0)
            if (mask == 0).sum() > 0:
                labels = [_field for _field, _m
                                  in zip(_df_queue.columns, mask) if _m != 0]
                _df_queue = _df_queue[labels]
            # push data to influx
            flag = self.writeDataframe(_df_queue, measure,
                                       self._tag_names[measure],
                                       w_batch_size=self._batch_size)
        except:
            # something happened, data will not be pushed!
            flag = False
        # clear the queue in case that the commit was succcessfull
        if flag == True:
            _columns = self._queue[measure].columns
            self._queue[measure] = pd.DataFrame(columns=_columns)
        return flag

    ##########################################################################
    ###
    ###             read  methods
    ###
    ##########################################################################

    def _get_query_msg(self, measure, tags, _ini, _end,_fields):
        ''' Description:
        -----------------------------------------------------------------------
        Input:  - measurement: (str) measurement name
                - tags: (dict) {key_tag: value_tag}
                - _ini: (timestamp obj. or None)
                - _end: (timestamp obj. or None)
        -----------------------------------------------------------------------
        Output: - (str) message to be posted
        '''
        measure_msg = _measure4query(self.database,
                                     self.retention_policy,
                                     measure)
        if isinstance(_fields,list):
            fields = ','.join(['"%s"'% _x for _x in _fields])
        else:
            fields = '*'
        msg2post = 'SELECT %s FROM %s' % (fields, measure_msg)
        time_msg = _time4query(_ini, _end)
        time_flag = 0
        if time_msg !='':
            msg2post += ' WHERE %s' % time_msg
            time_flag = 1
        tag_string = _tags4query(tags)
        if tag_string != '':
            msg2post += {0: ' WHERE %s',
                         1: ' AND %s'}[time_flag] % tag_string
        return msg2post

    def _query_data(self, measure, tags={}, ini_time=None, end_time=None,
                    limit=100,_fields=None):
        ''' Description:
            query data from a measure to current influx database
        -----------------------------------------------------------------------
        Input:  - measure: (str) name of the measurement
                - tags: (dict) tag_key : tag_value
                - ini_time: (timestamp) begining of period of query
                - end_time: (timestamp) end of period of query
        -----------------------------------------------------------------------
        Output: - (obj) returns influx data object
        '''
        msg2post = self._get_query_msg(measure, tags, ini_time,
                                       end_time,_fields)
        msg2post += ' LIMIT %d' % (limit)
        return self.connection.query(msg2post)

    def getFrameFromDevice(self, measurement, tags, _ini=None, _end=None,
                           batch_limit=1,_fields=None):
        '''
            request data from current database, retention_policy and given
            measurement given the tag dictionary and the (ini, end) timestamp
            object. The received data is returned as a dataframe.
        -----------------------------------------------------------------------
        Input:  - measurement: (str) measurement name
                - tags: (dict) {key_tag: value_tag}
                - _ini: (timestamp obj. or None)
                - _end: (timestamp obj. or None)
                - _batch_limit: (int) Max number of lines returned by query
        -----------------------------------------------------------------------
        Output: - (pd.DataFrame) dataframe with queried data
        '''
        _rs = self._query_data(measurement, tags,
                               ini_time=_ini,
                               end_time=_end,
                               limit=batch_limit,
                               _fields=_fields)
        dataframe = convertQuery2DataFrame(_rs, main_tags=self._main_tags)
        return dataframe

    #TODO test that this method works fine
    def getFramesFromDevices(self, measure_dict, device_dict, time_dict,
                             batch_limit= 1000):
        ''' Description:
            Requests
        -----------------------------------------------------------------------
        Input:  - measure_dict: (dict) {deviceID: measurement_name}
                - device_dict: (dict) {deviceID: {tag_key: tag_value}}
                - time_dict: {devID: {'ini: pd.Timestamp, 'end: pd.Timestamp}}
                - batch_limit: (int)
        -----------------------------------------------------------------------
        Output: - flag: (bool) True when writting process ended correctly.
                  Otherwise false.
        '''
        df = {}
        for _devID in device_dict.keys():
            df[_devID] = self.getFrameFromDevice(measure_dict[_devID],
                                                 device_dict[_devID],
                                                 _ini=time_dict[_devID]['ini'],
                                                 _end=time_dict[_devID]['end'],
                                                 batch_limit=batch_limit)
        return df

    def get_ini_end_measure(self, measure, tags, fields=None):
        ''' Description:
            request data from current database, retention_policy and given
            measurement given the tag dictionary and the (ini, end) timestamp
            object. The received data is returned as a dataframe.
        -----------------------------------------------------------------------
        Input:  - measurement: (str) measurement name
                - tags: (dict) {key_tag: value_tag}
        -----------------------------------------------------------------------
        Output: - _ini: (pd.Timestamp localizazed) First timestamp of measure
                - _end: (pd.Timestamp localizazed) Last timestamp of measure
        '''
        msg2post = self._get_query_msg(measure, tags, None, None,fields)
        msg_ini = msg2post + ' ORDER BY time DESC LIMIT 1'
        _rs = self.connection.query(msg_ini)
        for _name, _last in zip(_rs.raw['series'][0]['columns'],
                                _rs.raw['series'][0]['values'][0]):
            if 'time' == _name:
                break
        _dt_end = pd.to_datetime(_last, utc=True)
        # first datetime
        msg_end = msg2post + ' ORDER BY time ASC LIMIT 1'
        _rs = self.connection.query(msg_end)
        for _name, _first in zip(_rs.raw['series'][0]['columns'],
                                 _rs.raw['series'][0]['values'][0]):
            if 'time' == _name:
                break
        _dt_ini = pd.to_datetime(_first, utc=True)
        return (_dt_ini, _dt_end)
