import api_restful
import sql_connector
import pickle
import bz2
import _pickle as cPickle
import copy
import influx_test

NUMBER_SBOX_4 = 8
NUMBER_SBOX_2 = 4
NUMBER_SBOX_0 = 1

class Configuration():
    def __init__(self)  -> None:
        self.api_bettair = api_restful.Api()
        self.mydb = sql_connector.SqlConnector()
        self.alerts = []
        #self.order = "MANUAL"
        #self.afes_list =  []
        self.db_filter = "ALL"
        self.afes_filter = "ALL"
        self.cicles = [True, True, True, True, True]
        self.resample = 'None'
        self.show_tunel_sensors = []
        self.show_tunel_sensors_gas = []
        self.sensors_gas = []
        self.afes_under_test = {}
        self.afes_result_test = {}
        self.validate_test = False
        self.abox = 1
        self.traces = 0
        
        self.index = {'sbox' : "MANUAL"}
        self.sensors = {'sorted' : '', 'reverse' : True, 'sn_sensor' : '', 'sn_afe' : '', 'sensor_date' : '' }
        self.sensors_row = dict()
        self.repairs = {'filter' : 'ALL', 'repair_filter' : 'State', 'repair2_filter' : 'State', 'repair_sens' : ['','','','']}
        self.co2 = {'sensors' : ['']*8}
        
        try:
            self.order_list =  self.api_bettair.get_order_list()
        except Exception as e:
            self.order_list = []
            print(e)
        
    def get_afes_list(self, order):
        try:
            self.afes_list = self.afes_list + self.api_bettair.get_afes_list(order)
        except Exception as e:
            print(e)
    
    def reset_afes_list(self):
        if self.abox == 1:
            self.afes_list[0:5] = []
        elif self.abox == 2:
            self.afes_list[5:10] = []
    
    def get_resample_int(self):
        if (self.resample != 'None'):
            return int(self.resample.split(" ")[0])
        else:
            return 0
    
    def alert(self, alert_txt, color):
        self.alerts.append([alert_txt, color])
        
    def set_afes_under_test(self, afes, sensors):
        self.afes_under_test = {}
        for key in afes:
            #print(key)
            for i in range(2):
                #print(afes[key][i])
                if afes[key][i]:
                    self.afes_under_test[afes[key][i]] = sensors[key][i]
        self.afes_result_test = copy.deepcopy(self.afes_under_test)
        self.validate_test = False
            
    def restart_validate_test(self):
        self.afes_result_test = copy.deepcopy(self.afes_under_test)
        self.validate_test = False
    
    def save_data(self, obj, filename ):
        #with open('results/'+ filename + '.pkl', 'wb') as f:
        #    pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
        with bz2.BZ2File('results/'+ filename + '.pbz2', 'wb') as f:
            cPickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

    def get_sbox_occupied(self):
        result = {"TESTING" : ["",""]}
        SBOX_4, SBOX_2, SBOX_0 = self.get_sbox_distribution()
        for i, box in enumerate(SBOX_4):
            result[f"SBOX_4-{i+1}"] = [box.count(True), len(box)]
        for i, box in enumerate(SBOX_2):
            result[f"SBOX_2-{i+1}"] = [box.count(True), len(box)]
        for i, box in enumerate(SBOX_0):
            result[f"SBOX_0-{i+1}"] = [box.count(True), len(box)]
        return result
    
    def get_boxes_names(self):
        boxes = ["ABOX_1", "ABOX_2", "SBOX_4-1", "SBOX_4-2", "SBOX_4-3", "SBOX_4-4", "SBOX_4-5", "SBOX_4-6", "SBOX_4-7", "SBOX_4-8", "SBOX_2-1", "SBOX_2-2", "SBOX_2-3", "SBOX_2-4", "SBOX_0-1"]
        return boxes
    def get_boxes_powered(self):
        powered = [True, True, True, True, True, False, False, False, False, True, True, False, False, False]
        return powered
    
    def get_sbox_distribution(self):
        try:
            #SBOX_OK = ["SBOX_4-2", "SBOX_2-1"]
            #SBOX_NOT_OK = ["SBOX_4-5", "SBOX_2-2"]
            # Assignem totes les caixes buides
            SBOX_4 = []
            for i in range(1,NUMBER_SBOX_4+1):
                SBOX_4.append([False]*16)
            SBOX_2 = []
            for i in range(1,NUMBER_SBOX_2+1):
                SBOX_2.append([False]*32)
            SBOX_0 = []
            for i in range(1,NUMBER_SBOX_0+1):
                SBOX_0.append([False]*500)
            afes = self.mydb.get_all_afes()
            i=0
            #print(SBOX_0)
            for afe in afes:
                #print(afe['sn'])
                sbox = afe['location']
                #print(f"{i} : {afe['sn']}: {sbox} ")
                i += 1
                if (sbox.startswith("SBOX_")):
                    box = int(sbox.split("-")[1])-1
                    position = int(sbox.split("-")[2])-1
                    #print(f"{afe['sn']} : {box} : {position} : ?")
                    if (position >= 0 and box >= 0):
                        #print(f"{afe['sn']} : {box} : {position} : True")
                        if sbox.startswith("SBOX_4"):
                            SBOX_4[box][position] = True
                        elif sbox.startswith("SBOX_2"):
                            SBOX_2[box][position] = True
                        elif sbox.startswith("SBOX_0"):
                            SBOX_0[box][position] = True
        except Exception as e:
            print(e)
        """
        print("------- SBOX 4 ------------")
        print(SBOX_4)
        print("------- SBOX 2 ------------")
        print(SBOX_2)
        print("------- SBOX 0 ------------")
        print(SBOX_0)
        """
        return [SBOX_4, SBOX_2, SBOX_0]
    
    def get_afe_location(self, afe_in, state):
        
        SBOX_4, SBOX_2, SBOX_0 = self.get_sbox_distribution()
        #print(SBOX_0)
        location = ""
        if state == "VOID":
            print(f"Searching space in SBOX_0-1")
            #print(box)
            for i, pos in enumerate(SBOX_0[0]):
                if not pos:
                    location = f"SBOX_0-1-{i + 1}"
                    break
                        
        elif state == "OK" or state == "NOT_TESTED":
            if afe_in.startswith("2"):
                # Busquem espai apartir de la caixa SBOX_4-1
                for j, box in enumerate(SBOX_4[0:]):
                    print(f"Searching space in SBOX_4-{j + 1}")
                    #print(box)
                    for i, pos in enumerate(box):
                        if not pos:
                            location = f"SBOX_4-{j + 1}-{i + 1}"
                            break
                    else:
                        continue
                    
                    break
            # Busquem espai a la caixa SBOX_2-1
            elif afe_in.startswith("1"):
                # Busquem espai apartir de la caixa SBOX_2-1
                for j, box in enumerate(SBOX_2[0:]):
                    print(f"Searching space in SBOX_2-{j + 1}")
                    #print(box)
                    for i, pos in enumerate(box):
                        if not pos:
                            location = f"SBOX_2-{j + 1}-{i + 1}"
                            break
                    else:
                            continue
                        
                    break
        elif state == "NOT_OK":
            if afe_in.startswith("2"):
                START_BOX = 5
                # Busquem espai a la caixa SBOX_4-5
                for j, box in enumerate(SBOX_4[START_BOX-1:]):
                    print(f"Searching space in SBOX_4-{j + START_BOX}")
                    #print(box)
                    # Busquem espai a la caixa SBOX_4-5
                    for i, pos in enumerate(box):
                        if not pos:
                            location = f"SBOX_4-{j + START_BOX}-{i + 1}"
                            break
                    else:
                        continue
                    
                    break
            # Busquem espai a la caixa SBOX_2-3
            
            elif afe_in.startswith("1"):
                START_BOX = 3
                for j, box in enumerate(SBOX_2[START_BOX-1:]):
                    print(f"Searching space in SBOX_2-{j + START_BOX}")
                    print(box)
                    for i, pos in enumerate(box):
                        print(f"{i} -> {pos}")
                        if not pos:
                            location = f"SBOX_2-{j + START_BOX}-{i + 1}"
                            break
                            print(location)
                    else:
                        continue
                    
                    break
                
        if location:
            print(f"{afe_in} --> {location}")
            return location
        
        print(f"Not space available to alocate AFE {afe_in}")
        
        return ""
    
    def assign_afe_location(self, afe_in, state):
        location = self.get_afe_location(afe_in, state)
        if location:
            d =  {'repair_afe': afe_in, 'repair_location': location}
            return self.mydb.set_afe_location(d) 
        
        return False
        
    # Llegim del fitxer picke el diccionary de dataframes:
    # {'SN1_AE_mean' : df1, 'SN2_AE_mean' : df2,......, 'SN1_WE_mean' : dfn, ...., 'SN1_DIFF_mean' : dfm}
    # Each column of dataframes df:  26-000503  26-000515   26-000553
    def load_data(self, filename):
        try:
            #with open('results/' + filename + '.pkl', 'rb') as f:
            #    return pickle.load(f)
            with bz2.BZ2File('results/' + filename + '.pbz2', 'rb') as f:
                return cPickle.load(f)
        except Exception as e:
            print(e)
            return None
    
    def load_binary_file(self, filename):
        df = None
        try:
            with open(f'results/{filename}.pbz2', 'rb') as f:
                df = f.read()
        except Exception as e:
            self.alert(f"Fail reading {filename} Dataframe", "alert-danger")
        return df
    
    def load_anomaly(self):
        return self.load_binary_file('anomaly')
    
    def load_anomaly_amb(self):
        return self.load_binary_file('ambient')
    
    def save_binary_file(self, df, filename):
        try:
            with open(f'results/{filename}.pbz2', 'wb') as f:
                f.write(df)
        except Exception as e:
            self.alert(f"Fail writing {filename} Dataframe", "alert-danger")
        return None
    
    def save_anomaly(self, df):
        return self.save_binary_file(df, 'anomaly')
    
    def save_anomaly_amb(self, df):
        return self.save_binary_file(df, 'ambient')
    
if __name__ == "__main__":
    conf = Configuration()
    #conf.assign_afe_location("14-000432" ,"OK")
    #print(conf.get_afe_location('26-000522', 'OK'))
    #print(conf.get_sbox_distribution())
    #print(conf.get_sbox_occupied())
    #dfs = conf.load_data("anomaly")
    #for df in dfs:
    #    print(df)
    