use production;
show tables;

create table sensors (
sn varchar(10),
gas varchar(10),
zero_cal_date varchar(10),
ae_zero varchar(10),
we_zero varchar(10),
sensitivity_na varchar(10),
sensitivity_mv varchar(10),
sensitivity_no2_na varchar(10),
sensitivity_no2_mv varchar(10),
date_reception varchar(25),
batch varchar(15),
state varchar(15),
last_update varchar(25),
observations varchar(255),
changes varchar(1024),
unique(sn)
);

select * from test_bettair;
alter table test_bettair add column test_final TEXT(15000) after test_traces;
alter table test_bettair modify column test_final text(65535);

alter table sensors rename column WE_zero TO we_zero;
alter table afes modify column location varchar(15);
alter table afes add unique (sn);
select * from sensors;
select * from afes;
select * from afes where sn1 = '130380042' or sn2 = '130380042' or sn3 = '130380042' or sn4 = '130380042';
select count(sn) from sensors;
SELECT sn,gas,date_reception,batch,state,last_update,observations,changes FROM sensors WHERE NOT EXISTS (SELECT * FROM afes WHERE sn1 = sensors.sn or sn2 = sensors.sn or sn3 = sensors.sn or sn4 = sensors.sn);

select * from afes where sn = '10-101165';
delete from afes where sn = '10-101165';
delete from sensors where sn = '214751526';
delete from sensors where sn = '212000000';

select * from sensors where state = 'SBOX_1_2';
select * from sensors where state like '%SBOX_1_2%';
select * from sensors where sn ='212000000';

alter table afes drop column afe_type;

create table afes (
sn varchar(10),
pn varchar(15),
sn1 varchar(10),
sn2 varchar(10),
sn3 varchar(10),
sn4 varchar(10),
afe_type varchar(10),
zero_cal_date varchar(10),
location varchar(10),
ae_zero_sn1 varchar(10),
ae_zero_sn2 varchar(10),
ae_zero_sn3  varchar(10),
ae_zero_sn4 varchar(10),
we_zero_sn1 varchar(10),
we_zero_sn2 varchar(10),
we_zero_sn3 varchar(10),
we_zero_sn4 varchar(10),
pcb_gain_sn1 varchar(10),
pcb_gain_sn2 varchar(10),
pcb_gain_sn3 varchar(10),
pcb_gain_sn4 varchar(10),
date_reception varchar(25),
batch varchar(15),
state varchar(15),
last_update varchar(25),
observations varchar(255),
changes varchar(1024),
unique(sn)
);

create table anomaly (
date_start timestamp,
afes_list VARCHAR(255),
gases VARCHAR(30),
df MEDIUMBLOB,
df_amb MEDIUMBLOB,
df_vref mediumblob
);

alter table anomaly modify column afes_list varchar(2000);

alter table anomaly drop column gases;

select * from anomaly;

select * from test_bettair;
select * from afes where sn='26-000407';
select * from afes where sn='13-999999';
select * from sensors where sn='33';
select * from afes;
select * from sensors;

select * from sensors where sn='212391519';
update sensors set state='SBOX_1_2' where sn='212391519';

select * from anomaly;


create table sensors2 as select * from sensors;
create table afes2 as select * from afes;
select * from sensors where sn = '214800202';
select * from sensors where gas = 'O3';
create table stocks2 as select * from stocks;

select * from afes;
select * from afes where sn = '27-000019';
select * from afes where date_reception = '2023-12-21 10:30:00';
update afes set date_reception = "2022-12-29" where date_reception = "2022-12-29 9:12:00";

create table afes4 as select * from afes;
select * from afes;
update afes set location = replace (location, "SBOX_0-0", "SBOX_0-1") where location like '%SBOX_0-0%';

alter table afes2 modify column order_unit_id varchar(30);
show tables;

SELECT * from afes where sn = '26-000271';
select * from anomaly;

create table co2_test (
date_start timestamp,
co2_list VARCHAR(255),
df MEDIUMBLOB,
df_temp MEDIUMBLOB,
df_amb MEDIUMBLOB
);

alter table co2_test add column df_temp MEDIUMBLOB;
alter table co2_test add column df_hum MEDIUMBLOB;

select * from co2_test;

select df from co2_test where co2_list like '%5899812%';

delete from co2_test where co2_list = '1 2 3 4 5 6 7 8';

update co2_test set co2_list = '5900185 5912734 3366894 590009 3770014' where co2_list = '5900185 5912734 3366894 5900009 3770014';

select * from co2_test where co2_list like '%5900009%';

SELECT date_start, co2_list FROM co2_test;

select * from anomaly where afes_list like '%18-000181%';
select * from afes where sn = '18-000212';

select * from sensors where sn = '212601935' or sn = '212601921';

select * from sensors where sn ="134040822";

create table stocks (
pn VARCHAR(30),
description VARCHAR(255),[
stock VARCHAR(10),
last_update TIMESTAMP,
picture BLOB,
package MEDIUMTEXT, 
PRIMARY KEY (pn)
);
alter table stocks add column quantity varchar(5);
alter table stocks add column edit BOOL;
update stocks set edit = false;

select * from sensors where sn = "212801407";

update stocks set pn = "Cover_cartridge" where pn = "Cover_cartrdidge";

delete from stocks where pn = 'BET2001_SENSORS_B';

select * from afes where sn = "18-000208";

select * from afes where sn = '18-000212';
update afes set order_unit_id = null  where sn = "18-000212";

alter table anomaly add column df_vref mediumblob;
select * from anomaly;

delete from anomaly where afes_list = '[{"sn": "26-000081", "sn1": "212120510", "sn2": "214120602", "sn3": "75190043", "sn4": "132620245"}, {"sn": "26-000082", "sn1": "212120513", "sn2": "214120607", "sn3": "75190044", "sn4": "132620208"}]'
and date_start = '2023-12-14 13:19:41';

show tables;

create table sensors2 like sensors;
insert into sensors2 select * from sensors;


UPDATE anomaly SET df_vref = NULL WHERE date_start = '2023-05-17 06:32:00';
alter table anomaly drop column df_vref;

select * from sensors;
select * from afes where sn like '%B%';
SELECT * FROM sensors WHERE sn LIKE '0%';
select * from sensors where sn = '217570003';
update afes set last_update = '2023-12-21 10:30:00' where sn = '26-000007B';
select * from afes where sn like '%B%';
alter table sensors modify column changes varchar(4096);
update afes set sn4 = '143966453' where sn = '26-000006B';
select * from afes where sn = '25-001175';
update afes set order_unit_id = NULL where sn = '25-001226';

# Remove 0 from init sn in all table
update sensors set sn=SUBSTR(sn, 2) where sn LIKE '0%';
delete from sensors2 where sn = '0212391506';

select * from afes where sn like '19%';
update afes set pn = '810-0021-04' where sn = '19-000104';

select * from test_bettair;
alter table test_bettair modify column assembler varchar(60);

select * from afes where sn = "13-001212";
update afes set order_config = 'E26AG00-CFB40' where sn = '13-001212';
update afes set order_id = 'B000201' where sn = '13-001212';
update afes set order_unit_id = '655cb90ab0c71159bee36846' where sn = '13-001212';
update afes set location = 'NONE' where sn = '26-000128';

select * from afes where sn = "18-000190";
update afes set order_config = null where sn = '18-000190';
update afes set order_id = null where sn = '18-000190';
update afes set order_unit_id = null where sn = '18-000190';

alter table test_bettair add column sn_pm varchar(25);
alter table test_bettair add column sn_co2 varchar(10);
delete from test_bettair where requisition = 'TEST';