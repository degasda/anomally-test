#from locations import *
import openpyxl
from os import listdir
from os.path import isfile, join
import xlrd
import json

"""
Material	Material Text	Name of Customer	Order/Contract/PO No.	Purchase Order No	Quantity to be Delivered	Planned Delivery Date	Product Cost	Amount to be Delivered	Division	Customer GRP3 Desc	Order Date	Order Item	Order Type	Customer	Customer GRP1	Customer GRP1 Desc	Customer GRP2	Customer GRP2 Desc	Customer GRP3	    Net Value	Order Quantity	Base Unit of Qty	Delivered Quantity	Quantity to be Invoiced	Payment Term	Quantity Billed	Value Billed	Value to be Invoiced	Curr	Local Curr	  Convf.	Pay. Term	Date of Expected Payment	Material Group	Material Group Desc	Material Grp 2	Material Grp 2 Desc	              WBS	WBS Desc	Quotation No.	Quotation Item	Config Code	Sales Employee	Emp. Name	Material Custom Code	Tax Country	Country	  Discount	Reference Doc.	Higher Level item	Surcharge	       Cost	     Margin	    Benefit	Name of Project	Last contract date	Version	Firmware	Rej code	Rej desc	Itm cat.	Delivery address
"""

HEADER = {"material" : 0 , "material_text" : 1 , "customer" : 2 , "order" : 3 ,"quantity" : 4, "delivery_address" : 63}

def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)

class Excel:

    def __init__(self, path):
        self.path = path
    """
    # retorna algo així: 
    [
    {
        "AE_1_offset": 288.0,
        "AE_2_offset": 287.0,
        "WE_1_offset": 277.0,
        "WE_2_offset": 281.0,
        "afe_type": "810-0021",
        "circuit_type": "03",
        "order": "80212",
        "pcb_gain_1": 0.8,
        "pcb_gain_2": 0.8,
        "sensors": {
            "SN1": {
                "ae_zero": 17.0,
                "sensitivity_mv": 0.382,
                "sensitivity_na": 0.478,
                "sensitivity_no2_mv": 0.0,
                "sensitivity_no2_na": "",
                "sensor_type": "NO-A4",
                "serial": "130730027",
                "we_zero": 16.0
            },
            "SN2": {
                "ae_zero": -8.0,
                "sensitivity_mv": 0.258,
                "sensitivity_na": 0.322,
                "sensitivity_no2_mv": 0.0,
                "sensitivity_no2_na": "",
                "sensor_type": "VOC-A4",
                "serial": "217270139",
                "we_zero": 79.0
            }
        },
        "serial": "13-000201",
        "zero_cal_date": "03/03/2023"
    }
]
    """
    def get_afes(self):
        # Obtenim tots els fitxers del directori
        onlyfiles = [f for f in listdir(self.path) if isfile(join(self.path, f))]
        #print(onlyfiles)
        result = []
        # Extreiem l'informació de cada fitxer
        for f in onlyfiles:
            # obrim el fitxer excel
            wb = xlrd.open_workbook(filename = self.path + "\\" + f)
            # Trobem el nom de la fulla activa y la obrim per treballar sobre ella
            self.sheet = wb.sheet_by_index(0)
            # Preparem el diccionary amb tota la informació
            afe = dict()
            # Obtenim els camps que necesitem
            afe['order']           = self.sheet.cell_value(rowx=5, colx=2)  #6C
            afe['zero_cal_date']   = self.sheet.cell_value(rowx=6, colx=2)  #7C
            afe['serial']          = self.sheet.cell_value(rowx=7, colx=2)  #8C
            afe['afe_type']        = self.sheet.cell_value(rowx=10, colx=2)  #11C
            afe['circuit_type']    = self.sheet.cell_value(rowx=11, colx=2)  #12C
            for i in range(4):
                gain = self.sheet.cell_value(rowx=27, colx=3+i)
                if gain:
                    afe[f'pcb_gain_{i+1}'] = gain      # 28D, 28E, 28F, 28G
            for i in range(4):
                we = self.sheet.cell_value(rowx=18, colx=3+i)
                if we:
                    afe[f'WE_{i+1}_offset'] = we      # 19D, 19E, 19F, 19G
            for i in range(4):
                ae = self.sheet.cell_value(rowx=21, colx=3+i)
                if ae:
                    afe[f'AE_{i+1}_offset'] = ae      # 22D, 22E, 22F, 22G
            # Sensor information
            afe['sensors'] = {}
            for i in range(4):
                sensor = dict()
                sensor_type = self.sheet.cell_value(rowx=15, colx=3+i)      # 16D, 16E, 16F, 16G 
                if (sensor_type):
                    sensor['sensor_type'] = sensor_type
                    sensor['serial']        = str(int(self.sheet.cell_value(rowx=16, colx=3+i)))  # 17D, 17E, 17F, 17G
                    sensor['sensitivity_na'] = round(self.sheet.cell_value(rowx=25, colx=3+i), 3)  # 26D, 26E, 26F, 26G
                    sensor['sensitivity_mv'] = round(self.sheet.cell_value(rowx=28, colx=3+i), 3)  # 29D, 29E, 29F, 29G
                    sensor['sensitivity_no2_na'] = self.sheet.cell_value(rowx=26, colx=3+i)  # 27D, 27E, 27F, 27G
                    sensor['sensitivity_no2_mv'] = self.sheet.cell_value(rowx=29, colx=3+i)  # 30D, 30E, 30F, 30G
                    sensor['ae_zero'] = self.sheet.cell_value(rowx=22, colx=3+i)  # 23D, 23E, 23F, 23G
                    sensor['we_zero'] = self.sheet.cell_value(rowx=19, colx=3+i)  # 20D, 20E, 20F, 20G
                    afe['sensors'][f'SN{i+1}'] = sensor
            
            result.append(afe)
            
        return result

            
    def get_afes_sql(self):
       # Obtenim tots els fitxers del directori
        onlyfiles = [f for f in listdir(self.path) if isfile(join(self.path, f))]
        #print(onlyfiles)
        result = []
        # Extreiem l'informació de cada fitxer
        for f in onlyfiles:
            # obrim el fitxer excel
            wb = openpyxl.load_workbook(self.path + "\\" + f)
            print(wb)
            # Trobem el nom de la fulla activa y la obrim per treballar sobre ella
            ws = wb.active
            print(ws)
            # Preparem el diccionary amb tota la informació
            afe = dict()
            # Obtenim els camps que necesitem
            afe['batch']            = ws.cell(row=6, column =3).value
            afe['zero_cal_date']    = ws.cell(row=7, column =3).value
            afe['sn']               = ws.cell(row=8, column =3).value
            afe['pn']               = f"{ws.cell(row=11, column =3).value} - {ws.cell(row=12, column =3).value}"
            print(afe)
            for i in range(4):
                gain = ws.cell(row=28, column=4+i).value
                if gain:
                    afe[f'pcb_gain_sn{i+1}'] = gain      # 28D, 28E, 28F, 28G
            for i in range(4):
                we = ws.cell(row=19, column=4+i).value
                if we:
                    afe[f'we_zero_sn{i+1}'] = we      # 19D, 19E, 19F, 19G
            for i in range(4):
                ae = ws.cell(row=22, column=4+i).value
                if ae:
                    afe[f'ae_zero_sn{i+1}'] = ae      # 22D, 22E, 22F, 22G
            # Sensor information
            afe['sensors'] = {}
            for i in range(4):
                sensor = dict()
                sensor_type = ws.cell(row=16, column=4+i).value      # 16D, 16E, 16F, 16G 
                if (sensor_type):
                    sensor['gas'] = sensor_type
                    sensor['sn'] = ws.cell(row=17, column=4+i).value
                    sensor['sensitivity_na'] = ws.cell(row=26, column=4+i).value
                    sensor['sensitivity_mv'] = ws.cell(row=29, column=4+i).value
                    sensor['sensitivity_no2_na'] = ws.cell(row=27, column=4+i).value
                    sensor['sensitivity_no2_mv'] = ws.cell(row=30, column=4+i).value
                    sensor['ae_zero'] = ws.cell(row=23, column=4+i).value  # 23D, 23E, 23F, 23G
                    sensor['we_zero'] = ws.cell(row=20, column=4+i).value  # 20D, 20E, 20F, 20G
                    afe['sensors'][f'SN{i+1}'] = sensor
            
            result.append(afe)
            
        return result
    
    def get_afes_xls_sql(self):
        # Obtenim tots els fitxers del directori
        onlyfiles = [f for f in listdir(self.path) if isfile(join(self.path, f))]
        #print(onlyfiles)
        result = []
        # Extreiem l'informació de cada fitxer
        for f in onlyfiles:
            # obrim el fitxer excel
            wb = xlrd.open_workbook(filename = self.path + "\\" + f)
            # Trobem el nom de la fulla activa y la obrim per treballar sobre ella
            self.sheet = wb.sheet_by_index(0)
            # Preparem el diccionary amb tota la informació
            afe = dict()
            # Obtenim els camps que necesitem
            afe['batch']            = self.sheet.cell_value(rowx=5, colx=2)  #6C
            afe['zero_cal_date']    = self.sheet.cell_value(rowx=6, colx=2)  #7C
            afe['sn']               = self.sheet.cell_value(rowx=7, colx=2)  #8C
            afe['pn']               = self.sheet.cell_value(rowx=10, colx=2) + "-" + self.sheet.cell_value(rowx=11, colx=2)   #11C + #12C
            for i in range(4):
                gain = self.sheet.cell_value(rowx=27, colx=3+i)
                if gain:
                    afe[f'pcb_gain_sn{i+1}'] = gain      # 28D, 28E, 28F, 28G
            for i in range(4):
                we = self.sheet.cell_value(rowx=18, colx=3+i)
                if we:
                    afe[f'we_zero_sn{i+1}'] = we      # 19D, 19E, 19F, 19G
            for i in range(4):
                ae = self.sheet.cell_value(rowx=21, colx=3+i)
                if ae:
                    afe[f'ae_zero_sn{i+1}'] = ae      # 22D, 22E, 22F, 22G
            # Sensor information
            afe['sensors'] = {}
            for i in range(4):
                sensor = dict()
                sensor_type = self.sheet.cell_value(rowx=15, colx=3+i)      # 16D, 16E, 16F, 16G 
                if (sensor_type):
                    sensor['gas'] = sensor_type
                    sh = self.sheet.cell_value(rowx=16, colx=3+i)
                    if sh:
                        sensor['sn'] = str(int(sh))  # 17D, 17E, 17F, 17G
                    sh = self.sheet.cell_value(rowx=25, colx=3+i)
                    if sh:
                        sensor['sensitivity_na'] = round(sh, 3)  # 26D, 26E, 26F, 26G
                    sh = self.sheet.cell_value(rowx=28, colx=3+i)
                    if sh:
                        sensor['sensitivity_mv'] = round(sh, 3)  # 29D, 29E, 29F, 29G
                    sensitivity_no2_na = str(self.sheet.cell_value(rowx=26, colx=3+i))
                    if sensitivity_no2_na.isnumeric():
                        sensor['sensitivity_no2_na'] = round(sensitivity_no2_na, 3)  # 27D, 27E, 27F, 27G
                    sensitivity_no2_mv = str(self.sheet.cell_value(rowx=29, colx=3+i))
                    if sensitivity_no2_mv.isnumeric():
                        sensor['sensitivity_no2_mv'] = round(sensitivity_no2_mv, 3)  # 30D, 30E, 30F, 30G
                    sensor['ae_zero'] = self.sheet.cell_value(rowx=22, colx=3+i)  # 23D, 23E, 23F, 23G
                    sensor['we_zero'] = self.sheet.cell_value(rowx=19, colx=3+i)  # 20D, 20E, 20F, 20G
                    afe['sensors'][f'SN{i+1}'] = sensor
            
            result.append(afe)
            
        return result
    
if __name__ == "__main__":
    e = Excel(r'C:\Users\Daniel Gaspar\BETTAIR CITIES S.L\PRODUCTION Site - Documents\Sensórica\Alphasense calibration files\test')
    #jprint(e.get_afes())
    #jprint(e.get_afes_sql())
    

