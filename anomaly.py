# testing.production.bettair.es
# 10.7.1.164

import os
import flask
from flask import request, jsonify, render_template, redirect, url_for, flash, send_from_directory
from werkzeug.utils import secure_filename
import threading
import automation
import influx_test as flux
from os import listdir, remove, makedirs
from os.path import isfile, isdir, join, dirname
import shutil
import configuration 
import api_restful
#import mongo_db
import pandas as pd
import sys
import copy
import datetime
import excel
import sql_connector
import json
import co2
import i2c
import io
import partsbox_api

UPLOAD_FOLDER = "./afes/"
TMP_FOLDER = "./tmp/"
ALLOWED_EXTENSIONS = {'xls', 'xlsm'}

app = flask.Flask(__name__)
app.secret_key = "@¬~~#@~€¬~#dedsfsdgRFRRDFX((((9f--"
app.config["DEBUG"] = True
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['TMP_FOLDER'] = TMP_FOLDER

# objects
fridge = automation.Automation()
config = configuration.Configuration()
api = api_restful.Api()
influx = flux.Influx()
mydb = sql_connector.SqlConnector()
co2_obj = co2.Co2()
api_partsbox = partsbox_api.Api()

from jinja2 import pass_context, Template
from markupsafe import Markup


def eval(value):
    return config.repairs[value] if value in config.repairs else ""

app.add_template_filter(eval)

def remove_images(type_txt):
    mypath = r"./static/"
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    
    try:
        for file in onlyfiles:
            if file.startswith("SN") and type_txt in file:
                print(f"remove {file}")
                remove(join(mypath,file))
    except Exception as e:
        print(e)

def remove_all_files(mypath):
    #mypath = r"./static/"
    try:
        if not os.path.exists(mypath):
            os.makedirs(mypath)
            return
            
        onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    
        for file in onlyfiles:
            remove(join(mypath,file))
            
    except Exception as e:
        print(e)

def copy_images(type):
    if type == "anomaly":
        mypath_orig = "./static/"
        afes = flux.AFES
        afes_list = [afe for cartridge in afes for afe in afes[cartridge] if afe]
        mypath_dest = f"./results/{' '.join(afes_list)}/"
        onlyfiles = [f for f in listdir(mypath_orig) if isfile(join(mypath_orig, f))]
        makedirs(dirname(mypath_dest), exist_ok=True)
        f = open(join(mypath_dest,"started.txt"), "a")
        f.write(str(fridge.start_date) + "\n")
        f.write(' '.join(sorted(set(influx.show_sensors))) + "\n")
        f.write(' '.join(get_sensors_gas()) + "\n")
        f.close()
        
        try:
            for file in onlyfiles:
                if ("anomaly" in file  and file.endswith(f".png")):
                    print(f"copy {join(mypath_orig,file)} to {join(mypath_dest,file)}")
                    shutil.copy(join(mypath_orig,file), join(mypath_dest,file))
        
            mypath_orig = f"./results/anomaly.pbz2"
            if isfile(mypath_orig):
                shutil.copy(mypath_orig, join(mypath_dest,'anomaly.pbz2'))
                print(f"copy {mypath_orig} to {join(mypath_dest, 'anomaly.pbz2')}")
                    
        except Exception as e:
            print(e)

def uncopy_images(type, afe, mypath = f"./results/"):
    if type == "anomaly":
        directories = [f for f in listdir(mypath) if isdir(join(mypath, f))]
        for directory in directories:
            print(directory)
            if afe in directory:
                #try:
                mypath = join(mypath, directory)
                onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
                #print(onlyfiles)
                for file in onlyfiles:
                    #if (("mean_anomaly" in file or "mean_pearson" in file) and file.endswith(f".png")):
                    #    print(f"uncopy {file}")
                    #    mypath_dest = "./static/"
                    #    shutil.copy(join(mypath,file), join(mypath_dest,file))
                    if (".pbz2" in file):
                        mypath_dest = "./results/"
                        print(f"uncopy {join(mypath,'anomaly.pbz2')} --> {join(mypath_dest,'anomaly.pbz2')}")
                        shutil.copy(join(mypath,'anomaly.pbz2'), join(mypath_dest,'anomaly.pbz2'))
                    elif ("started" in file):
                        f = open(join(mypath,"started.txt"), "rt")
                        f.readline()
                        sensors_txt = f.readline().replace("\n","")
                        influx.show_sensors = sensors_txt.split(" ")
                        sensors_gas = f.readline().replace("\n","")
                        config.sensors_gas = sensors_gas.split(" ")
                break
                #except Exception as e:
                #    print(e)
"""
def get_afes_saved(mypath = f"./results/"):
    #if not fridge.started:
    mypath = f"./results/"
    directories = [f.split(" ") for f in listdir(mypath) if not isfile(join(mypath, f))]
    #print(directories)
    saved_afes = []
    for directory in directories:
        for afe in directory:
            #if (afe.startswith("2") or afe.startswith("1")) and "-" in afe:
            saved_afes.append(afe)
    return sorted(saved_afes)
"""
def get_json():
    dict_return = fridge.tojson()
    dict_return['alerts'] = config.alerts
    return dict_return

def get_sensors_gas():
    sensors_gas = []
    sensors_afe4 = []
    sensors_afe2 = []
    for i in range(5):
        sensors_gas = list(flux.SENSORS.values())[i]
        print(sensors_gas)
        for n in range(4):
            if (len(sensors_gas[0]) >= n+1):
                if (sensors_gas[0][n] and sensors_gas[0][n] != ' '):
                    sensors_afe4 = list(flux.SENSORS.values())[i][0]
                    print(f"SENSORS 4: {sensors_afe4}")
                    break
    for i in range(5):
        sensors_gas = list(flux.SENSORS.values())[i]
        for n in range(2):
            if (len(sensors_gas[1]) >= n+1):
                if (sensors_gas[1][n] and sensors_gas[1][n] != ' '):
                    sensors_afe2 = list(flux.SENSORS.values())[i][1]
                    print(f"SENSORS 2: {sensors_afe2}")
                    break
    
    sensors_gas = sensors_afe4 + sensors_afe2
    return sensors_gas

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
           
# ----------------------------------------------------------
# Navigation
#-----------------------------------------------------------
@app.route('/', methods=['GET'])
def home():
    dict_return = get_json()
    #dict_return['orders'] = config.order_list
    afes = mydb.get_afes_sbox(config.index['sbox'])
    #print(config.afes_under_test)
    #print(config.afes_result_test)
    dict_return['afes4'] = [afe for afe in afes if afe.startswith('2') if not afe in config.afes_under_test]
    dict_return['afes2'] = [afe for afe in afes if afe.startswith('1') if not afe in config.afes_under_test]
    #dict_return['order'] = config.order
    dict_return['sensors'] = flux.SENSORS
    dict_return['abox'] = config.abox
    dict_return['sboxes'] = ['MANUAL'] + [box for box in config.get_boxes_names() if box.startswith('SBOX') or box.startswith('ABOX')]
    dict_return['sbox'] = config.index['sbox']
    return render_template('index.html', **dict_return)

@app.route('/noise_test/', methods=['GET'])
def noise_test():
    dict_return = dict()
    dict_return['debug_mode'] = fridge.is_debug_mode()
    dict_return['sensors_gas'] = get_sensors_gas()
    dict_return['show_sensors'] = sorted(set(influx.show_sensors)) 
    dict_return['alerts'] = config.alerts
    #print(dict_return['sensors_gas'])
    return render_template('noise.html', **dict_return)

@app.route('/anomaly_test/', methods=['GET'])
def anomaly_test():
    dict_return = {"started" : fridge.started, "start_date" : fridge.start_date}
    if fridge.start_date:
        dict_return['sensors_gas'] = get_sensors_gas()
    else:
        dict_return['sensors_gas'] = config.sensors_gas
    dict_return['debug_mode'] = fridge.is_debug_mode()
    dict_return['show_sensors'] = sorted(set(influx.show_sensors))
    dict_return['std_dict'] = influx.std_dict
    dict_return['alerts'] = config.alerts
    dict_return['cicles'] = config.cicles
    dict_return['resample'] = config.resample
    dict_return['num_cicles'] = automation.NUM_CICLES
    dict_return['afes_result_test'] = config.afes_result_test
    dict_return['validate_test'] = config.validate_test
    #if not fridge.started:
    dict_return['saved_afes'] = mydb.get_afes_tested_list()
    #print(dict_return['saved_afes'])
    return render_template('anomaly.html', **dict_return)

@app.route('/co2_test/', methods=['GET'])
def co2_test():
    dict_return = {"running_co2" : co2_obj.running_co2}
    dict_return['alerts'] = config.alerts
    dict_return['co2_sensors'] = config.co2['sensors']
    dict_return['debug_mode'] = fridge.is_debug_mode()
    dict_return['alerts'] = config.alerts
    dict_return['saved_co2'] = mydb.get_co2_tested_list()
    dict_return['progress'] = co2_obj.percent
    dict_return['temp'] = co2_obj.temp
    dict_return['hum'] = co2_obj.hum
    dict_return['co2'] = co2_obj.co2
    dict_return['co2_detected'] = co2_obj.co2_detected
    dict_return['co2_master'] = influx.get_data_co2()
    return render_template('co2_test.html', **dict_return)

@app.route('/tunel_test/', methods=['GET'])
def tunel_test():
    dict_return = get_json()
    dict_return['debug_mode'] = fridge.is_debug_mode()
    dict_return['sensors_gas'] = config.show_tunel_sensors_gas
    dict_return['show_sensors'] = config.show_tunel_sensors
    order_list = config.order_list[2:]
    order_list.reverse()
    dict_return['orders'] = order_list
    dict_return['std_dict'] = influx.std_dict
    dict_return['tunnel_calibration'] = api.get_tunnel_information2()
    #dict_return['cartridge_to_order_dictionary'] = api.get_cartridge_to_order_dictionary()
    return render_template('tunel.html', **dict_return)

@app.route('/afes/', methods=['GET'])
def afes():
    dict_return = dict()
    dict_return['boxes'] = config.get_boxes_names()
    dict_return['powered'] = config.get_boxes_powered()
    dict_return['debug_mode'] = fridge.is_debug_mode()
    dict_return['alerts'] = config.alerts
    dict_return['afes_filter'] = config.afes_filter
    dict_return['afes'] = mydb.get_all_afes()
    if (dict_return['afes_filter'] == "SBOX"):
        dict_return['occupy'] = config.get_sbox_occupied()
    elif (dict_return['afes_filter'] == "STOCK"):
        stocks = mydb.get_afe_stocks()
        dict_return['stocks_ok'] = stocks[0]
        dict_return['stocks_conditioning'] = stocks[1]
        dict_return['stocks_ko'] = stocks[2]
        dict_return['stocks_outdate'] = stocks[3]
        dict_return['stocks_assembled'] = stocks[4]
    dict_return.update(config.repairs)
    return render_template('afes.html', **dict_return)

@app.route('/sensors/', methods=['GET'])
def sensors():
    #print("Sensors")
    dict_return = dict()
    dict_return['debug_mode'] = fridge.is_debug_mode()
    dict_return['alerts'] = config.alerts
    #dict_return['afes'] = mydb.get_all_sensors(config.sensors['sorted'], config.sensors['reverse'])
    dict_return['info'] = config.sensors_row
    dict_return['sensors'] = mydb.get_all_sensors()
    dict_return['stock'] = mydb.get_sensors_stock()
    #print(config.sensors['sn_sensor'])
    #print(dict_return['afes'])
    #dict_return.update(config.sensors)
    #print("done")
    return render_template('sensors.html', **dict_return)

@app.route('/stock/', methods=['GET'])
def stock():
    #print("Sensors")
    dict_return = dict()
    dict_return['debug_mode'] = fridge.is_debug_mode()
    dict_return['alerts'] = config.alerts
    dict_return['stock'] = mydb.get_all_stock()
    #dict_return['info'] = config.sensors_row
    #print(config.sensors['sn_sensor'])
    #print(dict_return['afes'])
    #dict_return.update(config.sensors)
    #print("done")
    return render_template('stock.html', **dict_return)

@app.route('/stock2/', methods=['GET'])
def stock2():
    dict_return = dict()
    dict_return['alerts'] = config.alerts
    stocks = mydb.get_afe_stocks(grouped = True)
    dict_return['stocks_ok'] = stocks[0]
    dict_return['stocks_conditioning'] = []
    dict_return['stocks_ko'] = stocks[1]
    #dict_return['stocks_outdate'] = []
    #dict_return['stocks_assembled'] = []
    dict_return['stock_mecanics'] = api_partsbox.get_all_stock()
    dict_return['stock_sensors'] = mydb.get_sensors_stock()
    return render_template('stock2.html', **dict_return)

@app.route('/sensors/<string:variable>/', methods=['GET'])
def sort_sensors(variable):
    #print(variable)
    config.sensors['sorted'] = "_".join(variable.split('_')[2:])
    config.sensors['reverse'] = not config.sensors['reverse']
    #print(config.sensors['sorted'])
    return redirect(url_for("sensors"))

@app.route('/tested/', methods=['GET'])
def tested_nodes():
    dict_return = dict()
    dict_return['debug_mode'] = fridge.is_debug_mode()
    dict_return['nodes_tested_columns'] = ['requisition', 'assembler','sn_device','pn_device','sn_cartridge','pn_cartridge','sensors', 'extras',
                                        'sn_main','pn_main','sn_communication','pn_communication','sn_sensor','pn_sensor','sn_external', 
                                        'pn_external','sn_interface','pn_interface', 'sn_pm', 'sn_co2', 'mac','board_id','fw_modem','imei','interface_id','external_id',
                                        'fw','test_date','test_time', 'afes', 'test_traces', 'test_final']
    dict_return['nodes_tested'] = mydb.get_all_bettair_test()
    dict_return['trace_index'] = config.traces
    return render_template('tested.html', **dict_return)

@app.route('/orders/', methods=['GET', 'POST'])
def orders():
    dict_return = dict()
    dict_return['debug_mode'] = fridge.is_debug_mode()
    dict_return['orders_columns'] = mydb.get_orders_processing_columns()
    #print(dict_return['orders_columns'])
    dict_return['orders'] = mydb.get_orders_processing()
    cartridge_serial_index = dict_return['orders_columns'].index('cartridgeSerial')
    cartridge_serials = {i[cartridge_serial_index] for i in dict_return['orders'] if i[cartridge_serial_index]}
    #print(cartridge_serials)
    dict_return['changes_pcb'] = mydb.get_all_test_changes_pcb()
    dict_return['extras'] = mydb.get_all_test_extras()
    #print(dict_return['changes_pcb'])
    #print(dict_return['extras'])
    return render_template('orders.html', **dict_return)

# ----------------------------------------------------------
# Variables 
# ----------------------------------------------------------

@app.route("/post/<string:variable>/", methods = ['POST'])
def restful(variable):
    #print("GENERIC POST")
    print(request.form)
    if (request.method == 'POST'):
        try:
            # ------------ Orders -----------------
            if (variable.startswith("sbox")):
                restful_sbox(variable, request)
            # ------------ Alerts -----------------
            elif (variable.startswith("close_alert")):
                if (variable.split("_")[1] == "alerts"):
                    config.alerts.clear()
                else:
                    config.alerts.pop(int(variable.split("_")[2]))
            # ------------ Afes -----------------
            elif (variable.startswith("afe_")):
                restful_afe(request)
            # ------------ Sensors -----------------
            elif (variable.startswith("sn")):
                restful_sn(request)
            # ------------ Abox -----------------
            elif (variable.startswith("abox")):
                abox = int(request.form['abox'])
                config.abox = abox
            # ------------ Repair -----------------
            elif (variable.startswith("repair2_")):
                form = request.form.to_dict()
                if ('repair2_filter' in form):
                    form['repair_filter'] = form['repair2_filter']
                #restful_repair(variable, form)
                restful_repair2(variable, form)
                #print(variable)
                return redirect(url_for("afes"))
            # ------------ Add afes-----------------
            elif (variable.startswith("add_afes_automatic")):
                restful_add_afe_automatic(variable, request)
                return redirect(url_for("afes"))
            elif (variable.startswith("add_afes_manual")):
                restful_add_afe_manual(variable, request)
                return redirect(url_for("afes"))
            # ------------ Outdate -----------------
            elif (variable.startswith("outdate")):
                restful_outdate(variable, request)
                return redirect(url_for("afes"))
            # ------------ Traces -----------------
            elif (variable.startswith("traces")):
                restful_traces(int(variable.split("_")[1]))
            # ------------ Sensors -----------------
            elif (variable.startswith("sensors")):
                restful_sensors(variable.split('_')[1])
                return redirect(url_for("sensors"))
            elif ("stock" in variable):
                print("Stock")
                restful_stock(variable, request.form.to_dict())
                return redirect(url_for("stock"))
            elif (variable.startswith("order_reset")):
                flux.AFES = flux.AFES_VOID
                flux.SENSORS = flux.SENSORS_VOID
            elif (variable.startswith("order_test")):
                flux.AFES = flux.AFES_TEST 
            elif (variable.startswith("orders")):
                print(variable)
                print(variable.split('_'))    
                restful_orders(variable.split("_")[1], variable.split("_")[2], request.form)     
                return redirect(url_for("orders"))                                   
        except Exception as e:
            print(e)
    
    #print("Return Home")
    return redirect(url_for("home"))

def restful_sbox(variable, request):
    print(variable)
    if (not "_" in variable):
        #order = request.form['order']
        #config.order = order
        #config.get_afes_list(order)
        sbox = request.form['sbox']
        config.index['sbox'] = sbox
        #print(sbox)
    # Test i Clear
    else:
        sub_order = variable.split("_")[1]
        if (sub_order == 'reset'):
            config.reset_afes_list()
            a = 5*(config.abox-1)
            b = 5 + 5 * (config.abox-1)
            for i in range(a, b):
                flux.AFES[flux.CARTRIDGE[i]] = ["", ""]
                flux.SENSORS[flux.CARTRIDGE[i]] = [["", "", "", ""], ["", ""]]
        elif (sub_order == 'test'):
            config.reset_afes_list()
            a = 5*(config.abox-1)
            b = 5 + 5 * (config.abox-1)
            for i in range(a, b):
                flux.AFES[flux.CARTRIDGE[i]] = [f"afe{i+1}1", f"afe{i+1}2"]
                flux.SENSORS[flux.CARTRIDGE[i]] = [["NO2", "O3", "NO", "CO"], ["SO2", "H2S"]]
            

def restful_afe(request):
    form = request.form.to_dict()
    #print(form)
    afes = flux.AFES
    sensors = flux.SENSORS
    for key in form:
        #print(key)
        # Afegim els zeros que falten per no haver de possar-los si no volem
        serial = form[key] if (len(form[key]) == 9 or not "-" in form[key]) else form[key].split("-")[0] + "-" + "0"*(9-len(form[key])) + form[key].split("-")[1]
        #print(serial)
        cartridge = key.split("_")[0]
        #print(cartridge)
        sensors_list = mydb.get_afe_sensors_type(serial)
        #print(f"SensorList: {sensors_list}")
        #print(key.split("_"))
        if (key.split("_")[1] == "afe1"):
            afes[cartridge][0] = serial
            if (sensors_list):
                sensors[cartridge][0] = sensors_list
        else:
            afes[cartridge][1] = serial
            if (sensors_list):
                sensors[cartridge][1] = sensors_list
            
   
    flux.SENSORS = sensors
    flux.AFES = afes
    #print(flux.AFES)
    #print(flux.SENSORS)
    config.set_afes_under_test(afes, sensors)

def restful_sn(request):
    form = request.form.to_dict()
    #print(form)
    sensors = flux.SENSORS
    for key in form:
        cartridge = key.split("_")[0]
        #print(cartridge)
        #print(key)
        #sensors_list = api.get_sensors(form[key])
        #print(sensors_list)
        #print(f"{key.split('_')[1]}")
        sn = int(key.split("_")[1].replace("sn",""))
        #print(sn)
        if (sn <= 4):
            #print("afes4")
            #print(sensors)
            sensors[cartridge][0][sn-1] = form[key]
        elif (sn > 4 and sn <= 6):
            #print("afes2")
            sensors[cartridge][1][sn-5] = form[key]
        else:
            print(f"Not valid value for SN: {form[key]}")
            config.alert(f"Not valid value for SN: {form[key]}", "alert-warning")
            
    #print(afes)
    flux.SENSORS = sensors
    config.set_afes_under_test(flux.AFES, sensors)
    #print(flux.SENSORS)
    #flux.AFES = afes

def restful_repair2(variable, form):
    #orm = request.form.to_dict()
    print(form)
    config.repairs.update(form)
    var_list = variable.split("_")
    print(var_list)
    if (var_list[1] == 'apply'):
        for key in form:
            config.repairs[key] = form[key]
        if 'repair_afe' in form:
            value = form['repair_afe'] if (len(form['repair_afe']) == 9 or not "-" in form['repair_afe']) else form['repair_afe'].split("-")[0] + "-" + "0"*(9-len(form['repair_afe'])) + form['repair_afe'].split("-")[1]
            config.repairs['repair_afe'] = value
        if (form['repair2_filter'] == 'State'):
            if (mydb.set_afe_state(form)):
                config.alert(mydb.msg, "alert-success")
            else:
                config.alert(mydb.msg, "alert-warning")
        elif (form['repair2_filter'] == 'Location'):
            if (not form['repair_location']):
                location = config.get_afe_location(form['repair_afe'], 'NOT_OK')
                form['repair_location'] = location
            if (mydb.set_afe_location(form)):
                config.alert(mydb.msg, "alert-success")
            else:
                config.alert(mydb.msg, "alert-warning")
        elif (form['repair2_filter'] == 'Sensors'):
            if (mydb.set_afe_sensors(form)):
                config.alert(mydb.msg, "alert-success")
            else:
                config.alert(mydb.msg, "alert-warning")
    elif (var_list[1] == 'clear'):
        config.repairs = {'filter' : config.repairs['filter'], 'repair2_filter' : config.repairs['repair2_filter']}
    elif (var_list[1] == 'sens') and (len(var_list) > 2):
        config.repairs[variable] = form[variable]
        config.repairs["repair_sens"][var_list[2]] = form[variable]
    elif (var_list[1] == 'getposition') and (len(var_list)>= 4):
        location = ""
        if (var_list[2]):
            if (var_list[3] == "ok"):
                location = config.get_afe_location(var_list[2], "OK")
            elif (var_list[3] == "nok"):
                location = config.get_afe_location(var_list[2], "NOT_OK")
                #print(location)
            if location.startswith("SBOX"):
                config.repairs['repair_location'] = location
            else:
                config.alert("Not available Space", "alert-warning")
        else:
            config.alert("Entry an AFE", "alert-warning")
    else:
        config.repairs[variable] = form[variable]

def restful_add_afe_automatic(variable, request):
    # Remove all directory content
    remove_all_files(UPLOAD_FOLDER)
    for file in request.files.getlist('file'):
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(join(app.config['UPLOAD_FOLDER'], filename))
    return restful_add_afe()

def restful_add_afe_manual(variable, request):
    msg_ok = ""
    msg_ko = ""
    form = request.form.to_dict()
    if not form['add_afe_sn']:
        config.alert("Entry valid AFE serial number", "alert-warning")
        return False
    if not form['add_afe_pn'] or form['add_afe_pn'] == 'Afe circuit':
        config.alert("Entry valid AFE circuit type", "alert-warning")
        return False
    location = config.get_afe_location(form['add_afe_sn'], 'VOID' if not form['add_sensor_sn1'] and form['add_sensor_sn2'] and form['add_sensor_sn3'] and form['add_sensor_sn4'] else 'NOT_TESTED')
    form['location'] = location
  
    if (mydb.create_entry_manual(form)):
        msg_ok += mydb.msg + " in " + location + "\n"
        config.alert(msg_ok, "alert-success")
    else:
        msg_ko += mydb.msg + "\n"
        config.alert(msg_ko, "alert-danger")
   
    return True


def restful_add_afe():
    #e = excel.Excel(UPLOAD_FOLDER)
    try:
        print("Try xls file")
        e = excel.Excel(UPLOAD_FOLDER)
        afes = e.get_afes_xls_sql()
    except Exception as e:
        print(e)
        print("Try xlsm file")
        e = excel.Excel(UPLOAD_FOLDER)
        afes = e.get_afes_sql()
    msg_ok = ""
    msg_ko = ""
    #print(len(afes))
    for afe in afes:
        location = config.get_afe_location(afe['sn'], 'NOT_TESTED')
        afe['location'] = location
        if (mydb.create_entry_from_file(afe)):
            msg_ok += mydb.msg + " in " + location + "\n"
        else:
            msg_ko += mydb.msg + "\n"
    
    if msg_ok:
        print(msg_ok)
        config.alert(msg_ok, "alert-success")
    if msg_ko:
        print(msg_ko)
        config.alert(msg_ko, "alert-danger")
    
    remove_all_files(UPLOAD_FOLDER)
    return True
        

def restful_outdate(variable, request):
    form = request.form.to_dict()
    #print(form)
    afe_serial = variable.split("_")[1]
    if afe_serial == "all":
        stock_outdated = mydb.get_afe_stocks()[3]
        #print(stock_outdated)
        msg_ok = ""
        msg_ko = ""
        for i in stock_outdated[1:]:
            dic = {'repair2_filter': 'Location', 'repair_afe': i[1], 'repair_location': 'OUTDATED'}
            #print(f"Outdate: {i[1]}")
            if not (mydb.set_afe_location(dic)):
                msg_ok += mydb.msg
            else:
                msg_ko += mydb.msg
        if msg_ok:
            config.alert(msg_ok, "alert-success")
        if msg_ko:
            config.alert(msg_ko, "alert-danger")
                        
        return
    # Afegim els zeros que falten per no haver de possar-los si no volem
    dic = {'repair2_filter': 'Location', 'repair_afe': afe_serial, 'repair_location': 'OUTDATED'}
    #print(dic)
    if (mydb.set_afe_location(dic)):
        config.alert(mydb.msg, "alert-success")
        return True
    else:
        config.alert(mydb.msg, "alert-danger")
        return False
        
    
def restful_traces(index):
    config.traces = index

def restful_sensors(variable):
    form = request.form.to_dict() 
    print(form)
    config.sensors_row = dict()
    config.sensors.update(form)
    sn = form['sn_sensor']
    # sn is an afe
    if '-' in sn:
        info = mydb.get_afe_info(sn)
        #print(info)
        config.sensors_row.update(info)
    # sn is a sensor
    else:
        if variable == 'info':
            sensor = sn.strip().lstrip('0')
            info = mydb.get_sensor_info(sensor)
            #print(info)
            config.sensors_row.update(info)
            afe = mydb.get_afe_from_sensor(sensor)
            if (afe):
                config.sensors_row['afe'] = afe
        elif variable == 'add':
            msg = ''
            if (mydb.create_sensor(sn)):
                msg += mydb.msg
                if (mydb.set_sensor_location(sn, 'SBOX_1_2')):
                    msg += mydb.msg
                    config.alert(msg, "alert-success")
                    return True
            msg += mydb.msg
            config.alert(msg, "alert-danger")
        elif variable == 'location':
            if (mydb.set_sensor_location(sn, form['location'])):
                config.alert(mydb.msg, "alert-success")
                return True
            config.alert(mydb.msg, "alert-danger")
        else:
            print('BAD OPTION')
    return True

def restful_stock(variable, form):
    #print("Restful_stock")
    #print(form)
    #print(variable)
    variable_clean = variable.replace('*','/')
    #print(variable_clean)
    var_split = variable_clean.split('_')
    action = var_split[0]
    #print(var_split)
    if (action == "modify"):
        pn = "_".join(var_split[2:])
        print(pn)
        mydb.update_stock({'pn' : pn, 'edit' : True})
    elif (action == "save"):
        row = int(var_split[3])
        col = int(var_split[4])
        stocks = mydb.get_all_stock()
        pn = stocks[row+1][0]
        key = stocks[0][col]
        value = form['td'].replace("\n","").strip()
        #print(stocks[0])
        #print(stocks[row+1])
        #print(f"*'{key}' : {value}")
        dic = {'pn' : pn, key : value}
        #print(dic)
        mydb.update_stock(dic)
        #config.alert("Change done", "alert-success")
    elif (action == "done"):
        pn = "_".join(var_split[2:])
        mydb.update_stock({'pn' : pn, 'edit' : False})
    elif (action == "picture"):
        pn = "_".join(var_split[2:])
        file = request.files['file']
        #filename = pn+".jpg"
        #print(file.filename)
        #print(allowed_file(file.filename))
        #for file in request.files.getlist('file'):
        #if file and allowed_file(file.filename):
        if file:
            filename = secure_filename(file.filename)
            #print(filename)
            file.save(join(app.config['TMP_FOLDER'], filename))
        with open(join(app.config['TMP_FOLDER'], filename), 'rb') as file:
            picture = file.read()
        mydb.update_stock({'pn' : pn, 'picture' : picture})
    elif (action == "delete"):
        pn = "_".join(var_split[2:])
        mydb.delete_stock(pn)
    elif (action == "add"):
        #print(form)
        
        file = request.files['file']
        if file:
            filename = secure_filename(file.filename)
            #print(filename)
            file.save(join(app.config['TMP_FOLDER'], filename))
        with open(join(app.config['TMP_FOLDER'], filename), 'rb') as file:
            picture = file.read()
        
        group = ""
        for key in form.keys():
            #print(key)
            if key.startswith("quantity"):
                if form[key]:
                    print("_".join(key.split('_')[1:]) + " " + form[key] + ",")
                    group += ("_".join(key.split('_')[1:]) + " " + form[key] + ",")

        print(group)
        mydb.insert_stock(form['pn'], form['description'], form['stock'], picture, group)

def restful_orders(action, order, form):
    #print(action)
    #print(form)
    form_dict = dict(form.lists())
    #print(form_dict)
    #print(order)
    # {'PM+': 'extra', 'General': 'changes', 'VAC power supply': 'changes', 'RS485 PM+': 'changes'}
    if action == 'update':
        mydb.update_orders_database()
    else:   # 'save'
        extras = ''
        changes = ''
        # clean all nodes of the order
        mydb.set_all_test_changes_pcb(changes, order) 
        mydb.set_all_test_extras(extras, order) 
        for key in form_dict:
            for item in form_dict[key]:
                #print(form_dict[key])
                #print(key)
                col, cartridge = item.split('_')
                #print(col)
                #print(cartridge)
                #print(f"UPDATE {action} of {cartridge} to '{key}'")
                if col == 'extras':
                    mydb.add_test_extras(key, cartridge)
                else:
                    mydb.add_test_changes_pcb(key, cartridge)
            
       
        
@app.route('/anomaly/afe/<string:result>/<string:afe>/', methods=['POST'])
def post_afe_result(result, afe):
    #print(request.form)
    if (request.method == 'POST'):
        result = result.upper()
        msg_ok = ""
        msg_ko = ""
        # per la pantalla de anomaly test
        if afe in config.afes_result_test:
            if result == "NOT_OK":
                fail_sensor = [("*"+sensor) for sensor in config.afes_result_test[afe][2:-1] if not sensor.startswith("*")]
            else:
                fail_sensor = [""]
            # Canviem l'estat de la AFE
            d = {'repair_afe': afe, 'repair_state': result, 'repair_status': " ".join(fail_sensor)}
            if (mydb.set_afe_state(d)):
                config.afes_result_test[afe][1] = mydb.get_afe_state(afe)
                msg_ok += mydb.msg + "\n"
            else:
                msg_ko += mydb.msg + "\n"
        
            # Canviem la ubicació de la AFE
            if (config.assign_afe_location(afe, result)):
                if (config.afes_result_test):
                    config.afes_result_test[afe][0] = mydb.get_afe_location(afe)
    return redirect(url_for("anomaly_test"))

# ----------------------------------------------------------
# Main page
#-----------------------------------------------------------
@app.route('/start/', methods=['POST'])
def start():
    afes = flux.AFES
    if (mydb.start_anomaly(afes)):
        config.alert(mydb.msg, "alert-success")
    else:
        config.alert(mydb.msg, "alert-warnig")
    fridge.start()
    return redirect(url_for("home"))

@app.route('/stop/', methods=['POST'])
def stop():
    fridge.stop()
    return redirect(url_for("home"))

@app.route('/update_afes/', methods=['POST'])
def update_afes():
    print("update AFES")
    print(f"Form: { request.form} ")
    #print(request.form.to_dict())
    #flux.AFES = request.form.to_dict()
    form = request.form.to_dict()
    afes = flux.AFES_VOID.copy()
    sensors = flux.SENSORS_VOID.copy()
    for key in form:
        cartridge = key.split("_")[0]
        #if ("sn" in key):
        #    continue
        if (key.split("_")[1] == "afe1"):
            afes[cartridge][0] = form[key]
        elif (key.split("_")[1] == "afe2"):
            afes[cartridge][1] = form[key]
        else:
            if (form[key] and "sn" in key):
                print(f"{key.split('_')[1]}")
                sn = int(key.split("_")[1].replace("sn",""))
                print(sn)
                if (sn <= 4):
                    sensors[cartridge][0][sn-1] = form[key]
                    print("afes4")
                elif (sn > 4 and sn <= 6):
                    sensors[cartridge][0][sn-5] = form[key]
                    print("afes2")
                else:
                    print(f"Not valid value for SN: {form[key]}")
                    config.alert(f"Not valid value for SN: {form[key]}", "alert-warning")
            
    flux.AFES = afes
    flux.SENSORS = sensors
    
    return redirect(url_for("home"))

@app.route('/request_afes/', methods=['POST'])
def request_afes():
    #print("request AFES")
    #print(f"Form: { request.form} ")
    #print(request.form.to_dict())
    if (request.form['order'] == 'MANUAL'):
        flux.AFES = flux.AFES_TEST
    else:
        order = request.form['order']
        #print(order)
        config.get_afes_list(order)
    
    return redirect(url_for("home"))


# ----------------------------------------------------------
# Noise test
#-----------------------------------------------------------

@app.route('/view_noise/', methods=['POST'])
def view_noise():
    print(f"Form: { request.form} ")
    
    bcontinue = False
    for afe in flux.AFES.values():
        if afe[0] or afe[1]:
            bcontinue = True
            break
    
    if not bcontinue:
        config.alert("You must entry some AFEs in Automation page", "alert-warning")
        return redirect(url_for("noise_test"))
    
    remove_images("noise")
    
    try:
        minutes_int = int(request.form['time'].split(" ")[0])
    except:
        return redirect(url_for("noise_test"))
    
    end_test = datetime.datetime.now(datetime.timezone.utc)
    init_test = end_test - datetime.timedelta(minutes = minutes_int)
    print(f"{init_test} <--> {end_test}")
    
    resample_int = 0
    if (request.form['resample'] != 'None'):
        resample_int = int(request.form['resample'].split(" ")[0])
        
    if (influx.get_data(init_test, end_test, "noise")):
        influx.plot_noise(resample_int)
    else:
        config.alert("Fail getting data from influx", "alert-warning")
    return redirect(url_for("noise_test"))

# ----------------------------------------------------------
# Anomaly test
#-----------------------------------------------------------

@app.route('/view_anomaly/', methods=['POST'])
def view_anomaly():
    print(f"Form: { request.form} ")
    
    there_are_afes = True

    remove_images("anomaly")
    
    resample_int = 0
    config.resample = request.form['resample']
    if (request.form['resample'] != 'None'):
        resample_int = int(request.form['resample'].split(" ")[0])
    
    afe_selected = request.form['afe_selected']
    isCurrentAnomaly = False if '.....' in afe_selected else True
    
    if (isCurrentAnomaly):
        start_date = fridge.start_date 
        bcontinue = False
        for afe in flux.AFES.values():
            if afe[0] or afe[1]:
                bcontinue = True
                break
        if not bcontinue:
            config.alert("You must entry some AFEs in Automation page", "alert-warning")
            there_are_afes = False
            #return redirect(url_for("anomaly_test"))  
    else:
        start_date_str = date = afe_selected.split('.....')[1].strip()
        start_date = datetime.datetime.strptime(start_date_str, '%Y-%m-%d %H:%M:%S')
    print(start_date)
        
        
    # CICLE_TIME = [150, 250, 370, 470, 590, 690, 810, 910, 1030, 1105]
    # Update configuration variables
    config.cicles = [False]*5
    for key in request.form:
        if key.startswith("cicles"):
            config.cicles[int(key.split("_")[1])] = True
    minutes = 0
    # Init time
    if ('cicles_0' in request.form):
        config.cicles[0] = True
        minutes = 0
    elif ('cicles_1' in request.form):
        config.cicles[1] = True
        minutes = automation.CICLE_TIME[0]
    elif ('cicles_2' in request.form):
        config.cicles[2] = True
        minutes = automation.CICLE_TIME[2]
    elif ('cicles_3' in request.form):
        config.cicles[3] = True
        minutes = automation.CICLE_TIME[4]
    elif ('cicles_4' in request.form):
        config.cicles[4] = True
        minutes = automation.CICLE_TIME[6]

    init_test = start_date + datetime.timedelta(minutes = minutes)
    
    # Final Time get_data
    if ('cicles_4' in request.form):
        minutes = automation.CICLE_TIME[8]
    elif ('cicles_3' in request.form):
        minutes = automation.CICLE_TIME[6]
    elif ('cicles_2' in request.form):
        minutes = automation.CICLE_TIME[4]
    elif ('cicles_1' in request.form):
        minutes = automation.CICLE_TIME[2]
    elif ('cicles_0' in request.form):
        minutes = automation.CICLE_TIME[0]
    
    end_test = start_date + datetime.timedelta(minutes = minutes)
        
    if (isCurrentAnomaly):
        if (there_are_afes):
            if (influx.get_data(init_test, end_test, "anomaly")):
                influx.plot_anomaly(resample_int)
            else:
                config.alert("Fail getting data from influx", "alert-warning")
        if (influx.get_data_ambient(init_test, end_test)):
            influx.plot_ambient(resample_int)
        else:
            config.alert("Fail getting data ambient from influx", "alert-warning")
    else:
        afe = afe_selected.split('.....')[0].strip()
        date = afe_selected.split('.....')[1].strip()
        df, df_amb = mydb.get_dataframes(date, afe)
        config.save_anomaly(df)
        influx.plot_anomaly(resample_int, init_test, end_test)
    
    return redirect(url_for("anomaly_test"))

@app.route('/save_anomaly/', methods=['POST'])
def save_anomaly():
    #print(f"Form: { request.form} ")
    #copy_images("anomaly")
    afes = flux.AFES
    afes_list = [afe for cartridge in afes for afe in afes[cartridge] if afe]
    df = config.load_anomaly()
    df_amb = config.load_anomaly_amb()
    #gases = ' '.join(get_sensors_gas())
    #gases_position = sorted(set(influx.show_sensors))
    #print(f"1: {gases_position}")
    #gases_type = get_sensors_gas()
    #print(f"2: {gases_type}")
    #gases_dic = dict(zip(gases_position, gases_type)) 
    #print(f"3: {gases_dic}")
    #gases_json = json.dumps(gases_dic)
    #print(f"4: {gases_json}")
    if (mydb.add_anomaly(fridge.start_date, afes_list, df, df_amb)):
        config.alert(f"Anomaly test saved successfully", "alert-success")
    else:
        config.alert(f"Fail saving Anomaly test", "alert-danger")
    #elif (df and df_amb):
    #    if (mydb.add_anomaly(fridge.start_date, afes_list, df, df_amb, None)):
    #        config.alert(f"Anomaly test saved successfully", "alert-success")
    #    else:
    #        config.alert(f"Fail saving Anomaly test", "alert-danger")
    #elif (df):
    #    if (mydb.add_anomaly(fridge.start_date, afes_list, df, None)):
    #        config.alert(f"Anomaly test saved successfully", "alert-success")
    #    else:
    #        config.alert(f"Fail saving Anomaly test", "alert-danger")
        
    return redirect(url_for("anomaly_test"))

@app.route('/load_anomaly/', methods=['POST'])
def load_anomaly():
    print(f"Form: { request.form} ")
    uncopy_images("anomaly")
    
    return redirect(url_for("anomaly_test"))

@app.route('/validate_anomaly/', methods=['POST'])
def validate_anomaly():
    #print(f"Form: { request.form} ")
    #print(flux.AFES)
    #print(flux.SENSORS)
    config.restart_validate_test()
    afes_ut = config.afes_under_test
    afes_res = config.afes_result_test
    #print(f"afes_ut --> {afes_ut}")
    #print(f"afes_res --> {afes_res}")
    # EXEMPLE:afes_ut {'26-000400': ['NO2', 'O3', 'NO', 'CO'], '10-001154': ['NO2', 'O3'], '10-001163': ['NO2', 'O3'], '26-000936': ['NO2', 'O3', 'NO', 'CO']}
    
    # split all parameters from request POST and get a dictionary with AFES validated as true in front end
    afes_ok = {}
    for key in request.form:
        key_list = key.split("_")
        afe = key_list[1]
        sensor = key_list[2]
        electrode = key_list[3]
        gas = key_list[4]
        if afe in afes_ok:
            afes_ok[afe] += " "+gas+"_"+electrode
        else:
            afes_ok[afe] = gas+"_"+electrode
    #print(f"afes_ok --> {afes_ok}") 
    
    # From afes_ok decide wich afes are ok and wich ones are not_ok
    afes_result = {}
    for afe in afes_ut:
        end = 4 if afe.startswith("2") else 2
        for i in range(end):
            # El criteri per donar per vàlida un AFE es que sigui validad 2 dels tres test: AE, WE o DIFF
            total = 0
            if afe in afes_ok:
                if afes_ut[afe][i]+"_WE" in afes_ok[afe]:
                    total += 1
                if afes_ut[afe][i]+"_AE" in afes_ok[afe]:
                    total += 1
                if afes_ut[afe][i]+"_DIFF" in afes_ok[afe]:
                    total += 1
            if (total >= 2 ):
                afes_res[afe][i] = "*" + str(afes_res[afe][i])
            #print(f"---> {afes_res[afe]}")
        result = all("*" in string for string in afes_res[afe])
        afes_res[afe].append(result)
        afes_res[afe].insert(0, mydb.get_afe_state(afe))
        afes_res[afe].insert(0, mydb.get_afe_location(afe))
    #print(f"afes_res --> {afes_res}") 
    #print(afes_res)
    config.validate_test = True
    config.afes_result_test = afes_res
    
    return redirect(url_for("anomaly_test"))

# ----------------------------------------------------------
# CO2 test
# ----------------------------------------------------------
@app.route('/start_stop_co2_test/', methods=['POST'])
def start_stop_co2():
    print(f"Form: {request.form} ")
    if (co2_obj.running_co2):
        co2_obj.stop_co2_test()
    else:
        for i in range(len(request.form)):
            print(f'co2_sensor_{i} : ' + request.form[f'co2_sensor_{i}'])
            config.co2['sensors'][i] = request.form[f'co2_sensor_{i}']
        print(config.co2['sensors'])
        if (not i2c.is_co2_test_available()):
            config.alert(f"Test device not avalilable. Connect UBB from test device to Anomaly PC workstation", "alert-warning")
        else:  
            co2_obj.start_co2_test(60*co2.MINUTE, config.co2['sensors'])
    
    return redirect(url_for("co2_test"))

@app.route('/view_co2_test/', methods=['POST'])
def view_co2_test():
    print(f"Form: {request.form}")
    remove_images("co2")
    co2_selected = request.form['co2_selected']
    #resample = request.form['resample']
    
    sensor = co2_selected.split('.....')[0].strip()
    date = co2_selected.split('.....')[1].strip()
    
    for table in ['df', 'df_temp', 'df_hum']:
        df = mydb.get_co2_dataframes(table, date, sensor)
        print(df.describe())
        #df = df.drop(columns=['co2_ch2'])
        print(df)
        influx.correlation_matrix_co2(df, table)
        influx.plot_lines_co2(df, table)
    
    return redirect(url_for("co2_test"))

# ----------------------------------------------------------
# Tunel test
#-----------------------------------------------------------

@app.route('/view_tunel/', methods=['POST'])
def start_tunel():
    #print(f"Form: { request.form} ")
    remove_images("tunel")
    
    end_test = datetime.datetime.now(datetime.timezone.utc)
    init_test = datetime.datetime.now(datetime.timezone.utc)
    if (request.form['resample'] != 'Total test'):
        try:
            multiplier = 60 if request.form['time'].split(" ")[1] == "h" else 1440
            minutes_int = int(request.form['time'].split(" ")[0])*multiplier
            init_test = end_test - datetime.timedelta(minutes = minutes_int)
        except:
            return redirect(url_for("anomaly_test"))
    else:
        init_test = fridge.start_date
    
    resample_int = 0
    if (request.form['resample'] != 'None'):
        resample_int = int(request.form['resample'].split(" ")[0])
    
    cartridge_list = api.get_cartridge_serials(request.form['order'])
    print(cartridge_list)
    if (len(cartridge_list) >= 1):
        config.show_tunel_sensors = []
        afes_list = sorted(api.get_afes(cartridge_list[0]), reverse=True)
        print(afes_list)
        for afe in afes_list:
            sensors_list = mydb.get_afe_sensors_type(afe)
            print(sensors_list)
            if (sensors_list):
                config.show_tunel_sensors_gas += [sensor for sensor in sensors_list if sensor]
                config.show_tunel_sensors += [f"SN_{i+1}" for i in range(len(sensors_list)) if sensors_list[i]]
                print(config.show_tunel_sensors)
                #print(config.show_tunel_sensors_gas)  
    
    #config.show_tunel_sensors = ['NO2', 'O3', 'NO', 'CO']
    influx.plot_tunel(init_test, end_test, cartridge_list, resample_int)
    
    return redirect(url_for("tunel_test"))

# ----------------------------------------------------------
# Database
#-----------------------------------------------------------

@app.route('/view_db/', methods=['GET'])
def view_db():
    #print(f"Args: { request.args.get('database_filter') } ")
    #dict_return = dict()
    # Guardem el valor del filtre de la base de dades
    config.db_filter = request.args.get('database_filter')
    return redirect(url_for("data_base"))

@app.route('/view_afes/', methods=['GET'])
def view_afes():
    #print(f"Args: { request.args.get('database_filter') } ")
    #dict_return = dict()
    # Guardem el valor del filtre de la base de dades
    config.afes_filter = request.args.get('afes_filter')
    config.repairs['filter'] = request.args.get('filter')
    return redirect(url_for("afes"))

# ----------------------------------------------------------
# Repairs
#-----------------------------------------------------------

@app.route('/view_repairs/', methods=['GET'])
def view_repairs():
    #print(f"Args: { request.args.get('database_filter') } ")
    #dict_return = dict()
    # Guardem el valor del filtre de la base de dades
    config.repairs['filter'] = request.args.get('filter')
    return redirect(url_for("repairs"))

# ----------------------------------------------------------
# API
#-----------------------------------------------------------
@app.route('/api/v1/start/', methods=['POST'])
def start_api():
    if (not fridge.started):
        fridge.start()
               
    return jsonify({"start" : "done"}, 200)
    
@app.route('/api/v1/stop/', methods=['POST'])
def stop_api():
    if (fridge.stop()):
        return jsonify({"stop" : "done"}, 200)
    else:
        return jsonify({"stop" : "Error stopping process"}, 502)

@app.route('/update/', methods=['GET'])
def update():
    return jsonify(**fridge.tojson())

@app.route('/api/v1/sensors/temperature/<string:position>/', methods=['GET'])
def temperature(position):
    if position == "all":
        return jsonify(fridge.sensors_temp)
    elif position in fridge.sensors_temp:
        return jsonify(fridge.sensors_temp[position])
    
    return page_not_found(404)

@app.route('/api/v1/sensors/humidity/<string:position>/', methods=['GET'])
def humidity(position):
    if position == "all":
        return jsonify(fridge.sensors_hum)
    elif position in fridge.sensors_hum:
        return jsonify(fridge.sensors_hum[position])
    
    return page_not_found(404)


@app.route('/api/v1/relays/compressor/<string:state>', methods=['GET', 'POST'])
def api_all():
    return jsonify(fridge.sensors_temp)


@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404

@app.route("/tmp/<path:filename>")
def tmp(filename):
    print(filename)
    return send_from_directory(
        app.config['TMP_FOLDER'], 
        filename
    )


if __name__ == "__main__":
    #form.init("daniel.deandres@zizautomation.com", "1234567890")
    #form.set_param("idioma", "es")
    #init()
    if (len(sys.argv) > 1):
        app.run(port=int(sys.argv[1]), host='0.0.0.0', threaded=True, debug=True)    
    else:
        app.run(port=8080, host='0.0.0.0', threaded=True, debug=True)
