import requests
from requests.auth import HTTPBasicAuth
import json
import pandas as pd
import sys
import datetime
import excel

# panel intern
PROTOCOL = "https"
DOMAIN = "api.partsbox.com/api/1"
APIKEY = "APIKey partsboxapi_eft4gxcrn0j7t9gdn8k13s95wqe1f8dcee8d86f66bef16dd2d00bb06d83b36c1"

BOX_TOP = '7kq8e8xvcphe78f2jq4tnbstq7'
BOX_BOTTOM = 'ctfeytfxgckbjb158wnagvx7z2'
MAIN_BOARD = '14ds8xph88kgp9ae92panyxf7a'
COM_BOARD = '1584ntpf06k7ha2m3wndrg77fq'
COM_BOARD_USA = '5zznke8t7th5q9z72zjxg1vmgy'
SENSOR_BOARD = 'eddexxje8yh7bb0bedwbe27xtk'
EXTERNAL_BOARD = '6cef30jhvejq4a8mbx0af95t5d'
INTERFACE_BOARD = '0y2wn1za24ge5a0b7hqam23a3f'


class Api():
    
    def __init__(self):
        self.s = requests.Session()
        self.message = ""

    def jprint(self, obj):
        # create a formatted string of the Python JSON object
        text = json.dumps(obj, sort_keys=True, indent=4)
        print(text)
    
    def get(self, api_url):
        headers = {'Content-Type': 'application/json', 'Authorization' : APIKEY}
        response = self.s.get(url=api_url, headers=headers)
        if str(response.status_code).startswith("2"):
            response = response.json()
            if (response and response['partsbox.status/message'].upper() == 'OK' and response['partsbox.status/category'].upper() == 'OK'):
                return response['source/quantity']
        elif str(response.status_code).startswith("4"):
            response = response.json()
        
        return -1
        
            
    def get_stock_part(self, id):
        api_url = f"{PROTOCOL}://{DOMAIN}/part/stock?part/id={id}"
        response = self.get(api_url)
        if (response >= 0):
            return response
       
        return -1
    
    def get_all_stock(self):
        stocks = [BOX_TOP, BOX_BOTTOM, MAIN_BOARD, COM_BOARD, COM_BOARD_USA, SENSOR_BOARD, EXTERNAL_BOARD, INTERFACE_BOARD]
        stocks_str = ['BOX_TOP', 'BOX_BOTTOM', 'MAIN_BOARD', 'COM_BOARD', 'COM_BOARD_USA', 'SENSOR_BOARD', 'EXTERNAL_BOARD', 'INTERFACE_BOARD']
        result = []
        for i in stocks:
            result.append(self.get_stock_part(i))    
        return dict(zip(stocks_str, result))

if __name__ == "__main__":
   
    api = Api()
    """
    stocks = [BOX_TOP, BOX_BOTTOM, MAIN_BOARD, COM_BOARD, COM_BOARD_USA, SENSOR_BOARD, EXTERNAL_BOARD, INTERFACE_BOARD]
    for i in stocks:
        print(api_rest.get_stock_part(i))
    """
    #print(api.get_all_stock())