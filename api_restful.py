import requests
import json
import pandas as pd
import sys
import datetime
import excel
import sql_connector

# panel intern
PROTOCOL = "https"
DOMAIN = "internal.api.bettair.es"
PORT = 443

# Panel de Adrian
PROTOCOL_B = "http"
DOMAIN_B = "control.bettair.es"
PORT_B = 5858
DAYS_OUTDATE = 365 # 1 any

cert_path = '.\\cert\\'

class Api():
    
    def __init__(self):
        self.s = requests.Session()
        self.s.verify = cert_path + 'ca_cert.crt'
        self.s.cert = (cert_path + 'client_cert.crt', cert_path + 'client_cert.key')
        self.message = ""
        self.CARTRIDGE_TO_ORDER_DICT = {}

    def jprint(self, obj):
        # create a formatted string of the Python JSON object
        text = json.dumps(obj, sort_keys=True, indent=4)
        print(text)
    
    def post(self, api_url, body, msg_ok, msg_ko):
        response = self.s.post(url=api_url, json=body)
        #print(response.json())
        if str(response.status_code).startswith("2"):
            self.message = msg_ok
            #print(self.message)
            return True
        elif str(response.status_code).startswith("4"):
            if "error" in response.json():
                self.message = f"ERROR {response.status_code} : {response.json()['error']}"
            else:
                self.message = f"Status Code: {response.status_code}"
        else:
            self.message = f"Status Code: {response.status_code}"
        
        self.message += "\n" + msg_ko
        #print(self.message)
        return False
    
    def put(self, api_url, body, msg_ok, msg_ko):
        response = self.s.put(url=api_url, json=body)
        if str(response.status_code).startswith("2"):
            self.message = msg_ok
            print(self.message)
            return True
        elif str(response.status_code).startswith("4"):
            if "error" in response.json():
                self.message = f"ERROR {response.status_code} : {response.json()['error']}"
            else:
                self.message = f"Status Code: {response.status_code}"
        else:
            self.message = f"Status Code: {response.status_code}"
        
        self.message += "\n" + msg_ko
        print(self.message)
        return False
    
    def get(self, api_url, msg_ok, msg_ko):
        response = self.s.get(url=api_url)
        #print(response.json())
        if str(response.status_code).startswith("2"):
            self.message = msg_ok
            #print(self.message)
            #print(response.json())
            return response.json()
        elif str(response.status_code).startswith("4"):
            try:
                if "error" in response.json():
                    self.message = f"ERROR {response.status_code} : {response.json()['error']}"
                else:
                    self.message = f"Status Code: {response.status_code}"
            except Exception as e:
                msg_ko = str(e)
                print(e)
        else:
            self.message = f"Status Code: {response.status_code}"
        
        self.message += "\n" + msg_ko
        print(self.message)
        return None
    
    def update_serials(self, order, config, deviceSerial, cartridgeSerial, boardSerials):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/serial/lock/{order}"
        try:
            body = {
            "config": config,
            "cartridgeSerial" : int(cartridgeSerial),
            "deviceSerial" : int(deviceSerial),
            "boardSerials" : [int(serial) for serial in boardSerials]
            }
        except Exception as e:
            print (f"ERROR: {e}")
            return False
        #print(body)
        response = self.s.post(url=api_url, json=body)
        #print(response.status_code)
        if str(response.status_code).startswith("2"):
            print("Serials updated Succesful")
            return True
        elif str(response.status_code).startswith("4"):
            #print("Error updating serials")
            #print(response.json())
            #print(response.status_code)
            #self.jprint(response.json())
            if "error" in response.json():
                print(f"ERROR {response.status_code} : {response.json()['error']}")
                return False
            else:
                print(f"Status Code: {response.status_code}")
        else:
            print(f"Status Code: {response.status_code}")
        
        print("ERROR: FAIL UPDATING SERIALS TO DATABASE")
        return False

    # Response from Api
    """
    [
        {
            'id': '623da9bbfa464f351751a451', 
            'orderId': 'B000104', 
            'requisitioner': '623da9b3fa464f351751a44f', 
            'client': '623da93dfa464f351751a44e', 
            'requestDate': '2022-03-25T11:31:48.046Z', 
            'deliveryAddress': 'c. Fragua, 4-A\nBloques 2 y 9\n28760 Tres Cantos (Madrid) ', 
            'configurations': 
                [
                    {
                        'id': '623da9bbfa464f351751a450', 
                        'configurationId': 'BCONFIG-000012', 
                        'power': 'L', 
                        'tx': 'G', 
                        'nodeVersion': 26, 
                        'no2': 1, 
                        'o3': 1, 
                        'no': 1, 
                        'co': 1,
                        'so2': 1, 
                        'h2s': 1, 
                        'co2': 0, 
                        'nh3': 0, 
                        'voc': 0, 
                        'extras': 5, 
                        'configType': 0, 
                        'pm': 1, 
                        'units': 2, 
                        'solar': 1, 'anemometer': 
                        'Ultrasonic', 'powerSupply': 0
                    }
                ], 
            'dueDate': '2022-04-21T10:36:32.000Z', 
            'stations': ['BET00220029', 'BET00220030'], 
            'orderNumberRef': '108447', 
            'orderType': 'NEW_STATION', 
            'assembledStations': 2, 
            'state': 'SHIPPED', 
            'requireUnits': 2, 
            'unitsWithAfeLocked': 2, 
            'unitsWithOkAFEs': 2, 
            'boardSerials': [9000105, 9000106, 9000107, 9000108, 9000109, 9000110, 9000111, 9000112, 9000113, 9000114], 
            'cartridgeSerials': [7000101, 7000102], 
            'deviceSerials': [8000101, 8000102], 
            'sensorBoardIds': [], 
            'orderAFEs': ['26-000432', '14-000409', '14-000410', '26-000272'], 
            'orderWithRMA': False
        }, ...
    ]
    """        
    def get_order_list(self, state = ['ALL']):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/order/"
        #print(api_url)
        response = self.s.get(url=api_url)
        #print(response.status_code)
        orders_list = ["MANUAL"]
        if (response.status_code == 200):
            #self.jprint(response.json())
            orders = response.json()
            for order in orders:
                if 'ALL' in state:
                    orders_list.append(order["orderId"])
                elif order['state'] in state:
                    orders_list.append(order["orderId"])
        return orders_list
    
    
    """
    response from API:
    [
    {
        "afeSerial": [
            "26-000092"
        ],
        "boardSerials": [
            9002526,
            9002527,
            9002528,
            9002529,
            9002530
        ],
        "cartridgeSerial": 7000576,
        "deviceSerial": 8000566,
        "orderConfig": "E26AG00-CF040",
        "sensorBoardId": "E70EE4CB",
        "state": "CALIBRATING",
        "stateDates": {
            "ASSEMBLED": "2024-07-17T05:25:08.093Z",
            "CALIBRATING": "2024-07-17T12:00:00.000Z",
            "PRE-ASSEMBLED": "2024-07-16T13:54:58.110Z",
            "RECEIVED": "2024-07-12T11:22:56.393Z",
            "STARTED": "2024-07-16T13:44:54.241Z"
        },
        "stationName": "BET00240124"
    }
]
    """
    def get_orders_processing(self):
        now = datetime.datetime.now()
        #print(now)
        orders_columns = self.get_orders_processing_columns()
        print(f"COLUMNS: {datetime.datetime.now()-now}")
        orders_list = []
        orders = self.get_order_list(['RECEIVED','STARTED', 'PRE-ASSEMBLED', 'ASSEMBLED', 'CALIBRATING', 'CALIBRATED'])
        print(f"ORDER_LIST: {datetime.datetime.now()-now}")
        #print(orders)
        mysql = sql_connector.SqlConnector()
        for order in orders[1:]:
            #print(f" -------------- {order} ------------------")
            orders_state = self.get_order_state(order, complete_info = True)
            #print(len(orders_state))
            #for order in orders_state:python
            #    print((datetime.datetime.now(datetime.timezone.utc) - datetime.datetime.strptime(order['stateDates']['RECEIVED'].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S').replace(tzinfo=datetime.timezone.utc)).days)
            orders_state = [ order for order in orders_state if (datetime.datetime.now(datetime.timezone.utc) - datetime.datetime.strptime(order['stateDates']['RECEIVED'].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S').replace(tzinfo=datetime.timezone.utc)).days < 365]
            print(f"ORDER {order} STATE: {datetime.datetime.now()-now}")
            #print(orders_state)
            for order_state in orders_state:
                if (order_state['state'] == 'RECEIVED'):
                    continue
                elif (order_state['state'] == 'STARTED'):
                #if (order_state['state'] == 'RECEIVED' or order_state['state'] == 'STARTED'):
                    configs = self.get_order_config(order)
                    #print(configs)
                    print(f"ORDER {order} CONFIG: {datetime.datetime.now()-now}")
                    for config in configs:
                        #print(config)
                        order_list = ['']*len(orders_columns)
                        order_list[0] = order
                        order_list[orders_columns.index('orderConfig')] = config
                        order_list[orders_columns.index('state')] = orders_state[0]['state']
                        if config.startswith('E'):
                            order_list[orders_columns.index('extras')] = mysql.get_node_extras(config.split('-')[0])
                            order_list[orders_columns.index('changes')] = ",".join(mysql.get_assembling_alerts(config.split('-')[0]))
                        #print(order_list)
                        orders_list.append(order_list)
                    break
                else:
                    order_list = ['']*len(orders_columns)
                    order_list[0] = order
                    if (order_state['state'] != 'PACKAGED' and order_state['state'] != 'SHIPPED'):
                        for key in order_state:
                            #print(key)
                            if key in orders_columns:
                                order_list[orders_columns.index(key)] = ' '.join(str(x) for x in order_state[key]) if type(order_state[key]) is list else order_state[key]
                        if 'orderConfig' in order_state and order_state['orderConfig'].startswith('E'):
                            order_list[orders_columns.index('extras')] = mysql.get_node_extras(order_state['orderConfig'].split('-')[0])
                            order_list[orders_columns.index('changes')] = ",".join(mysql.get_assembling_alerts(order_state['orderConfig'].split('-')[0]))
                        if 'cartridgeSerial' in order_state:
                            tests = mysql.get_test_passed(order_state['cartridgeSerial'])
                            order_list[orders_columns.index('test_bettair')] = tests['test_traces']   
                            order_list[orders_columns.index('test_final')] = tests['test_final'] 
                        orders_list.append(order_list)
                        #print(f"***************** {order['orderId']} *******************")
                        #print(self.jprint(self.get_order_config(order["orderId"])))
        return orders_list
    
    def get_orders_processing_columns(self):
        return ['order_number', 'state', 'orderConfig', 'extras', 'changes', 'test_bettair', 'test_final', 'afeSerial', 'boardSerials', 'cartridgeSerial', 'deviceSerial', 'sensorBoardId']
    
    def get_order_duedate(self):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/order/"
        #print(api_url)
        response = self.s.get(url=api_url)
        #print(response.status_code)
        orders_list = ["MANUAL"]
        if (response.status_code == 200):
            #print(response.json())
            orders = response.json()
            for order in orders:
                orders_list.append([order["orderId"],order["dueDate"]])
        return orders_list
    
    def get_orderid_list(self):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/order/"
        response = self.s.get(url=api_url)
        #print(response.status_code)
        orders_list = ["MANUAL"]
        if (response.status_code == 200):
            #print(response.json())
            orders = response.json()
            for order in orders:
                orders_list.append(order["id"])
        return orders_list
    
    
    
    # return a list with serial numbers like this:
    # [[['CF040', '7000138'], ['CF040', '7000139'], ['CF040', '7000140'], ['E26AG00', '8000138'], ['E26AG00', '8000139'], ['E26AG00', '8000140'], ['G2002B001', '9000290'], ['G2003B001', '9000291'], ['G2001B001', '9000292'], ['G2106D001', '9000293'], ['G2004A001', '9000294'], ['G2002B001', '9000295'], ['G2003B001', '9000296'], ['G2001B001', '9000297'], ['G2106D001', '9000298'], ['G2004A001', '9000299'], ['G2002B001', '9000300'], ['G2003B001', '9000301'], ['G2001B001', '9000302'], ['G2106D001', '9000303'], ['G2004A001', '9000304']]]
    def get_part_number_serial_list(self, order):
        #order_list = self.get_order_list(False)
        #orderid_list = self.get_orderid_list()
        order_type = "order" if order.startswith("B") else "rma" if order.startswith("R") else ""
        #index = order_list.index(order)
        if not order_type:
            return []
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/serial/{order_type}"
        response = self.s.get(url=api_url)
        #print(response.json())
        serial_list = []
        if (response.status_code == 200):
            serials = response.json()
            for serial in serials:
                #print(serial[f"{order_type}Id"])
                if (serial[f"{order_type}Id"] == order):
                    serial_data = serial["data"] if type(serial["data"]) == list else [serial['data']]
                    for data in serial_data:
                        #print(serial["data"])
                        #group = []
                        if ("serials" in data):
                            data["serials"]
                            if ("cartridgeSerials" in data["serials"] and data["serials"]["cartridgeSerials"]):
                                for cartridge in data["serials"]["cartridgeSerials"]:
                                    #print(cartridge)
                                    serial_list.append([cartridge["cartridgeConfig"], str(cartridge["cartridgeSerial"])])
                            else:
                                limit = data["units"] if "units" in data else 1
                                for i in range(limit):
                                    serial_list.append(["x", "x"])
                                    
                            if ("deviceSerials" in data["serials"] and data["serials"]["deviceSerials"]):
                                for device in data["serials"]["deviceSerials"]:
                                    #print(device)
                                    serial_list.append([device["deviceConfig"], str(device["deviceSerial"])])
                            else:
                                limit = data["units"] if "units" in data else 1
                                for i in range(limit):
                                    serial_list.append(["x", "x"])
                            if ("boardSerials" in data["serials"] and data["serials"]["boardSerials"]):
                                for board in data["serials"]["boardSerials"]:
                                    #print(board)
                                    serial_list.append([board["parNumber"], str(board["boardSerial"])])
                            else:
                                limit = data["units"] if "units" in data else 1
                                for i in range(limit):
                                    serial_list.append(["x", "x"])
                            #serial_list.append(group)
                            #print(f"*{group}")
        #print(serial_list)
        
        return serial_list
    
    def get_sensors(self, afe):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/sensors/afe/{afe}"
        #print(api_url)
        response = self.s.get(url=api_url)
        #print (response)
        #print (response.json())
        if str(response.status_code).startswith("2"):
            #print (response.json())
            sensors_dict = response.json()['sensors']
            #print(sensors_dict)
            sensors_list = ["","","","","",""]
            for key in sensors_dict:
                sensors_list[int(key.split("_")[1])-1] = sensors_dict[key]
            return sensors_list
            
        return ["","","","","",""]
    
    def get_afes(self, cartridge):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/afe/cartridge/{cartridge}"
        response = self.s.get(url=api_url)
        #print (response.json())
        if str(response.status_code).startswith("2"):
            afes = response.json()
            return afes["afeSerialList"]
        return []

    def set_afes(self, cartridge, afes_list):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/serial/afe/lock"
        try:
            body = {
            "cartridgeSerial" : int(cartridge),
            "afeSerialList" : afes_list
            }
        except Exception as e:
            print (f"ERROR: {e}")
            return False
        #print(body)
        response = self.s.post(url=api_url, json=body)
        #print(response)
        #print(response.json())
        if str(response.status_code).startswith("2"):
            print("Afe Serials updated Succesful")
            return True
        elif str(response.status_code).startswith("4"):
            if "error" in response.json():
                print(f"ERROR {response.status_code} : {response.json()['error']}")
                return False
            else:
                print(f"Status Code: {response.status_code}")
        else:
            print(f"#Status Code: {response.status_code}")
        return False
    
    def set_station(self, cartridge, board_id):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/station"
        try:
            body = {
            "cartridgeSerial" : int(cartridge),
            "sensorBoardId" : board_id
            }
        except Exception as e:
            print (f"ERROR: {e}")
            return False
        #print(body)
        response = self.s.post(url=api_url, json=body)
        #print(response)
        #print(response.json())
        if str(response.status_code).startswith("2"):
            print("Afe Serials updated Succesful")
            return True
        elif str(response.status_code).startswith("4"):
            if "error" in response.json():
                print(f"ERROR {response.status_code} : {response.json()['error']}")
                return False
            else:
                print(f"Status Code: {response.status_code}")
        else:
            print(f"#Status Code: {response.status_code}")
        return False
    
    
    def get_cartridge_serials(self, order):
        nodes_state = self.get_order_state(order)
        cartridges = [str(cartridge) for cartridge in nodes_state.keys()]
        return cartridges

    def get_afes_list(self, order):
        afes_list = []
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/order/{order}"
        response = self.s.get(url=api_url)
        if str(response.status_code).startswith("2"):
            response_json = response.json()
            configs = response_json['config']
            for config in configs:
                for units in config['units']:
                    for afe in units['afes']:
                        #print(afe['serial'])
                        afes_list.append((afe['serial']))
        
        return afes_list
         
    """
    [
    {
        "_id": "62bd92566f4a3b8e06cb8ff1",
        "cart": "CART2",
        "pole": "B",
        "position": 1,
        "cartridgeSerial": "7000128",
        "initTime": "2022-05-16T12:00:00.000Z"
    },
    {
        "_id": "62beae336f4a3b8e06ea128a",
        "cart": "CART2",
        "pole": "C",
        "position": 1,
        "cartridgeSerial": "7000131",
        "initTime": "2022-06-27T12:00:00.000Z"
    },...
    ]
    """
    
    def get_part_number(self, serial_number):
        #print("1")
        #cartridge_to_order = self.get_cartridge_to_order_dictionary()
        order = self.CARTRIDGE_TO_ORDER_DICT[serial_number][0]
        part_numbers = self.get_part_number_serial_list(order)
        part_numbers_dict = {}
        for p in part_numbers:
            part_numbers_dict[p[1]] = p[0]
        part_number = part_numbers_dict[serial_number]
        return part_number
    
    def get_part_number_text(self, part_number):
        if part_number.startswith("E"):
            print("ERROR: No device available")
            return ""
        bin_str = f"{int(part_number[1:], 16) : 016b}"[2:]
        return bin_str
    
    def get_tunnel_information(self):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/tunnel/slots"
        #print(api_url)
        response = self.s.get(url=api_url)
        resp = {}
        #print (response)
        #print (response.json())
        #self.get_cartridge_to_order_dictionary()
        if str(response.status_code).startswith("2"):
            resp = response.json()
            i = 0
            for field in resp:
                if field['cartridgeSerial'].startswith("7"):
                    field['initTime'] = datetime.datetime.strptime(field['initTime'].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S').replace(tzinfo=datetime.timezone.utc)
                    field['finalTime'] = field['initTime'] + datetime.timedelta(days = 10)
                    date_now = datetime.datetime.now(datetime.timezone.utc)
                    field['restTime'] = (date_now - field['finalTime']).days
                    if (field['restTime'] < 0):
                        field['progress'] = int (( (12 + field['restTime']) / 12 ) * 100)
                    else:
                        field['progress'] = 100
                    field['fail'] = self.get_device_status(field['cartridgeSerial'])
                
            #print (response.json())
            #print(response.json()['state'])
        
        return resp
    
    def get_tunnel_information2(self):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/order"
        response_order = self.s.get(url=api_url)
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/tunnel/slots"
        response_tunnel = self.s.get(url=api_url)
        resp = [['State', 'Order', 'Description', 'Cartridge Serial', 'Init Calibration', 'Final Calibration', 'Rest time', 'Progress', 'Failures']]
        if str(response_order.status_code).startswith("2") and str(response_tunnel.status_code).startswith("2"):
            resp_order = response_order.json()
            #self.jprint(resp_order)
            resp_tunnel = response_tunnel.json()
            #self.jprint(resp_tunnel)
            # creem un diccionari per relacionar el serial cartridge number amb la data de inici de calibració
            initTime = {device['cartridgeSerial'] : device['initTime'] for device in resp_tunnel}
            #print(initTime)
            #self.jprint(resp_order)
            #i = 0
            for field in resp_order:
                #if field['orderId'] == 'B000266':
                #    self.jprint(field)
                if (field['state'] == 'CALIBRATING' or field['state'] == 'CALIBRATED'):
                    for cartridge in field['cartridgeSerials']:
                        if (str(cartridge) in initTime):
                            init = datetime.datetime.strptime(initTime[str(cartridge)].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S').replace(tzinfo=datetime.timezone.utc)
                            final = init + datetime.timedelta(days = 10)
                            date_now = datetime.datetime.now(datetime.timezone.utc)
                            rest = (date_now - final).days
                            if (rest < 0):
                                progress = int (( (12 + rest) / 12 ) * 100)
                            else:
                                progress = 100
                            fail = self.get_device_status(str(cartridge))
                            if "orderDescription" in field:
                                orderDescription = field['orderDescription'] if 'orderDescription' in field else ''
                                resp.append([field['state'], field['orderId'], orderDescription, str(cartridge), init, final, rest, progress, fail])
        #print(resp)
        return resp
        
    
    # Response:
    # get from API
    # {'order': 'B000133', 'state': 'PRE-ASSEMBLED', 'companyName': 'DNOTA', 'orderType': 'NEW_STATION', 'orderDescription' : 'DIBA'}
    # 'orderDescription' just if exist
    # get from response
    # 'DNOTA'
    def get_order_info(self, order):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/order/{order}/info"
        #print(api_url)
        response = self.s.get(url=api_url)
        #print(response.json())
        company = ""
        if str(response.status_code).startswith("2"):
            return response.json()
        elif str(response.status_code).startswith("4"):
            if "error" in response.json():
                print(f"ERROR {response.status_code} : {response.json()['error'] if 'error' in response.json() else '---'}")
                return ""
            else:
                print(f"Status Code: {response.status_code}")
        else:
            print(f"#Status Code: {response.status_code}")
            
        return None
    
    # Response
    # {'7000101': ['B000104', 'SHIPPED', 'DNOTA'], '7000102': ['B000104', 'SHIPPED', 'DNOTA'], '7000103': ['B000105', 'SHIPPED', 'SGS'], '7000104': ['B000105', 'SHIPPED', 'SGS'], '7000105': ['B000105', 'SHIPPED', 'SGS'],....
    def get_cartridge_to_order_dictionary(self):
        orders = self.get_order_list()
        orders = [order for order in orders if order.startswith("B")]
        #print(orders)
        orders_dict = dict()          
        for order in orders:
            nodes_state = self.get_order_state(order)
            #print(f"{order}: {nodes_state}")
            #cartridges = self.get_cartridge_serials(order)
            cartridges = [str(cartridge) for cartridge in nodes_state.keys()]
            info = self.get_order_info(order)
            #print(cartridges)
            for cartridge in cartridges:
                #print(cartridge)
                if int(cartridge) in nodes_state:
                    orders_dict[cartridge] = [order, nodes_state[int(cartridge)], info['companyName'], info['orderDescription'] if 'orderDescription' in info else '']
                else:
                    orders_dict[cartridge] = [order, 'NONE', 'NONE']
        self.CARTRIDGE_TO_ORDER_DICT = orders_dict
        return orders_dict
    
    # Return dict for each node and his state
    # Response from api:
    # {'stationName': 'BET00210070', 'state': 'SHIPPED', 'stateDates': {'RECEIVED': '2022-04-05T09:14:34.332Z', 'CALIBRATING': '2022-06-01T12:00:00.000Z', 'CALIBRATED': '2022-07-06T12:00:00.000Z', 'PACKAGED': '2022-09-07T09:15:33.417Z', 'SHIPPED': '2022-09-07T09:15:34.933Z'}, 'cartridgeSerial': 7000106, 'deviceSerial': 8000106, 'boardSerials': [9000130, 9000131, 9000132, 9000133, 9000134], 'orderConfig': 'E26LGA0-CFE40', 'afeSerial': ['26-000374', '14-000414']}
    # Response
    # {7000151: 'SHIPPED', 7000152: 'SHIPPED', 7000150: 'SHIPPED', 7000144: 'SHIPPED', 7000145: 'SHIPPED', 7000146: 'SHIPPED', 7000147: 'SHIPPED', 7000148: 'SHIPPED', 7000149: 'SHIPPED', 7000153: 'SHIPPED'}
    def get_order_state(self, order, complete_info = False):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/order/{order}/station"
        #print(api_url)
        response = self.s.get(url=api_url)
        state_dict = dict()
        if str(response.status_code).startswith("2"):
            response = response.json()
            #self.jprint(response)
            for node in response:
                if 'cartridgeSerial' in node:
                    state_dict[node['cartridgeSerial']] = node['state']
        elif str(response.status_code).startswith("4"):
            if "error" in response.json():
                print(f"ERROR {response.status_code} : {response.json()['error']}")
                return False
            else:
                print(f"Status Code: {response.status_code}")
        else:
            print(f"#Status Code: {response.status_code}")
            
        return response if complete_info else state_dict
    
    # Response
    # E26AG00-CF040
    def get_order_config(self, order):
        resp_list = []
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/serial/order/"
        #print(api_url)
        response = self.s.get(url=api_url)
        if str(response.status_code).startswith("2"):
            response = response.json()
            #self.jprint(response)
            for config in response:
                #print(config['orderId'])
                if (config['orderId'] == order):
                    if 'data' in config:
                        for data in config['data']:
                            #self.jprint(data)
                            for conf in data['serials']['cartridgeSerials']:
                                #print(conf)
                                if not conf['locked']:
                                    #print(conf['config'])
                                    resp_list.extend([conf['config']])
        elif str(response.status_code).startswith("4"):
            if "error" in response.json():
                print(f"ERROR {response.status_code} : {response.json()['error']}")
                return []
            else:
                print(f"Status Code: {response.status_code}")
        else:
            print(f"#Status Code: {response.status_code}")
            
        return resp_list
    
    # Return dict for each node and his state
    # Response from api:
    # {'stationName': 'BET00210070', 'state': 'SHIPPED', 'stateDates': {'RECEIVED': '2022-04-05T09:14:34.332Z', 'CALIBRATING': '2022-06-01T12:00:00.000Z', 'CALIBRATED': '2022-07-06T12:00:00.000Z', 'PACKAGED': '2022-09-07T09:15:33.417Z', 'SHIPPED': '2022-09-07T09:15:34.933Z'}, 'cartridgeSerial': 7000106, 'deviceSerial': 8000106, 'boardSerials': [9000130, 9000131, 9000132, 9000133, 9000134], 'orderConfig': 'E26LGA0-CFE40', 'afeSerial': ['26-000374', '14-000414']}
    # Response
    # {7000151: 'SHIPPED', 7000152: 'SHIPPED', 7000150: 'SHIPPED', 7000144: 'SHIPPED', 7000145: 'SHIPPED', 7000146: 'SHIPPED', 7000147: 'SHIPPED', 7000148: 'SHIPPED', 7000149: 'SHIPPED', 7000153: 'SHIPPED'}
    def get_order_state_dates(self, order):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/order/{order}/station"
        #print(api_url)
        response = self.s.get(url=api_url)
        state_dict = dict()
        if str(response.status_code).startswith("2"):
            response = response.json()
            #print(response)
            for node in response:
                if 'cartridgeSerial' in node:
                    state_dict[node['cartridgeSerial']] = node['stateDates']
        elif str(response.status_code).startswith("4"):
            if "error" in response.json():
                print(f"ERROR {response.status_code} : {response.json()['error']}")
                return False
            else:
                print(f"Status Code: {response.status_code}")
        else:
            print(f"#Status Code: {response.status_code}")
            
        return state_dict
    
    def add_new_afe(self, body):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/stock/afe"
        msg_ok = f"Afe {body['serial']} created successfully"
        msg_ko = f"Fail creating Afe {body['serial']}"
        return self.post(api_url, body, msg_ok, msg_ko)        
    
    def get_code_text(self, code):
        decode = {
        '21' : 'AFES',
        '25' : 'NH3',
        '30' : 'SI7021 - BMEX80',
        '31' : 'STH85',
        '32' : 'CO2',
        '33' : 'wind',
        '34' : 'VOC',
        '35' : 'meteo',
        '40' : 'PMs',
        '41' : 'PM+',
        '50' : 'noise',
        'e0' : 'telemetria',
        'f0' : 'RSSI',
        'f2' : 'GPS',
        }
        
        if code in decode:
            return decode[code]
        return ''
    
    # Return dict for each node and his state
    def get_device_status(self, cartridge_serial):
        api_url = f"{PROTOCOL_B}://{DOMAIN_B}:{PORT_B}/device/{cartridge_serial}" 
        #print(api_url)
        msg_ok = f"Received {cartridge_serial} information"
        msg_ko = f"Fail getting {cartridge_serial} information"
        response = self.get(api_url, msg_ok, msg_ko)
        #print(response)
        #part_number = self.get_part_number(cartridge_serial)
        #print(f"{cartridge_serial} - {part_number} - {self.get_part_number_text(part_number)}")
        fail_txt = ""
        if response and 'lastRawData' in response:
            keys = list(response['lastRawData'].keys())
            for key in keys:
                if key.startswith("f"):
                    continue
                #print(key)
                last_raw_data = datetime.datetime.strptime(response['lastRawData'][key].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S')
                #print(last_raw_data)
                date_now = datetime.datetime.utcnow()
                #print(date_now)
                inactivity = (date_now - last_raw_data)
                #print(inactivity)
                max_diff =  datetime.timedelta(minutes = 15)
            
                if inactivity > max_diff:
                    #print(f"OK {inactivity} < {max_diff}")
                    fail_txt += self.get_code_text(key) + f"({inactivity}), "
                #else:
                #    print(f"NOT OK {inactivity} >= {max_diff}")
        
        #print(fail_txt)
        if fail_txt:
            return fail_txt[:-2]
        return ""
    
    def get_statistics(self):   
        pd 
        duedates = self.get_order_duedate()
        duedates = [duedate for duedate in duedates if duedate[0].startswith("B")]
        n = 0
        total = 0
        calibracio = 0
        assembly = 0
        timedDelivery = 0
        for duedate in duedates:
            order = duedate[0]
            #duedate = duedate[1]
            info = self.get_order_state_dates(order)
            for i in info:
                if info[i]['RECEIVED'].startswith('2023'):
                    try:
                        received = datetime.datetime.strptime(info[i]['RECEIVED'].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S').replace(tzinfo=datetime.timezone.utc)
                        started = datetime.datetime.strptime(info[i]['STARTED'].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S').replace(tzinfo=datetime.timezone.utc)
                        preasembled = datetime.datetime.strptime(info[i]['PRE-ASSEMBLED'].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S').replace(tzinfo=datetime.timezone.utc)
                        assembled = datetime.datetime.strptime(info[i]['ASSEMBLED'].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S').replace(tzinfo=datetime.timezone.utc)
                        calibrating = datetime.datetime.strptime(info[i]['CALIBRATING'].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S').replace(tzinfo=datetime.timezone.utc)
                        calibrated = datetime.datetime.strptime(info[i]['CALIBRATED'].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S').replace(tzinfo=datetime.timezone.utc)
                        packaged = datetime.datetime.strptime(info[i]['PACKAGED'].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S').replace(tzinfo=datetime.timezone.utc)
                        shipped = datetime.datetime.strptime(info[i]['SHIPPED'].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S').replace(tzinfo=datetime.timezone.utc)
                        duedate = datetime.datetime.strptime(duedate[1].split(".")[0].replace("T"," "), '%Y-%m-%d %H:%M:%S').replace(tzinfo=datetime.timezone.utc)
                        #print(f"{n} {received} {started} {preasembled} {assembled} {calibrating} {calibrated} {packaged} {shipped}")
                        total += (shipped-received).days
                        calibracio += (calibrated-calibrating).days
                        assembly += (calibrated-received).days
                        timedDelivery += 1 if (shipped-duedate).days < 0 else 0
                        #total += shipped-started
                        #print(total)
                        print(f"{n} Total: {(shipped-received).days} Calibració: {(calibrated-calibrating).days} Assembly: {(calibrated-received).days} Timed Delivery: {(shipped-duedate).days}")
                        n += 1
                    except Exception as e:
                        #print(e)
                        continue
        print(f"{n} Total: {total/n} Calibració: {calibracio/n} Assembly: {assembly/n} Timed Delivery: {(timedDelivery/n)*100}")
            
    
    """
    def post_add_new_afe(self, body):
        api_url = f"{PROTOCOL}://{DOMAIN}:{PORT}/repair/sensor"
        body = {
                "sensorSerial": serial_sensor,
                "sensor_type": type_sensor
                }
        msg_ok = f"Sensor {type_sensors[i]}, serial  {serial_sensors[i]}"
        msg_ko = f"Fail changing sensor type of sensor {serial_sensor} to {type_sensor}"
        return self.post(api_url, body, msg_ok, msg_ko)    
    """    
        
"""
api_url = "https://jsonplaceholder.typicode.com/todos/1"
response = requests.get(api_url)
print(response)
print(response.json())
print(response.status_code)
"""
"""
s = requests.Session()
#s.cert = '.\\cert\\ca_cert.pem'
print(s)
api_url = "https://internal.api.bettair.es:443/serial/"
response = s.get(api_url, verify=False)
print(response)
print(response.json())
print(response.status_code)
"""

"""
api_url = "https://internal.api.bettair.es:443//serial/lock/B000103"
body = {
"config":"E26HL4-CC4004",
"cartridgeSerial":"7000100",
"deviceSerial":"8000100",
"boardSerials":["9000100","9000101","9000102","9000103","9000104"]
}
response = requests.post(api_url, json=body, verify=False)
print(response.json())
"""

if __name__ == "__main__":
    # argv[1]: instancia
    # argv[2]: user
    api_rest = Api()
    #api_rest.get_statistics()
    
    #print(orders)
             
    
    #print(api_rest.get_cartridge_serials("B000121"))
    #print(api_rest.get_serial_list("B000105"))
    #print(api_rest.get_part_number_serial_list("B000133"))
    #api_rest.update_serials("B000106" ,"E26A4G-CF0400", 8000104, 7000103, [9000120, 9000116, 9000117, 9000118, 9000124])
    #d = {'cartridgeSerial': 7000112, 'afeSerialList': ['26-000370', '14-000294']}
    #d = {'cartridgeSerial': 7000114, 'afeSerialList': ['25-000701']}
    #api_rest.set_afes(d['cartridgeSerial'], d['afeSerialList'])
    
    #d = {'cartridgeSerial': 7000122, 'boardID': '0076D301'}
    #api_rest.set_station(7000122, '0076D301')
    #print(api_rest.get_sensors('26-000317'))
        
    """
    afes = [f"26-000{i}" for i in range(514, 527)]
    i = 1
    for afe in afes:
        print(f"({i}) {afe}")
        print(api_rest.put_afe_location(afe, f"SBOX_4-7-{i}"))
        print(api_rest.put_afe_state(afe, "NOT_TESTED"))
        print(api_rest.get_afe_location(afe))
        i += 1
        input("")
    """
    #print(api_rest.get_stock_ok())
    #print(api_rest.get_stock_ko())
    #print(api_rest.get_afes('7000137'))
    #api_rest.get_serial_list('B000110')
    
    #api_rest.put_afe_location(str(sys.argv[1]), str(sys.argv[2]))
    #api_rest.get_tunnel_information()
    
    #print(api_rest.get_cartridge_to_order_dictionary())
    #api_rest.get_part_number('7000128')
    
    """
    afes = ["10-001164", "10-001165", "10-001171"]
    for afe in afes:
        api_rest.put_afe_location(afe, "TESTING")
        api_rest.put_afe_state(afe, "TESTING", " ")
    """ 
    """  
    afes = ["10-001164"]
    for afe in afes:
        api_rest.put_afe_location(afe, "SBOX_2-2-2")
        api_rest.put_afe_state(afe, "NOT_OK", " ")
    """
    #api_rest.put_afe_state('26-000504', 'NOT_OK', '*SN3(NO)')
    #api_rest.put_afe_location('10-001165', "0076AE51")
    
    #e = excel.Excel(r'C:\Users\Daniel Gaspar\BETTAIR CITIES S.L\PRODUCTION Site - Documents\Sensórica\Alphasense calibration files\72308\72308_Bettair_070722\72308_Bettair_070722')
    #for i in e.get_afes():
        #print(i)
    #    serial_afe = i[0]
    #    serial_sensors = i[1]
    #    type_sensors = i[2]
    #    if (serial_afe.startswith("1")):
    #        api_rest.post_repair_sensor(serial_sensors, type_sensors)
    #    api_rest.post_repair_sensors(serial_afe, serial_sensors, type_sensors)
        #input('')
    
    #print(api_rest.get_order_state("B000118"))
    #print(api_rest.get_order_state("B000106"))
    #print(api_rest.get_cartridge_to_order_dictionary())
    #api_rest.get_order_list()
    #afes = ['26-000493']
    #for afe in afes:
    #    api_rest.put_afe_location(afe, "00772459")
    #api_rest.post_repair_afe(7000126, ['26-000480'])
    #api_rest.put_afe_location('26-400', "TESTING")
    #print(api_rest.message)
    #api_rest.put_afe_state('26-000400', "NOT_OK")
    #print(api_rest.message)
    #print(api_rest.get_tunnel_information())
    #api_rest.get_order_list()
    #print(api_rest.get_tunnel_information2())
    #api_rest.get_device_info('7000163')
    #api_rest.get_cartridge_to_order_dictionary()
    #print(api_rest.get_device_status("7000491"))
    #status = api_rest.get_device_status("7000149")
    #print(status)
    #print(api_rest.get_order_info('B000158'))
    
    #orders = api_rest.get_orders_processing()
    #print(orders)
    """
    for order in orders:
        print(order)
        for l in order[4].split(','):
            print(l)
    """
    #print(orders)
    #api_rest.jprint(api_rest.get_orde
    # r_state('B000273', complete_info = True))
    #print(api_rest.get_order_config('B000266'))
    #print(api_rest.get_order_state('B000265'))
    
    print(api_rest.get_tunnel_information2())