FROM python:3.10

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install item-manager --trusted-host nexus.bettair.es --extra-index-url https://nexus.bettair.es/repository/pypi-private/simple/
RUN pip freeze

EXPOSE 5000

COPY . .

RUN ls

CMD [ "python", "-u", "./anomaly.py" ]