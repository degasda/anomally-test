import i2c
import pandas as pd
import matplotlib.pyplot as plt
import time, datetime
import sql_connector
import threading
import influx_test

INTERVAL = 10 # seconds
INTERVAL_SAVE = 2
MINUTE = 60
HOUR = 60*MINUTE

MAX_SENSORS = 8

class Co2():
    
    def __init__(self):
        self.running_co2 = False
        self.running_time = 0
        self.percent = 0
        self.co2_detected = [False]*MAX_SENSORS
        self.t = None
        self.co2 = ['']*8
        self.temp = ['']*8
        self.hum = ['']*8
     
    def init_co2_sensors(self, co2_list):
        print("Initilizing sensors")
        self.co2 = ['']*8
        self.temp = ['']*8
        self.hum = ['']*8
        co2_list_enabled = [True]*MAX_SENSORS
        for ch in range(MAX_SENSORS):
            print()
            print("*************************************")
            print(f"***** Initilizing sensor ch {ch+1} *******")
            print("*************************************")
            time.sleep(0.01)
            if (i2c.select_i2c_channel(ch+1)):
                time.sleep(0.01)
                if (not i2c.stop_continuos_measurement()):
                    print(f"Fail stopping sensor channel {ch+1}")
                    co2_list_enabled[ch] = False
                    continue
                time.sleep(0.01)
                if (not i2c.write_ASC(False)):
                    print(f"Fail deactivating ASC mode channel {ch+1}")
                    co2_list_enabled[ch] = False
                    continue
                time.sleep(0.01)
                if (not i2c.set_interval(INTERVAL_SAVE)):
                    print(f"Fail setting interval sensor channel {ch+1}")
                    co2_list_enabled[ch] = False
                    continue
                time.sleep(0.01)
                if not (i2c.get_interval()[1] == str(hex(INTERVAL_SAVE)).replace('0x','')):
                    print(f"Fail interval sensor not updated succesfully channel {ch+1}")
                    co2_list_enabled[ch] = False
                    continue
                time.sleep(0.01)
                if (not i2c.trigger_continuos_measurement()):
                    print(f"Fail triggering continuos measuremnet channel {ch+1}")
                    co2_list_enabled[ch] = False
                    continue
                time.sleep(0.01)
                if (not i2c.set_temperature_offset(0)):
                    co2_list_enabled[ch] = False
                    continue
                time.sleep(0.01)
                """
                if (not i2c.write_FRC(0)):
                    co2_list_enabled[ch] = False
                    continue
                time.sleep(0.01)
                if (not i2c.soft_reset()):
                    co2_list_enabled[ch] = False
                    continue
                """
            else:
                co2_list_enabled[ch] = False
                #print(f"Initialized channel {ch+1} Done")
        #print(co2_list_enabled)
        for i, enabled in enumerate(co2_list_enabled):
            if not enabled:
                print(f"Disabled channel {i+1} sensor {co2_list[ch]}")
        self.co2_detected = co2_list_enabled
        return co2_list_enabled

    def start_co2_test(self, duration_s, co2_list):
        self.percent = 0
        self.t = threading.Thread(target=self.worker, args=(duration_s, co2_list))
        self.t.start()
    
    def stop_co2_test(self):
        self.running_co2 = False
        self.t.join()
        print("Thread stoped")

    def calibrate_temp_hum(self, temp_hum, temp, ch):
        for i in range (10):
            if (temp_hum and temp):
                print(f"Evaluating temperature offset to channel {ch+1}")
                dif = round(temp-temp_hum[0],2)
                if (i2c.set_temperature_offset(dif)):
                    #time.sleep(0.1)
                    #if (i2c.soft_reset()):
                    print(f"SET OFFSET TO {dif} TO CHANNEL {ch+1}")
                    bContinue = True
                    return True
                else:
                    print(f"({10-i}) Fail forcing recalibration temperature channel {ch+1}. Retrying...")
                    time.sleep(1)
        print(f"FAIL RECALIBRATING")
        return False
    
    def calibrate_co2(self, co2, ch):
        for i in range (10):
            if (i2c.write_FRC(co2)):
                print(f" FORCE RECALIBRATION TO {co2} ppm channel {ch+1}")
                #print(i2c.read_FRC())
                return True
            else:
                print(f"({10-i}) Fail forcing recalibration sensor channel {ch+1}. Retrying...")
                time.sleep(1)
        
        print(f"FAIL RECALIBRATING")
        return False
    
    def calibrate(self, co2, temp_hum, temp, ch):
        print(f"calibrate({co2}, {temp_hum}, {temp}, {ch})")
        if (self.calibrate_temp_hum(temp_hum, temp, ch)):
            time.sleep(0.1)
            if (self.calibrate_co2(co2, ch)):
                return True
        
    def print_debug_traces(self, date_now, date_start, duration, d, d_temp, d_hum):
        print()
        print(f" ********* TIME ELAPSED : {date_now - date_start} **************")
        print(f" ********* DURATION : {duration} **************")
        print()
        print(f"co2: {pd.Series(d)}")
        print(f"temperature: {pd.Series(d_temp)}")
        print(f"humidity: {pd.Series(d_hum)}")

    
    def worker(self, duration_s, co2_list):
        # init values
        self.running_co2 = True
        co2_list_enabled = self.init_co2_sensors(co2_list)
        #time.sleep(INTERVAL+1)
        df = pd.DataFrame(columns=['time'])
        df_temp = pd.DataFrame(columns=['time'])
        df_hum = pd.DataFrame(columns=['time'])
        date_start = datetime.datetime.now()
        date_now = datetime.datetime.now()
        print()
        print("Starting Measuremnets")
        #for i in range (duration):
        duration = datetime.timedelta(seconds = duration_s)
        half_duration = datetime.timedelta(seconds = int(duration_s/2))
        calibrated = False
        # Run test for a duration time
        while (date_now - date_start < duration):
            # stop the test
            if (not self.running_co2):
                break
            calibrate = False
            # Eval lapse time
            self.running_time = int((date_now - date_start).total_seconds())
            self.percent = int ((self.running_time / duration_s)*100)
            # execute once each interval time
            if (datetime.datetime.now() - date_now >= datetime.timedelta(seconds = INTERVAL)):
                date_now = datetime.datetime.now()
                # Calibrate
                if ((date_now - date_start > half_duration) and not calibrated):
                    #print("Calibrate set True")
                    calibrate = True
                    temp_hum = i2c.get_temp_hum_average()
                    ifx = influx_test.Influx()
                    co2_master = ifx.get_data_co2()
                
                d = {'time' : date_now}
                d_temp = {'time' : date_now}
                d_hum = {'time' : date_now}
                # Select channel in i2c hub
                for ch in range(len(co2_list)):
                    if (co2_list_enabled[ch]):
                        if (i2c.select_i2c_channel(ch+1)):
                            # Read values from sensor
                            print(f"\n *** Reading channel {ch+1} ***")
                            temp = None
                            if (i2c.is_data_ready()):
                                resp = i2c.read_co2_temp_hum()
                                if (resp):
                                    #Convert values into floats
                                    co2 = i2c.convert_co2(resp)
                                    temp = i2c.convert_temp(resp)
                                    hum = i2c.convert_hum(resp)
                                    # Save data into dataframe
                                    d[f'{co2_list[ch]}'] = co2
                                    self.co2[ch] = round(co2,1)
                                    d_temp[f'{co2_list[ch]}'] = temp
                                    self.temp[ch] = round(temp,1)
                                    d_hum[f'{co2_list[ch]}'] = hum
                                    self.hum[ch] = round(hum,1)
                            
                            # Calibrate at the middle of the test
                            if (calibrate):
                                print(f"******************** CALIBRATING CH {ch+1} ***************")
                                print(f"calibrate: {calibrate}")
                                if (self.calibrate(co2_master, temp_hum, temp, ch)):
                                    calibrated = True
                                    print(f"calibrated: {calibrated}")
                                
                    else:
                        print("NOT Ready")
                
                # Print for console last measure
                self.print_debug_traces(date_now, date_start, duration, d, d_temp, d_hum)
                
                # Add to the data frame last datas            
                new_row = pd.Series(d)
                df = pd.concat([df, new_row.to_frame().T], ignore_index=True)
                new_row = pd.Series(d_temp)
                df_temp = pd.concat([df_temp, new_row.to_frame().T], ignore_index=True)
                new_row = pd.Series(d_hum)
                df_hum = pd.concat([df_hum, new_row.to_frame().T], ignore_index=True)
           
            if (not self.running_co2):
                break
            time.sleep(1)
            
        if (self.running_co2)    :
            db = sql_connector.SqlConnector()
            db.insert_anomaly_to_db(date_start, co2_list, df, df_temp, df_hum)
            self.running_co2 = False
        print("CO2 TEST FINISHED")
            
if __name__ == "__main__":
    co2 = Co2()
    #for i in range(100):
    """
    temp_hum = [26.58, 46.2]
       
    for i in range(4):
        time.sleep(1)
        if (i2c.select_i2c_channel(i+1)):
            time.sleep(1)
            co2.calibrate(1400, temp_hum, 30, i)
        else:
            print("++++++++++++++++++++++++++++++++++++++++")
    time.sleep(1)
    for i in range(100):
        time.sleep(2)
        vec = []
        for i in range(4):
            i2c.select_i2c_channel(i+1)
            time.sleep(0.1)
            if (i2c.is_data_ready()):
                resp = i2c.read_co2_temp_hum()
                if (resp):
                    #Convert values into floats
                    vec.append(i2c.convert_co2(resp))
        print(f"**** {vec} *******")
    """                
                
    #temp_hum = i2c.get_temp_hum_average()
    #print(f"**** {temp_hum} *******")
    #print(i2c.get_crc8("0002"))
    #co2.start_co2_test(3*MINUTE, ['1', '2', '3', '4', '5', '6', '7', '8'])
    #start_co2_test(30*HOUR, ['5879201', '590009x', '7059457', '5899812', '7025166', '3366894', '5900185'])
    co2.worker(1*MINUTE, ['1', '2', '3', '4', '5', '6', '7', '8'])
    #print(['sensor_'+str(i+1) for i in range(8)])
    #start_co2_test(1*HOUR, [str(i+1) for i in range(8)])
    #start_co2_test(HOUR, ['7059457'])
    #start_co2_test(HOUR, ['5899812', '7025166'])
    #time.sleep(0.5)
    #for i in range(8):
    #    i2c.select_i2c_channel(i+1)
    #print(i2c.is_co2_test_available())
    """
    temp_hum = i2c.get_temp_hum_average()
    
    time.sleep(1)
    print(f"**** {temp_hum} *******")
    co2_list = ['1', '2', '3', '4', '5', '6']
    co2_list_enabled = [True]*6
    for ch in range(len(co2_list)):
        if (co2_list_enabled[ch]):
            if (i2c.select_i2c_channel(ch+1)):
                #i2c.set_temperature_offset(0)
                #i2c.write_FRC(0)
                #if ((date_now - date_start > half_duration_min) and (date_now - date_start < half_duration_max)):
                    #if (i2c.write_FRC(400)):
                    #    print(f" FORCE RECALIBRATION TO 400 ppm channel {ch+1}")
                    #else:
                    #    print(f"Fail forcing recalibration sensor channel {ch+1}")
                # Read values from sensor
                print(f"\n *** Reading channel {ch+1} ***")
                if (i2c.is_data_ready()):
                    resp = i2c.read_co2_temp_hum()
                    if (resp):
                        #Convert values into floats
                        co2 = i2c.convert_co2(resp)
                        temp = i2c.convert_temp(resp)
                        hum = i2c.convert_hum(resp)
                        print(temp_hum[0])
                        print(temp)
                        dif = round(temp-temp_hum[0],2)
                        print(dif)
                        #print(i2c.read_temperature_offset())
                        i2c.write_FRC(0)
                        if(i2c.set_temperature_offset(0)):
                            if (i2c.soft_reset()):
                                print(f"SET TEMP CH {ch+1}")
                                #print(i2c.read_temperature_offset())
    """
                     
    