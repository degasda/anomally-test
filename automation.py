import threading
import time, datetime
from datetime import timezone
import random
import mod
import influx_test as flux

# Cicle inicial d'enfriament de 120min
# 4 cicles de 150 minuts on la 75 min está refredant i 75 min escalfant.
COMPRESSOR = [True, False, True, False, True, False, True, False]
CICLE_HOT_TIME = 70
CICLE_COLD_TIME = 120
INITIAL_CICLE = 150
FINAL_CICLE = 75
NUM_CICLES = 3
CICLES = [CICLE_HOT_TIME, CICLE_COLD_TIME]*NUM_CICLES
CICLE_TIME = [INITIAL_CICLE+sum(CICLES[:i]) for i in range(len(CICLES)+1)]
CICLE_TIME = CICLE_TIME + [CICLE_TIME[-1] + FINAL_CICLE]
# CICLE_TIME = [150, 250, 370, 470, 590, 690, 810, 910, 1030, 1105]
MAX_TEMPERATURE = 25
MIN_TEMPERATURE = -10
#HISTERESI = 1

class Automation():
    def __init__(self) -> None:

        # Create some test data for our catalog in the form of a list of dictionaries.
        self.sensors_temp = {
            'main' : 20,
            'backup' : 20
        }
        
        self.sensors_hum = {
            'main' : 70,
            'backup' : 70,
        }

        self.relays = {
            'compressor' : False,
            'lights' : False,
            'heater' : False
        } 
        
        self.registers = {
            'temperature' : [],
            'temperature2' : [],
            'humidity' : [],
            'humidity2' : [],
            'compressor' : [],
            'heater' : []
        }   
        
        self.modbus = mod.Modbus()
                
        self.labels_time = [i for i in range(CICLE_TIME[-1])]
        self.started = False
        self.start_date = None
        #self.start_date =  datetime.datetime(2024,9,6,8,48,0, tzinfo=timezone.utc)

        self.time_elapsed_s = 0
        self.initialize()
        self.alert = None
        self.compressor_last_state = False
        
              
    # ------------------------------------------
    #  ----------- TEMPERATURE -----------------
    # ------------------------------------------
    # position: "main", "backup"
    # Auxiliar functions to manage structures
    def get_temp(self, position):
        if position in self.sensors_temp:
            return self.sensors_temp[position]
        return None

    def set_temp(self, position, value):
        if position in self.sensors_temp:
            self.sensors_temp[position] = value
        return None
    
    # ------------------------------------------
    #  -------------- HUMIDITY -----------------
    # ------------------------------------------
    # position: "main", "backup"
    # Auxiliar functions to manage structures
    def get_hum(self, position):
        if position in self.sensors_hum:
            return self.sensors_hum[position]
        return None
    
    def set_hum(self, position, value):
        if position in self.sensors_hum:
            self.sensors_hum[position] = value
        return None
    
    # ------------------------------------------
    # ----------- TIME COUNTER -----------------
    # ------------------------------------------
    # Initialize
    def initialize(self):
        if (self.modbus.connect_gateway()):
            self.modbus.set_compressor(False)
            self.modbus.set_heater(False)
            self.modbus.set_fridge_light(False)
            self.modbus.close_gateway()
        else:
            print("Modbus not connected")
            
    
    # Manage time
    def start(self):
        print("start")
        self.started = True
        self.start_date = datetime.datetime.now(datetime.timezone.utc)
        t = threading.Thread(target=self.worker)
        t.start()
           
    def stop(self):
        self.started = False
        for i in range(5):
            if (self.modbus.connect_gateway()):
                if (self.modbus.set_compressor(False)):
                    if (self.modbus.set_fridge_light(False)):
                        if (self.modbus.set_heater(False)):
                            return True
                self.modbus.close_gateway()
            else:
                print("Modbus not connected")
            
        return False
        
    def is_finished_test(self):
        if int(self.time_elapsed_s/60) >= self.labels_time[-1]:
            return True
        return False
    
    def worker(self):
        print("thread start")
        self.time_elapsed_s = 0
        self.init_registers()
        self.set_temp_hum_value()
                
        while (self.started and not self.is_finished_test()):
            self.set_compressor_light_state()
            self.set_temp_hum_value()
            self.add_registers()
            time.sleep(60)
            self.time_elapsed_s += 60
            print(self.time_elapsed_s)
            #print(len(self.registers["temperature"]))
        #self.time_elapsed_s = 0
        print("Stop")
        self.stop()
    
    # return seconds
    def get_counter(self):
        if self.started:
            return self.time_elapsed_s
        else:
            return 0
    
    # return seconds
    def get_running_time_txt(self):
        if self.started:
            return self.seconds_to_time(self.get_counter())
        return ""
    
    # return seconds
    def get_left_time_txt(self):
        if self.labels_time and self.started:
            return self.seconds_to_time(self.labels_time[-1]*60 - self.get_counter()) 
        elif self.is_finished_test():
            return "Finished Test"
        return ""
    
    def seconds_to_time(self, seconds):
        hours = int(seconds/3600)
        minutes = int((seconds - hours*3600 )/60)
        rest_seconds = int(seconds -  hours*3600 - minutes*60)
        final_time = datetime.time(hours, minutes, rest_seconds)
        #print(final_time)
        #print(f"{hours:02d}:{rest_minutes:02d}")
        return f"{hours:02d}:{minutes:02d}:{rest_seconds:02d}"
        
           
    # -------------------------------------------------------
    # ------- set instant values Compressor -----------------
    # -------------------------------------------------------  
    # get values from sensors and relays and update instant variables
    def set_compressor_light_state(self):
        seconds = self.get_counter()
        minutes = int(seconds / 60)
        # initial condicionament
        old_relay_state = self.relays["compressor"]
        
        if minutes <= CICLE_TIME[0]:
            self.relays["compressor"] = COMPRESSOR[0]
            self.relays["heater"] = not COMPRESSOR[0]
        # clicle 1
        elif minutes <= CICLE_TIME[1]:
            self.relays["compressor"]= COMPRESSOR[1]
            self.relays["heater"] = not COMPRESSOR[1]
        elif minutes <= CICLE_TIME[2]:
            self.relays["compressor"]= COMPRESSOR[2]
            self.relays["heater"] = not COMPRESSOR[2]
        # clicle 2
        elif minutes <= CICLE_TIME[3]:
            self.relays["compressor"]= COMPRESSOR[3]
            self.relays["heater"] = not COMPRESSOR[3]
        elif minutes <= CICLE_TIME[4]:
            self.relays["compressor"]= COMPRESSOR[4]
            self.relays["heater"] = not COMPRESSOR[4]
        # clicle 3
        elif minutes <= CICLE_TIME[5]:
            self.relays["compressor"]= COMPRESSOR[5]
            self.relays["heater"] = not COMPRESSOR[5]
        elif minutes <= CICLE_TIME[6]:
            self.relays["compressor"]= COMPRESSOR[6]
            self.relays["heater"] = not COMPRESSOR[6]
        # clicle 4
        elif minutes <= CICLE_TIME[7]:
            self.relays["compressor"]= COMPRESSOR[7]
            self.relays["heater"] = not COMPRESSOR[7]
        elif minutes <= CICLE_TIME[8]:
            self.relays["compressor"]= COMPRESSOR[8]
            self.relays["heater"] = not COMPRESSOR[8]
        # clicle 5
        elif minutes <= CICLE_TIME[9]:
            self.relays["compressor"]= COMPRESSOR[9]
            self.relays["heater"] = not COMPRESSOR[9]
        #elif minutes <= 870:
        #    self.relays["compressor"]= True
        else:
            self.relays["compressor"]= False
        
        # Set heater
        self.relays["heater"] = not self.relays["compressor"]
        
        # Adjust fridge compressor for histeresis
        if (self.relays["compressor"]):
            temp = self.get_temp("backup")
            if (temp and temp <= MIN_TEMPERATURE):
                self.relays["compressor"] = False
            #elif (temp and temp >= MIN_TEMPERATURE and  temp <= MIN_TEMPERATURE + HISTERESI and not self.compressor_last_state):
            #    self.relays["compressor"] = False
        
        # Start stop Heater
        temp = self.get_temp("main")
        if (temp and temp >= MAX_TEMPERATURE):
            self.relays["heater"]= False
        #elif (not self.relays["compressor"] and temp < MAX_TEMPERATURE):
        #    self.relays["heater"]= True
        #else:
        #    self.relays["heater"]= False
        
        obj_connected = self.modbus.connect_gateway()
        if (obj_connected):
            self.modbus.set_fridge_light(self.started)
            #self.compressor_last_state = self.relays["compressor"]
            self.modbus.set_compressor(self.relays["compressor"])
            self.modbus.set_heater(self.relays["heater"])
            self.modbus.close_gateway()
        else:
            print("Modbus not connected")
            
    # Debug mode
    def set_temp_hum_value(self):
        temp_hum_list = []
        temp_hum_list_backup = []
        if (self.modbus.connect_gateway()):
            temp_hum_list = self.modbus.get_temp_hum("01")
            #print(temp_hum_list)
            time.sleep(1)
            temp_hum_list_backup = self.modbus.get_temp_hum("02")
            time.sleep(1)
            #print(temp_hum_list_backup)
            self.modbus.close_gateway()
        else:
            print("Modbus not connected")
        if (temp_hum_list):
            self.set_temp("main", temp_hum_list[0])
            self.set_hum("main", temp_hum_list[1])
        if (temp_hum_list_backup):
            self.set_temp("backup", temp_hum_list_backup[0])
            self.set_hum("backup", temp_hum_list_backup[1])
        
        self.modbus.close_gateway()
        
        """
        if self.relays["compressor"]:
            lt = [-0.1, -0.2]
        else:
            lt = [0.1, 0.2]
        lh = [-0.5, 0.5]
        
        temp = self.get_temp("top") + random.choice(lt)
        self.set_temp("top", round(temp,1))
        temp = self.get_temp("bottom") + random.choice(lt)
        self.set_temp("bottom", round(temp,1))
        temp = self.get_temp("left") + random.choice(lt)
        self.set_temp("left", round(temp,1))
        temp = self.get_temp("right") + random.choice(lt)
        self.set_temp("right", round(temp,1))
        hum = self.get_hum("top") + random.choice(lh)
        self.set_hum("top", round(hum,1))
        hum = self.get_hum("bottom") + random.choice(lh)
        self.set_hum("bottom", round(hum,1))
        hum = self.get_hum("left") + random.choice(lh)
        self.set_hum("left", round(hum,1))
        hum = self.get_hum("right") + random.choice(lh)
        self.set_hum("right", round(hum,1))
        """
    
    def is_debug_mode(self):
        return self.modbus.is_mod_debug_mode()
    
    # ------------------------------------------
    # ------------- Registers -----------------
    # ------------------------------------------  
    # Register of sensors and relays to print graphics in front end
    def add_registers(self):
        # Compressor
        self.registers["compressor"].append("ON" if self.relays["compressor"] else "OFF")
        self.registers["heater"].append("ON" if self.relays["heater"] else "OFF")
        
        # Temperature
        if (self.get_temp("main") < 50):
            temp = self.get_temp("main")
        else:
            if (len(self.registers["temperature"]) > 1):
                temp = self.registers["temperature"][-1]
                
        if (self.get_temp("backup") < 50):
            temp2 = self.get_temp("backup")
        else:
            if (len(self.registers["temperature2"]) > 1):
                temp2 = self.registers["temperature2"][-1]
        if ('temp' in locals()):
            self.registers["temperature"].append(temp)
            self.registers["temperature2"].append(temp2)
        
        # Humidity
        if (self.get_hum("main") < 100):
            hum = self.get_hum("main")
        else:
            if (len(self.registers["humidity"]) > 1):
                hum = self.registers["humidity"][-1]
                
        if (self.get_hum("backup") < 100):
            hum2 = self.get_hum("backup")
        else:
            if (len(self.registers["humidity2"]) > 1):
                hum2 = self.registers["humidity2"][-1]
        
        if ('hum' in locals()):
            self.registers["humidity"].append(hum)
            self.registers["humidity2"].append(hum2)
    
    def init_registers(self):
        self.registers["compressor"] = []
        self.registers["heater"] = []
        self.registers["temperature"] = []
        self.registers["temperature2"] = []
        self.registers["humidity"] = []
        self.registers["humidity2"] = []
    
    def get_hours_min(self, minutes):
        h = int(minutes/60)
        m = (minutes%60)
        return f"{h}:{m}"
                
    # ------------------------------------------
    # -------------- Front end -----------------
    # ------------------------------------------
    # Convert all values to json usable in front-end
    def tojson(self):
        #print(self.registers)
        result_dict = dict()
        temp_dict = {"temp" : self.get_temp("main") if self.get_temp("main") < 999 else "---", "temp_backup" : self.get_temp("backup") if self.get_temp("backup") < 999 else "---"}
        hum_dict = {"hum" : self.get_hum("main") if self.get_hum("main") < 999 else "---" , "hum_backup" : self.get_hum("backup") if self.get_hum("backup") < 999 else "---"}
        #result_dict.update({"started" : self.started})
        result_dict.update(temp_dict)
        result_dict.update(hum_dict)
        result_dict.update({"labels_time" : self.labels_time})
        result_dict.update({"temp_values" : self.registers["temperature"]})
        result_dict.update({"temp_values2" : self.registers["temperature2"]})
        result_dict.update({"hum_values" : self.registers["humidity"]})
        result_dict.update({"hum_values2" : self.registers["humidity2"]})
        result_dict.update({"compressor_values" : self.registers["compressor"]})
        result_dict.update({"heater_values" : self.registers["heater"]})
        result_dict.update({"running_time" : self.get_running_time_txt()})
        result_dict.update({"left_time" : self.get_left_time_txt()})
        result_dict.update({"start" : self.started})
        result_dict.update({"afes" : flux.AFES})
        result_dict.update({'debug_mode' : self.modbus.is_mod_debug_mode()})
        result_dict.update({'compressor' : COMPRESSOR})
        #result_dict.update({'cicle_time' : CICLE_TIME})
        #result_dict.update({'cicle_time_h' : [self.get_hours_min(m) for m in CICLE_TIME]})
        now = datetime.datetime.now(datetime.timezone.utc)
        cicle_time_h = [self.get_hours_min(m) for m in CICLE_TIME]
        cicle_time_date = [str(now + datetime.timedelta(minutes = m)).split('.')[0] for m in CICLE_TIME]
        cicle_time_txt = ["\n".join([ f"{CICLE_TIME[i-1] if i >0 else '0'} - {CICLE_TIME[i]}  min", f"{cicle_time_h[i-1] if i >0 else '0'} - {cicle_time_h[i]} h", f"{cicle_time_date[i-1] if i >0 else str(now).split('.')[0]} - {cicle_time_date[i]}"]) for i in range(len(CICLE_TIME))]
        result_dict.update({'cicle_time_str' : cicle_time_txt})
        
        #print(result_dict["temp_values"])
        #print(result_dict["temp_values2"])
        #print(result_dict["hum_values"])
        #print(result_dict["hum_values2"])
        #print(result_dict["compressor_values"])
        #print(result_dict["heater_values"])
        #print(result_dict["afes"])
        #print(result_dict)
        return result_dict

    