from influxwrapper import InfluxObject
import pandas as pd
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import seaborn as sns
import datetime
import time
from sklearn.preprocessing import StandardScaler
from scipy import stats
from math import log
import gc
import configuration

CARTRIDGE = ["0076AF57", "0077034B", "00770733", "0076AE51", "00772214", "00770871", "00769732", "00768E4F", "00770BB9", "0076CD84"]
CARTRIDGE_AMB = ["0076AF57", "0076CD84"]
CARTRIDGE_CO2 = ['BETTAIR_CO2_1']

MASTERS_25 = [210030]

AFES = {
        CARTRIDGE[0] : ["", ""], 
        CARTRIDGE[1] : ["", ""], 
        CARTRIDGE[2] : ["", ""], 
        CARTRIDGE[3] : ["", ""], 
        CARTRIDGE[4] : ["", ""],
        CARTRIDGE[5] : ["", ""], 
        CARTRIDGE[6] : ["", ""], 
        CARTRIDGE[7] : ["", ""], 
        CARTRIDGE[8] : ["", ""], 
        CARTRIDGE[9] : ["", ""]
        }

AFES_VOID = {
        CARTRIDGE[0] : ["", ""], 
        CARTRIDGE[1] : ["", ""], 
        CARTRIDGE[2] : ["", ""], 
        CARTRIDGE[3] : ["", ""], 
        CARTRIDGE[4] : ["", ""],
        CARTRIDGE[5] : ["", ""], 
        CARTRIDGE[6] : ["", ""], 
        CARTRIDGE[7] : ["", ""], 
        CARTRIDGE[8] : ["", ""], 
        CARTRIDGE[9] : ["", ""]
        }

AFES_TEST = {
    CARTRIDGE[0] : ["afe11", "afe12"], 
    CARTRIDGE[1] : ["afe21", "afe22"], 
    CARTRIDGE[2] : ["afe31", "afe32"], 
    CARTRIDGE[3] : ["afe41", "afe42"], 
    CARTRIDGE[4] : ["afe51", "afe52"],
    CARTRIDGE[5] : ["afe61", "afe62"], 
    CARTRIDGE[6] : ["afe71", "afe72"], 
    CARTRIDGE[7] : ["afe81", "afe82"], 
    CARTRIDGE[8] : ["afe91", "afe92"], 
    CARTRIDGE[9] : ["afe101", "afe102"]
    }

SENSORS = {
                CARTRIDGE[0] : [[" ", " ", " ", " "], [" ", " "]], 
                CARTRIDGE[1] : [[" ", " ", " ", " "], [" ", " "]],
                CARTRIDGE[2] : [[" ", " ", " ", " "], [" ", " "]], 
                CARTRIDGE[3] : [[" ", " ", " ", " "], [" ", " "]], 
                CARTRIDGE[4] : [[" ", " ", " ", " "], [" ", " "]],
                CARTRIDGE[5] : [[" ", " ", " ", " "], [" ", " "]], 
                CARTRIDGE[6] : [[" ", " ", " ", " "], [" ", " "]],
                CARTRIDGE[7] : [[" ", " ", " ", " "], [" ", " "]], 
                CARTRIDGE[8] : [[" ", " ", " ", " "], [" ", " "]], 
                CARTRIDGE[9] : [[" ", " ", " ", " "], [" ", " "]]
                }

SENSORS_VOID = {
                CARTRIDGE[0] : [["", "", "", ""], ["", ""]], 
                CARTRIDGE[1] : [["", "", "", ""], ["", ""]],
                CARTRIDGE[2] : [["", "", "", ""], ["", ""]], 
                CARTRIDGE[3] : [["", "", "", ""], ["", ""]],
                CARTRIDGE[4] : [["", "", "", ""], ["", ""]],
                CARTRIDGE[5] : [["", "", "", ""], ["", ""]], 
                CARTRIDGE[6] : [["", "", "", ""], ["", ""]],
                CARTRIDGE[7] : [["", "", "", ""], ["", ""]], 
                CARTRIDGE[8] : [["", "", "", ""], ["", ""]],
                CARTRIDGE[9] : [["", "", "", ""], ["", ""]]
                }

SENSORS_TEST = {
                CARTRIDGE[0] : [["NO2", "O3", "NO", "CO"], ["SO2", "H2S"]], 
                CARTRIDGE[1] : [["NO2", "O3", "NO", "CO"], ["SO2", "H2S"]],
                CARTRIDGE[2] : [["NO2", "O3", "NO", "CO"], ["SO2", "H2S"]], 
                CARTRIDGE[3] : [["NO2", "O3", "NO", "CO"], ["SO2", "H2S"]], 
                CARTRIDGE[4] : [["NO2", "O3", "NO", "CO"], ["SO2", "H2S"]],
                CARTRIDGE[5] : [["NO2", "O3", "NO", "CO"], ["SO2", "H2S"]], 
                CARTRIDGE[6] : [["NO2", "O3", "NO", "CO"], ["SO2", "H2S"]],
                CARTRIDGE[7] : [["NO2", "O3", "NO", "CO"], ["SO2", "H2S"]], 
                CARTRIDGE[8] : [["NO2", "O3", "NO", "CO"], ["SO2", "H2S"]], 
                CARTRIDGE[9] : [["NO2", "O3", "NO", "CO"], ["SO2", "H2S"]]
                }




class Influx():
    def __init__(self) -> None:
        self.HOST = 'taller2.bettair.es'
        self.DATABASE = 'anomaly_box'
        self.TAG_KEY = 'cartridge'
        self.MEASURE = 'raw_data'
        AFES = AFES_VOID
        self.show_sensors = []
        self.show_sensors_tunel = []
        self.std_dict = None
        self.config = configuration.Configuration()
    
    def set_database(self, db):
        if (db == 'noise' or db == 'anomaly'):
            print("SET DB to noise, anomaly")
            self.HOST = 'taller2.bettair.es'
            self.DATABASE = 'anomaly_box'
            self.TAG_KEY = 'cartridge'
            self.MEASURE = 'raw_data'
            self.FILTER_COLUMNS = ["SN1_AE_mean", "SN1_WE_mean", "SN2_AE_mean", "SN2_WE_mean", 
                  "SN3_AE_mean", "SN3_WE_mean", "SN4_AE_mean", "SN4_WE_mean", 
                  "SN5_AE_mean", "SN5_WE_mean", "SN6_AE_mean", "SN6_WE_mean"]
        elif (db == 'tunel'):
            print("SET DB to tunel")
            self.HOST = 'control.bettair.es'
            self.DATABASE = 'BettAir'
            self.TAG_KEY = 'CARTRIDGE'
            self.MEASURE = 'rawDataTCP'
            self.FILTER_COLUMNS = ["SN_1_AE", "SN_1_WE", "SN_2_AE", "SN_2_WE", 
                  "SN_3_AE", "SN_3_WE", "SN_4_AE", "SN_4_WE", 
                  "SN_5_AE", "SN_5_WE", "SN_6_AE", "SN_6_WE"]
        elif (db == "ambient"):
            print("SET DB to ambient")
            self.HOST = 'taller2.bettair.es'
            self.DATABASE = 'anomaly_box'
            self.TAG_KEY = 'cartridge'
            self.MEASURE = 'raw_data'
            self.FILTER_COLUMNS = ["RH_HB", "TEMP_HB"]
        elif (db == "co2"):
            print("SET DB to co2")
            self.HOST = 'preproduction.bettair.es'
            self.DATABASE = 'benchmark_co2_multiple'
            self.TAG_KEY = 'DEVICE'
            self.MEASURE = 'processedDataAIProduction'
            self.FILTER_COLUMNS = ["CO2"]
        else:
            print("BAD database name: {db}")
    
    def signaltonoise(self, a, axis=0, ddof=0):
        ar = a.copy()
        ar = ar.resample('1T').mean()
        ar = ar.resample('5S').pad()
        #ar = pd.concat([ar, a]).drop_duplicates(keep=False)
        #print(ar.head().index)
        #print(a.head().index)
        #print(ar.tail().index)
        #print(a.tail().index)
        for index in ar.index:
            if not index in a.index:
                #print(f" Drop for ar: {index}")
                ar = ar.drop(index)
        for index in a.index:
            if not index in ar.index:
                #print(f" Drop for a: {index}")
                a = a.drop(index)
        a = a - ar
        #print(a.head())
        a = np.asanyarray(a)
        m = a.mean(axis)
        #print(f"media: {m}")
        sd = a.std(axis=axis, ddof=ddof)
        #print(f"Std: {10*log(sd/m)}")
        return np.where(sd == 0, 0, m/sd)
    
    def eval_snr(self, df):
        for column in df.columns:
            #print(df[column])
            #arr = stats.zscore(df[column])
            arr = df[column]
            #print(arr)
            snr = self.signaltonoise(arr)
            #print(snr)
            #print(f"SNR of {column} is : {20*log(abs(snr))}")
        return 20*log(abs(snr))
    
    def connect(self):
        t = time.time()
        influxConnectionInfo = {'host': self.HOST, 'port': 8086, 'database':  self.DATABASE}
        influxCon = InfluxObject(**influxConnectionInfo)
        print(f"Time to connect to Influx = {time.time() - t}")
        return influxCon
        
    # Get data from influx
    # Return a dicctionary with all all sensors data per cartridge
    # data[cartridge] --> all data sensors, columns contain SN1_AE, SN1_WE,....
    def get_data_from_influx(self, list_devices, init_time, end_time):
        #t = time.time()
        #print(self.FILTER_COLUMNS)
        influxCon = self.connect()
        if (not influxCon):
            print('There is not a valid connection')
            return {}
        nlines = 1e7
        data = {}
        for cartridge in list_devices:
            print('cartridge %s: retrieving data' % cartridge)
            flag = True
            dt_ini = pd.to_datetime(init_time, utc=True)
            dt_end = pd.to_datetime(end_time, utc=True)
            if cartridge not in data:
                load_data =False
                try:
                    fileLabel = 'cartridge_id_%s.pkl'%cartridge
                    destination = storeFolder+fileLabel
                    if fileLabel in os.listdir('sensor_data'):
                        df_load = pd.read_pickle(destination)
                        dt_ini_load = df_load.index[-1]
                        load_data = True
                    else:
                        dt_ini_load = dt_ini
                    print("init download: " + dt_ini_load.isoformat())
                except:
                    dt_ini_load = dt_ini
                dt_i = dt_ini_load
                first = True
                download = False
                print(dt_i)
                print(dt_end)
                while dt_end > dt_i:
                    dt_e = dt_i + pd.Timedelta('12H')
                    if (dt_e > dt_end):
                        dt_e = dt_end
                    df_tmp = influxCon.getFrameFromDevice(self.MEASURE,{self.TAG_KEY: cartridge},
                                                                _ini=dt_i, 
                                                                _end=dt_e,
                                                                batch_limit= int(nlines), _fields=self.FILTER_COLUMNS)
                    download = True
                    if first:
                        df_data_base = df_tmp.copy(deep=True)
                        first = False
                    else:
                        df_data_base = pd.concat((df_data_base, df_tmp),axis=0)
                    print('cartridge: %s got data between: %s and %s'%(cartridge,dt_i.isoformat(),dt_e.isoformat()))
                    dt_i = dt_e
                if load_data:
                    if download:
                        data[cartridge] = pd.concat((df_load,df_data_base),axis=0)
                    else:
                        data[cartridge] = df_load
                else:
                    data[cartridge] = df_data_base
        #print('cartridge %s: retrieving data' % cartridge)
        
         # Add diff column        
        df1 = pd.DataFrame()
        for key_data in data:
            for column in data[key_data].columns:
                if column.endswith("_AE") or column.endswith("_AE_mean"):
                    df1[column.replace("_AE","_DIFF")] = data[key_data][column.replace("_AE","_WE")] + data[key_data][column]
                    data[key_data][column.replace("_AE","_DIFF")] = data[key_data][column.replace("_AE","_WE")] - data[key_data][column]
            
        
        #for key in data:
            #print(key)
            #print(data[key].head())
            #data[key].dropna()
            #print(data[key].head())
        #    #print(data[key].columns)
        
        return data
    
    # resample in minutes, 0 not resample
    # Transform data to get all same sensors for each dataframe
    # data[0] --> include all SN1_AE sensors
    # data[1] --> include all SN1_WE sensors
    # data[2] --> include all SN2_AE sensors
    def get_sensors_data(self, data):
        # Prepare the output return
        # position 0: include all SN1 mesures either AE and WE
        # position 1: include all SN2 mesures either AE and WE
        # ...
        # position 4: include all SN5 mesures either AE and WE
        
        df_dict = dict()
        electrodes = ["AE", "WE", "DIFF"]
        for electrode in electrodes:
            for sensor in range(1,7): 
                df = pd.DataFrame()
                #A cada una de les sensorboards
                label = ""
                for i in range(len(AFES)):
                    # Afegim a la AFE tots els sensors
                    try:
                        if (sensor <= 4):
                            #print(f"{AFES[CARTRIDGE[i]][0]} - SN{sensor}_{electrode}_mean")
                            if (AFES[CARTRIDGE[i]][0] != ""):
                                df[AFES[CARTRIDGE[i]][0]] = data[CARTRIDGE[i]][f"SN{sensor}_{electrode}_mean"]
                        else:
                            #print(f"{AFES[CARTRIDGE[i]][1]} - SN{sensor}_{electrode}_mean")
                            if (AFES[CARTRIDGE[i]][1] != ""):
                                df[AFES[CARTRIDGE[i]][1]] = data[CARTRIDGE[i]][f"SN{sensor}_{electrode}_mean"]
                        label = f"SN{sensor}_{electrode}_mean"
                    except Exception as e:
                        print(e)
                
                if not df.empty:
                    df_dict[label] = df
                    self.show_sensors.append(label.split("_")[0])
                    #print(f"{label} --> {df.head()}")
       
        #for key in df_dict:
        #    print(df_dict[key].head())
        
        #print(self.show_sensors)
              
        return df_dict
    
    def get_ambient_data(self, data):
        #print(data)
        df = dict()
        #df = {f"{i}" : pd.DataFrame() for i in self.FILTER_COLUMNS}
        for amb in self.FILTER_COLUMNS:
            #print(f"\n**** {amb} ****")
            df[amb] = pd.DataFrame()
            #df = {f"{i}" : pd.DataFrame() for i in self.FILTER_COLUMNS}
            #print(df)
            #A cada una de les sensorboards
            label = ""
            for cartridge in CARTRIDGE_AMB:
                # Afegim a la AFE tots els sensors
                #print(f"\n---- {cartridge} ABOX_{CARTRIDGE_AMB.index(cartridge)+1} -----")
                #print(f"get data[{cartridge}][{amb}] ")
                #print(f"Set data[{amb}][ABOX_{CARTRIDGE_AMB.index(cartridge)+1}] ")
                #print(f"{data[cartridge][amb]}")
                try:
                    df[amb][f"ABOX_{CARTRIDGE_AMB.index(cartridge)+1}"] = data[cartridge][amb]
                    #print(df[amb][f"ABOX_{CARTRIDGE_AMB.index(cartridge)+1}"])
                    #label = cartridge
                except Exception as e:
                    print(e)
                """
                if not df[amb].empty:
                    df_dict[amb] = df[amb]
                    if (label.split("_")[0]):
                        self.show_sensors.append(label.split("_")[0])
                    print(f"{amb} --> {df_dict[amb].head()}")
                """
            #print(df[amb].head())
            
        #for key in df:
        #    print(f"\n**** {key} ****")
        #    print(df[key].head())
              
        return df
    
    def get_sensors_data_tunel(self, data):
        df_dict = dict()
        electrodes = ["AE", "WE", "DIFF"]
        for electrode in electrodes:
            for sensor in range(1,7): 
                df = pd.DataFrame()
                #A cada una de les sensorboards
                label = ""
                for key in data:
                    # Afegim a la AFE tots els sensors
                    try:
                        #print(f"{key} - SN{sensor}_{electrode}_mean")
                        df[key] = data[key][f"SN_{sensor}_{electrode}"]
                        label = f"SN_{sensor}_{electrode}"
                    except Exception as e:
                        print(e)
                
                if not df.empty:
                    df_dict[label] = df
                    self.show_sensors_tunel.append(label.split("_")[0] + "_" + label.split("_")[1])
                
        return df_dict
    
    def get_data(self, init_time, end_time, filename):
        self.set_database(filename)
        t = time.time()
        data = self.get_data_from_influx(CARTRIDGE, init_time, end_time)
        df_sensors = self.get_sensors_data(data)
        if df_sensors:
            self.config.save_data(df_sensors, filename)
            f = self.config.load_data(filename)
            print(f"Time getting data from Influx and saving = {time.time() - t}")
            if not f:
                print("ERROR Saving ambient data")
                return False
            return True
        print(f"There are not data in influx for save")
        return False
    
    def get_data_ambient(self, init_time, end_time):
        t = time.time()
        self.set_database("ambient")
        #list_devices = ["RH_HB", "TEMP_HB"]
        data = self.get_data_from_influx(CARTRIDGE_AMB, init_time, end_time)
        df_sensors = self.get_ambient_data(data)
        if df_sensors:
            self.config.save_data(df_sensors, "ambient")
            f = self.config.load_data("ambient")
            print(f"Time getting ambient data from Influx and saving = {time.time() - t}")
            if not f:
                print("ERROR Saving ambient data")
                return False
            return True
        print(f"There are not data in influx for save")
        return False
    
    def get_data_co2(self):
        t = time.time()
        final_time = datetime.datetime.now(datetime.timezone.utc)
        init_time = final_time - datetime.timedelta(minutes = 10)
        self.set_database("co2")
        #list_devices = ["RH_HB", "TEMP_HB"]
        data = self.get_data_from_influx(CARTRIDGE_CO2, init_time, final_time)
        df = data[CARTRIDGE_CO2[0]]
        co2_int = 0
        if ('CO2' in df):
            co2_value = df['CO2'].iloc[-1]
            if (co2_value):
                co2_int = int(co2_value)
        
        return co2_int
    
    def resampler(self, data, resample):
        t = time.time()
        # Apply resample if needed
        if resample > 0:
            # Resample
            for key in data:
                #print(data[key].head())
                data[key] = data[key].resample(f'{resample}T').mean()
                #data[key] = data[key].resample('5S').pad()
                #print(data[key].head())
        
        print(f"Resampled = {time.time() - t}")
        
    def plot_noise(self, resample):
        t = time.time()
        df_sensors = self.config.load_data("noise")
        print(f"Loading data file = {time.time() - t}")
        self.resampler(df_sensors, resample)
        t = time.time()
        self.boxplot(df_sensors)
        print(f"Time printing boxplot = {time.time() - t}")
        t = time.time()
        self.plot_lines(df_sensors, "noise")
        print(f"Time printing plot lines = {time.time() - t}")
        plt.close('all')
        gc.collect()
        #df_sensors = self.get_sensors_data(data, resample = 1)
        #t = time.time()
        #self.plot_lines(number_sensors, df_sensors, resampled = resampled)
        #print(f"Time printing plot lines resampled = {time.time() - t}")
    
    def plot_anomaly(self, resample, init_test = None, final_test = None):
        t = time.time()
        df_sensors = self.config.load_data("anomaly")
        print(f"Loading data file = {time.time() - t}")
                
        if (init_test and final_test):
            for key in df_sensors:
                df_sensors[key] = df_sensors[key].dropna()
                df_sensors[key] = df_sensors[key][init_test.replace(tzinfo=datetime.timezone.utc):final_test.replace(tzinfo=datetime.timezone.utc)]
        self.show_sensors = list({sensor.split("_")[0] for sensor in df_sensors.keys()})
        
        self.resampler(df_sensors, resample)
        
        t = time.time()
        self.std(df_sensors)
        print(f"Standard deviation data file = {time.time() - t}")
        
        t = time.time()
        self.correlation_matrix(df_sensors, 'anomaly')
        print(f"Time printing correlation matrix = {time.time() - t}")
        t = time.time()
        self.plot_lines(df_sensors, "anomaly")
        print(f"Time printing plot lines = {time.time() - t}")
        plt.close('all')
        gc.collect()
        #self.get_sensors_data(data, resample = 1)
        #t = time.time()
        #self.plot_lines(number_sensors, df_sensors, resampled = True)
        #print(f"Time printing plot lines resampled = {time.time() - t}")
    
    def plot_ambient(self, resample, init_test = None, final_test = None):
        t = time.time()
        df_sensors = self.config.load_data("ambient")
        #print(df_sensors)
        if (init_test and final_test):
            for key in df_sensors:
                df_sensors[key] = df_sensors[key].dropna()
                df_sensors[key] = df_sensors[key][init_test:final_test]
        self.resampler(df_sensors, resample)
        self.plot_lines(df_sensors, "ambient")
        
    def plot_tunel(self, init_time, end_time, cartridge_list, resample):
        self.set_database("tunel")
        t = time.time()
        data = self.get_data_from_influx(MASTERS_25 + cartridge_list, init_time, end_time)
        print(f"Time getting data from Influx = {time.time() - t}")
        df_sensors = self.get_sensors_data_tunel(data)
        for key in df_sensors:
            df_sensors[key] = df_sensors[key].dropna()
        self.resampler(df_sensors, resample)
        self.config.save_data(df_sensors, "tunel")
        t = time.time()
        self.std(df_sensors)
        print(f"Standard deviation data file = {time.time() - t}")
        t = time.time()
        self.correlation_matrix(df_sensors, 'tunel')
        print(f"Time printing correlation matrix = {time.time() - t}")
        t = time.time()
        self.plot_lines(df_sensors, "tunel")
        print(f"Time printing plot lines = {time.time() - t}")
        plt.close('all')
        gc.collect()
    
    def plot_tunel2(self):
        self.set_database("tunel")
        t = time.time()
        #data = self.get_data_from_influx(MASTERS_25 + cartridge_list, init_time, end_time)
        #print(f"Time getting data from Influx = {time.time() - t}")
        #df_sensors = self.get_sensors_data_tunel(data)
        #self.resampler(df_sensors, resample)
        df_sensors = self.config.load_data("tunel")
        for key in df_sensors:
            df_sensors[key] = df_sensors[key].dropna()
        self.config.save_data(df_sensors, "tunel")
        t = time.time()
        self.std(df_sensors)
        print(f"Standard deviation data file = {time.time() - t}")
        t = time.time()
        self.correlation_matrix(df_sensors, 'tunel')
        print(f"Time printing correlation matrix = {time.time() - t}")
        t = time.time()
        self.plot_lines(df_sensors, "tunel")
        print(f"Time printing plot lines = {time.time() - t}")
        plt.close('all')
        gc.collect()      
            
       
    def plot_lines(self, df_sensors, type):
        for key in df_sensors:
            print(f"Ploting {key}...")
            try:
                t = time.time()
                fig = plt.figure(figsize=(15,6))
                fig.patch.set_facecolor('whitesmoke')
                ax = fig.add_subplot()
                ax.set_facecolor('whitesmoke')
                                
                #ax.xaxis.set_major_formatter(mdates.DateFormatter('%d-%m-%Y %H:%M'))
                ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M\n%d-%m-%y'))
                #print(df_sensors[i].head())
                #print(f"Time 1 = {time.time() - t}")
                t = time.time()
                
                p = ax.plot(df_sensors[key])
                #plt.title(key)
                #fig.suptitle(key, fontsize=20)
                COLUMNS = [key for key in df_sensors]
                #print(f"Sensor: {key} - {COLUMNS}")
                leg = plt.legend(loc=(1.04,0.5), labels=df_sensors[key].columns, fontsize="18")
                for legobj in leg.legendHandles:
                    legobj.set_linewidth(2.0)
                #plt.xticks(fontsize="18", rotation=90)
                plt.xticks(fontsize="18")
                plt.yticks(fontsize="18")
                #print(f"Time 2 = {time.time() - t}")
                #print(f"_SUB1 Time printing plot lines = {time.time() - t}")
                t = time.time()
                plt.savefig(f"./static/{key}_{type}", bbox_inches='tight')
                #print(f"Time 3 = {time.time() - t}")
                #plt.close(fig)
                #print(f"_SUB2 Time printing plot lines = {time.time() - t}")
            except Exception as e:
                print(e)
            #plt.clf()
            #plt.cla()
    
    def plot_lines_co2(self, df, table):
        try:
            t = time.time()
            fig = plt.figure(figsize=(15,6))
            fig.patch.set_facecolor('whitesmoke')
            ax = fig.add_subplot()
            ax.set_facecolor('whitesmoke')
                            
            #ax.xaxis.set_major_formatter(mdates.DateFormatter('%d-%m-%Y %H:%M'))
            ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M\n%d-%m-%y'))
            #print(df_sensors[i].head())
            #print(f"Time 1 = {time.time() - t}")
            t = time.time()
            
            #p = ax.plot(df)
            df.plot(ax=ax, x ='time', y=df.columns[1:])
            #plt.title(key)
            #fig.suptitle(key, fontsize=20)
            COLUMNS = [col for col in df]
            #print(f"Columns: {COLUMNS}")
            leg = plt.legend(loc=(1.04,0.5), labels=df.columns[1:], fontsize="18")
            for legobj in leg.legendHandles:
                legobj.set_linewidth(2.0)
            #plt.xticks(fontsize="18", rotation=90)
            plt.xticks(fontsize="18")
            plt.yticks(fontsize="18")
            #print(f"Time 2 = {time.time() - t}")
            #print(f"_SUB1 Time printing plot lines = {time.time() - t}")
            plt.savefig(f"./static/co2_{table}", bbox_inches='tight')
            print(f"Time plot {table} = {time.time() - t}")
            #plt.close(fig)
            #print(f"_SUB2 Time printing plot lines = {time.time() - t}")
        except Exception as e:
            print(e)
        plt.cla()
        plt.clf()
        plt.close(fig)
        
    
    # methods: 
    # pearson : standard correlation coefficient
    # kendall : Kendall Tau correlation coefficient
    # spearman : Spearman rank correlation
    def correlation_matrix(self, df_sensors, type):
        sns.set(rc={'axes.facecolor':'whitesmoke', 'figure.facecolor':'whitesmoke'})
        fig = plt.figure(figsize=(5,5))
        ax = fig.add_subplot()
        ax.set_facecolor('whitesmoke')
        for key in df_sensors:
            try:
                corr = df_sensors[key].corr(method ='pearson')
                #print(key)
                #print(corr)
                #print(df_sensors[key].head())
                #print(df_sensors[key].dtypes)
                h = sns.heatmap(corr, annot=True, cmap='Greens', vmin = 0.6, vmax = 1, cbar=False, annot_kws={"fontsize":12}, xticklabels="auto")
                h.set_xticklabels(h.get_xticklabels(),rotation=30)
                h.set_yticklabels(h.get_yticklabels(),rotation=0)
                #h.set_xticklabels(h.get_xmajorticklabels(), fontsize = 18)
                #h.set_yticklabels(h.get_ymajorticklabels(), fontsize = 18)
                #plt.title(key)
                #fig.suptitle(key, fontsize=20)
                plt.savefig(f"./static/{key}_pearson_{type}.png", dpi=100, bbox_inches='tight')
            except Exception as e:
                print(e)
            plt.cla()
            plt.clf()
        plt.close(fig)
    
    # methods: 
    # pearson : standard correlation coefficient
    # kendall : Kendall Tau correlation coefficient
    # spearman : Spearman rank correlation
    def correlation_matrix_co2(self, df, table):
        t = time.time()
        sns.set(rc={'axes.facecolor':'whitesmoke', 'figure.facecolor':'whitesmoke'})
        fig = plt.figure(figsize=(5,5))
        ax = fig.add_subplot()
        ax.set_facecolor('whitesmoke')
        try:
            #print(df)
            #print("--------------")
            corr = df.corr(method ='pearson')
            #print("--------------")
            #print(corr)
            #print(df_sensors[key].describe())
            h = sns.heatmap(corr, annot=True, cmap='Greens', vmin = 0.6, vmax = 1, cbar=False, annot_kws={"fontsize":12}, xticklabels="auto")
            h.set_xticklabels(h.get_xticklabels(),rotation=30)
            h.set_yticklabels(h.get_yticklabels(),rotation=0)
            #h.set_xticklabels(h.get_xmajorticklabels(), fontsize = 18)
            #h.set_yticklabels(h.get_ymajorticklabels(), fontsize = 18)
            #plt.title(key)
            #fig.suptitle(key, fontsize=20)
            plt.savefig(f"./static/pearson_co2_{table}.png", dpi=100, bbox_inches='tight')
        except Exception as e:
            print(e)
        plt.cla()
        plt.clf()
        plt.close(fig)
        print(f"Time correlation {table} = {time.time() - t}")
        
               
    def boxplot(self, df_sensors):
        sns.set(rc={'axes.facecolor':'whitesmoke', 'figure.facecolor':'whitesmoke'}, font_scale = 1.8)
        fig = plt.figure(figsize=(10,10))
        for key in df_sensors:
            try:
                # Restem la mitja al senyal per centrar a zero
                data = df_sensors[key].sub(df_sensors[key].mean())
                #print(key)
                #print(data.describe())
                #self.eval_boxplot(key, data)
                b = sns.boxplot(data=data)
                b.set_xticklabels(b.get_xticklabels(),rotation=30)
                plt.savefig(f"./static/{key}_boxplot.png", dpi=100)
                
            except Exception as e:
                print(e)
            plt.clf()
            plt.cla()
        plt.close(fig)
    
    def boxplot_co2(self, df, table):
        sns.set(rc={'axes.facecolor':'whitesmoke', 'figure.facecolor':'whitesmoke'}, font_scale = 1.8)
        fig = plt.figure(figsize=(10,10))
        try:
            # Restem la mitja al senyal per centrar a zero
            data = df.sub(df.mean())
            #print(key)
            #print(data.describe())
            #self.eval_boxplot(key, data)
            b = sns.boxplot(data=data)
            b.set_xticklabels(b.get_xticklabels(),rotation=30)
            plt.savefig(f"./static/boxplot_co2_{table}.png", dpi=100)
        except Exception as e:
            print(e)
        plt.clf()
        plt.cla()
        plt.close(fig)
        
    def std(self, df_sensors):
        std_dict = dict()
        for key in df_sensors:
            std_dict[key] = dict()
            try:
                # Restem la mitja al senyal per centrar a zero
                #print(key)
                #print(df_sensors[key].describe())
                #std_frame = df_sensors[key].resample('1T').std()
                #std_frame = std_frame.mean()
                statistics = df_sensors[key].describe()
                mean = [0, 0, 0]
                ref = [9999, 9999, 9999]
                for afe in statistics.columns:
                    #print(df_sensors[key].loc[:,afe])
                    #std = round(statistics.loc['std', afe], 2)
                    #print(f"1: {std}")
                    std = df_sensors[key][afe].resample('1T').std()
                    std = round(std.mean(), 2)
                    #print(f"2: {afe} : {std}")
                    diff = abs(round((statistics.loc['max', afe]-statistics.loc['min', afe]), 2))
                    perc = abs(round((statistics.loc['50%', afe]-statistics.loc['25%', afe]), 2))
                    #snr = self.eval_snr(df_sensors[key][[afe]])
                    std_dict[key][str(afe)] = [std, diff, perc]
                    mean = np.add(mean, [std, diff, perc]).tolist()
                    ref = [std if ref[0] > std else ref[0], 0, perc if ref[2] > perc else ref[2]]
                    #mean = list(map(lambda a,b : round(a/b, 2), mean, [len(statistics.columns)]*3))
                
                # Add mean of each column
                mean = np.divide(mean, len(statistics.columns))
                mean = np.round_(mean, decimals = 2)
                std_dict[key]['mean'] = mean  
                ref[1] = mean[1]
                std_dict[key]['ref'] = ref
                
            except Exception as e:
                print(e)
        
        print(std_dict)
        self.std_dict = std_dict
    
    def get_gas_sensor(self, sensor):
        index = int(sensor.split("_")[0].replace("SN",""))
        return SENSORS[CARTRIDGE[index-1]]
    
    def eval_boxplot(self, sensor, data):
        #print(sensor)
        #print(data.describe())
        if sensor == "NO2" or sensor == "O3" :
            STD_MAX = 3e-01
            MIN = -1
            MAX = 1
            PERCENTIL25 = 0.25 
            PERCENTIL25 = 0.75
        elif sensor == "NO":
            STD_MAX = 4e-01
            MIN = -1
            MAX = 1
            PERCENTIL25 = 0.4
            PERCENTIL25 = 0.4
        elif sensor == "CO":
            STD_MAX = 1.1
            MIN = -3
            MAX = 3
            PERCENTIL25 = 1
            PERCENTIL25 = 1
        
        if data.describe()['std'] > STD_MAX:
            print(f"Exceeded STD_MAX: ({data.describe()['std']}/{STD_MAX})")
        else:
            print(f"Valid STD_MAX: ({data.describe()['std']}/{STD_MAX})")
    
    def print(self, df_sensors):
        for i in range(10):
            print(df_sensors[i].head())
            corr = df_sensors[i].corr(method ='pearson')
            print(corr)
      
if __name__ == "__main__":
    ifx = Influx()
    #final_time = datetime.datetime.now(datetime.timezone.utc)
    #init_time = final_time - datetime.timedelta(minutes = 10)
    
    #init_time = datetime.datetime(2024,2,19,16,0,0, tzinfo=datetime.timezone.utc)
    #final_time = datetime.datetime(2024,2,19,17,0,0, tzinfo=datetime.timezone.utc)
    #co2 = ifx.get_data_co2()
    #print(co2)
    #ifx.get_data_ambient(init_time, final_time)
    #AFES = AFES_TEST
    #ifx.plot_tunel2()
    #config = configuration.Configuration()
    #df = config.load_data("tunel")
    #print(df)
    """
    import sql_connector
    db = sql_connector.SqlConnector()
    for table in ['df', 'df_temp', 'df_hum']:
        df = db.get_co2_dataframes(table, '2023-09-28 09:43:01', '7059457')
        #print(df.describe())
        #df = df.drop(columns=['co2_ch2'])
        print(df.head())
        #ifx.correlation_matrix_co2(df, table)
        #ifx.plot_lines_co2(df, table)
        #ifx.boxplot_co2(df, table)
        #ifx.plot_ambient(0, 'ambient')
    """
        