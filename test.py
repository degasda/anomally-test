def twos_comp(hex_str):
    bin_str = f"{int(hex_str, 16) : 016b}"[2:]
    val = int(bin_str,2)
    bits = len(bin_str)
    if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val                         # return positive value as is
  
hex_str = "FFF6"
out = twos_comp(hex_str)

print(out)