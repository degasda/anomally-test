#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 11:44:49 2021

@author: bettaircities
"""

from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime, timezone
import pandas as pd
from influx_manager import InfluxManager

#####
# TODO: add logger and remove debugging prints
##############################
# TODO: default config in separate file
"""
host = "ws2.bettair.es"
port = "27017"
user = "mongoadmin"
pw = "mongobettair"
"""

host = "127.0.0.1"
port = "27017"
user = "root"
pw = "example"

config_types = {
    # SENSOR
    # ( for now assumes EC alphasense sensor )
    'sensor': {  # collection name in mongodb
        'collection_name': 'allitems',
        # list of editable fields
        'list_variable_fields': ['status', 'placedin',
                                 'contains', 'message', 'logs', 'last_update'],
        # list of fixed fields (mongo id not included):
        'list_fixed_fields': ['serial', 'reception', 'itemtype'],
        'list_factory_fields': ['sensor_type', 'WE_zero_from_afe', 'AE_zero_from_afe',
                                'WE_zero_from_sensor', 'AE_zero_from_sensor', 'sensitivity', 'batch'],
        'typein': ['afe'],
        'typecontains': [],
    },
    # AFE
    'afe': {'collection_name': 'allitems',
            'list_variable_fields': ['status', 'placedin',
                                     'contains', 'message', 'logs', 'last_update','state', 'lastStateUpdateDate',
                                     'sboardAssigned'],
            'list_fixed_fields': ['serial', 'reception', 'itemtype'],
            'list_factory_fields': [
                'afe_type', 'circuit_type', 'zero_cal_date',
                'pcb_gain_1', 'pcb_gain_2', 'pcb_gain_3',
                'pcb_gain_4', 'WE_1_offset', 'WE_2_offset',
                'WE_3_offset', 'WE_4_offset', 'AE_1_offset',
                'AE_2_offset', 'AE_3_offset', 'AE_4_offset',
                'slot_types'],
            'typein': ['sbox', 'sboard'],
            'typecontains': ['sensor'],
            },
    # SENSOR BOARD (CARTRIDGE when not in anomaly box)
    'sboard': {'collection_name': 'allitems',
               'list_variable_fields': ['status', 'placedin',
                                        'contains', 'message', 'logs', 'last_update',
                                        'horn_sensor', 'extra_T_sensor'],
               # influx connector contains all needed info to retrieve data
               # from db to device id
               'list_fixed_fields': ['serial', 'reception', 'itemtype',
                                     'influx_connector', 'influx_name',
                                     'sensor_names'],
               'typein': ['abox', 'cartridge'],
               'typecontains': ['afe', 'pmsensor', 'co2sensor']  # , hornpcb
               },
    'cartridge': {'collection_name': 'allitems',
                  'list_variable_fields': ['status', 'placedin',
                                           'contains',
                                           'message', 'logs', 'last_update', 'calibration'],
                  'list_fixed_fields': ['serial', 'reception', 'itemtype'],
                  'typein': ['tunnel'],
                  'typecontains': ['sboard'],
                  },
    'node': {'collection_name': 'allitems',
             'list_variable_fields': ['status', 'placedin',
                                      'contains', 'main', 'station_id', 'gps', 'quectel',
                                      'sim2', 'sim_status', 'firmware', 'customer', 'verified',
                                      'message', 'logs', 'last_update'],
             'list_fixed_fields': ['serial', 'node_id', 'model', 'reception', 'itemtype'],
             'typein': [],
             'typecontains': ['cartridge'],
             },
    'tunnel': {'collection_name': 'allitems',
               'list_variable_fields': ['status',
                                        'contains', 'message', 'logs', 'last_update',
                                        'itemsin', 'itemsout'],
               'list_fixed_fields': ['serial', 'reception', 'itemtype'],
               'typein': [],
               'typecontains': ['cartridge'],
               },

    # ANOMALY BOX
    'abox': {'collection_name': 'allitems',
             'list_variable_fields': ['status', 'placedin',
                                      'contains', 'message', 'logs', 'last_update'],
             'list_fixed_fields': ['serial', 'reception', 'itemtype'],
             'typein': [],
             'typecontains': ['sboard'],
             },

    # AFE STORAGE BOX
    'sbox': {'collection_name': 'allitems',
             'list_variable_fields': ['status', 'placedin',
                                      'contains', 'humidity', 'message', 'logs', 'last_update'],
             'list_fixed_fields': ['serial', 'reception', 'itemtype'],
             'typein': [],
             'typecontains': ['afe'],
             },

    'pmsensor': {
        'collection_name': 'allitems',
        'list_variable_fields': ['status', 'placedin',
                                 'contains', 'message', 'logs', 'last_update'],
        'list_fixed_fields': ['serial', 'reception', 'itemtype'],
        'list_factory_fields': ['model'],
        'typein': ['sboard'],
        'typecontains': [],
    },
    'co2sensor': {
        'collection_name': 'allitems',
        'list_variable_fields': ['status', 'placedin',
                                 'contains', 'message', 'logs', 'last_update'],
        'list_fixed_fields': ['serial', 'reception', 'itemtype'],
        'typein': ['sboard'],
        'typecontains': [],
    },
}
# hornpcb ignored
##########################
# default influx_connetor in sensorboard (or cartridge) item fields
# for anomaly tests

influx_default = {
    'influxConnectionInfo': {'host': 'taller2.bettair.es',
                             'port': "8086",
                             'database': 'anomaly_box',
                             },
    'measure': 'raw_data',
    'tag_key': 'cartridge',
}
#########################

"""
sensor names in sensor board
 if in contains : {1: afexxx_id, 2: afeyyy_id} #these are afe items.
 The sensor cannot be in the board unless through an afe so we do not
 list them as contained in it.
 The board data in influx identifies the sensors separately though. In
 order to locate them, we need to know the mapping from names to sensor
 positions:
 sensor_names = { 'afe1_pos1': 'sn1'
                  'afe1_pos2': 'sn2'
                  'afe1_pos3': 'sn3'
                  'afe1_pos4': 'sn4'
                  'afe2_pos1': 'sn5'
                  'afe2_pos2': 'sn6' }
 This is fixed and does not depend on the specific afe that is placed in
 the board. In general it will be also constant for all boards.
 For example: in order to locate the sensor named sn3, look for the sensor
 in position 3 of the afe in position 1 (key 1_3)
"""
sensor_names_default = {'1_1': 'SN1',
                        '1_2': 'SN2',
                        '1_3': 'SN3',
                        '1_4': 'SN4',
                        '2_1': 'SN5',
                        '2_2': 'SN6'}


##########################

class BettairItem(object):
    """
    bettair items db connector.
    Implementation for mongodb.
    ----------
    If different collections are used for
    different item types, we just need to set the
    collection names in the config data under the corresponding
    itemtype key. Same can be done with db if needed.

    If an item type requires especific attributes or methods, they will be
    defined as a BettaiItem subclass.

    TODO : anomalies tests data also as attribute {idtest:resulttest}
    TODO: enforce types where item can be palced or that can contain.
          enforce possible slots

    """

    def __init__(self, *args, db_name='Bettair_items_db', itemtype=None,
                 collection_name='allitems', collection=None, **kwargs):
        # initialize mongo db variables and connectors
        if len(args) > 0:
            raise TypeError('__init__() takes no positional arguments')
        if collection is not None:
            try:
                self.collection = collection
                self.db = self.collection.database
                self.client = self.db.client
                self.db_name = self.db.name
                self.collection_name = collection.name

            except:
                print('Error accessing passed collection object.'
                      + ' Initializing with default.')
                collection = None
        if collection is None:
            self.db_name = db_name
            self.client = MongoClient(
                'mongodb://' + user + ':' + pw + '@' + host + ':'
                + port + '/?authSource=admin')
            self.db = self.client[self.db_name]
            self.collection_name = collection_name
            self.collection = self.db[self.collection_name]

        # initialize lists of attributes depending on type
        self._set_type(itemtype)
        self.list_all_fields = (self.list_variable_fields
                                + self.list_fixed_fields
                                + self.list_factory_fields)
        for field in self.list_all_fields:
            if field == 'itemtype':
                continue
            elif field == 'influx_connector':
                self.influx_connector = influx_default
                continue
            elif field == 'contains':
                self.contains = {}
                continue
            self.__dict__[field] = None
        # initilize empty general attributes
        self.logs = []
        # self.changes = {}
        # self.list_changed = []
        self._id = None
        self.cache_updates = []
        # self.pastlog = []
        #############
        # sets attributes from init keyword arguments
        # if _id is passed, loads the item data from db first, then
        # edits the others. It does not save changes.
        if len(kwargs) > 0:
            if '_id' in kwargs.keys():
                if not self.load(kwargs['_id']):
                    raise Exception('Failed load of %s in init' % kwargs['_id'])
                _ = kwargs.pop('_id')
            self._parse_fields(self._enforce_fields(kwargs,
                                                    self.list_all_fields))

    def __repr__(cls):
        return 'BettairItem'

    def _set_type(self, itemtype):

        if itemtype not in config_types.keys():
            self.list_variable_fields = ['status', 'placedin',
                                         'contains', 'message', 'logs', 'last_update']
            self.list_fixed_fields = ['serial', 'reception', 'itemtype']
            self.list_factory_fields = []
        else:

            self.collection_name = config_types[itemtype]['collection_name']
            self.list_variable_fields = config_types[itemtype][
                'list_variable_fields']
            self.list_fixed_fields = config_types[itemtype][
                'list_fixed_fields']
            try:
                self.list_factory_fields = config_types[itemtype][
                    'list_factory_fields']
            except:
                self.list_factory_fields = []

        self.itemtype = itemtype

    def load(self, item_id):
        """
        get item with given id from db

        Parameters
        ----------
        item_id : str or objectid
            id to fetch in db.

        Returns
        -------
        bool.
            False if failed load

        """
        if isinstance(item_id, str):
            try:
                item_id = ObjectId(item_id)
            except:
                return False
        return self.load_from_fields({'_id': item_id})

    def load_from_fields(self, *args, **kwargs):
        """
        Load from db

        Parameters
        ----------
        *args : positional arguments
            Optional dict with fields to filter for the query
            (only admints 1 positional argument).
        **kwargs : keyword arguments
            Any number of keyword arguments that will be fields to filter
            for the query.

        Returns
        -------
        bool
            False if failed load.

        """
        if len(args) > 1:
            # launch expection typeError
            print('Error: Takes one positional argument (%i given)' % len(args))
            return False
        else:
            try:
                fields = args[0]
            except:
                fields = {}
        if not isinstance(fields, dict):
            fields = {}
            print('Warning: Positional argument should be a dict. Ignored')

        if len(kwargs) > 0:
            for itkey, itval in kwargs.items():
                fields[itkey] = itval

        if self.itemtype is None:
            if 'serial' in fields.keys():
                if '@' in fields['serial']:
                    sserial = fields['serial'].split('@')
                    fields['itemtype'] = sserial[0]
                    fields['serial'] = sserial[1]
                    self._set_type(sserial[0])
        else:
            fields['itemtype'] = self.itemtype
            if 'serial' in fields.keys():
                if '@' in fields['serial']:
                    print('Warning: When item is initialized to a itemtype'
                          ' serial value should not use prefix.')
        try:
            result = self.collection.find_one(fields)
        except:  # InvalidId:
            return False
        if result is not None:
            # reset to avoid mix of present and loaded data
            self.contains = {}
            self.cache_updates = []
            # self.changes = {}
            self.list_changed = []
            self._parse_fields(result)
            # self._id = result['_id']
            return True
        else:
            print('Not found')
            return False

    def save(self, list_fields=None, timestamp=None, force_update=False):
        """
        Save item object in mongo db

        Parameters
        ----------
        list_fields : list, optional
            list of fields to save (update). The default is None.
        timestamp : datetime, optional
            timestamp can be passed . The default is datetime.now(tz=timezone.utc).
        force_update : TYPE, optional
            If True, forces update of all fields, including fixed fields
            (if list_fields is not passed).
            The default is False.

        Returns
        -------
        None.

        """
        flag_saved = False
        if self._id is None:
            print('Adding new item in db')
            past_item = None
        else:
            print('Saving item %s' % self._id)
            past_item = BettairItem(_id=self._id, collection=self.collection)
        if timestamp is None:
            timestamp = datetime.now(tz=timezone.utc)
        if ((list_fields is None) & (force_update)) | (self._id is None):
            list_fields = self.list_all_fields
        if self._id is None:
            # not yet in db
            if self._ensure_fields():
                result = self.collection.insert_one(self._compose_fields(
                    list_fields,
                    timestamp=timestamp,
                    tolog=False)
                )
                if result.acknowledged:
                    self._id = result.inserted_id
                    self.last_update = timestamp
                    flag_saved = True
                else:
                    print('Could not save in %s' % self.db_name)
            else:
                print('Fixed fields have to be set first: ')
                print(', '.join(self.list_fixed_fields))
        else:
            if list_fields is None:
                # list_fields = self.list_changed
                list_fields = self._list_changes(past_item)
            # item already in db but need to update changes
            # caution with updates if there can be concurrent accessess to db!
            if len(list_fields) > 0:
                flag_saved = True
                result = self.collection.update_one(
                    {'_id': self._id},
                    {'$set': self._compose_fields(
                        list_fields,
                        timestamp=timestamp,
                        tolog=False)

                    })
                # self.list_changed = []
                # if force_update:
                #     self.last_update = timestamp
                # list_updated = []
                pastlog = past_item._compose_fields(list_fields,
                                                    timestamp=timestamp,
                                                    tolog=True)
                if len(pastlog) > 0:

                    # for updates of logs we add changes appended to list
                    # for ilog, pastlog in enumerate(self.pastlog):
                    result = self.collection.update_one(
                        {'_id': self._id},
                        {'$push': {'logs': pastlog}
                         })
                    if result.acknowledged:
                        self.logs.append(pastlog)
                        # list_updated.append(ilog)

                    else:
                        print('Could not update log')
                # if len(list_updated) == len(self.pastlog):
                #     self.pastlog = []
        ###
        # dependencies updates

        if len(self.cache_updates) > 0:
            flag_saved = True
            # for updates of relations in other items of the db

            for item, field, pos in self.cache_updates:
                if field == 'placedin':
                    # item.set_fields(placedin=[self._id, pos],
                    #                 update_chain=False)
                    print('Updating child')
                    item.placein(self._id, pos, update_chain=False)
                elif field == 'contains':

                    # item.set_fields({'contains.' + pos: self._id},
                    #                 update_chain=False)
                    print('Updating parent')
                    item.contain(self._id, pos, update_chain=False)
                elif field == 'remove_placein':
                    item.remove_placein(update_chain=False)
                elif field == 'remove_contains':
                    item.remove_contains([pos], update_chain=False)
            self.cache_updates = []
        if not flag_saved:
            print('Nothing to save')

    def _list_changes(self, other):
        list_changes = []
        for param, val in self.__dict__.items():
            try:
                if val != other.__dict__[param]:
                    list_changes.append(param)
            except:
                list_changes.append(param)
        return list_changes

    def reset(self):
        """
        Reset object as in init without any info related to any item.
        This does not affect the db

        Returns
        -------
        None.

        """
        self.__init__(collection=self.collection)

    def remove(self):
        """
        Remove item from db
        TODO : remove from positions where it is placed and/or items contained
        """
        if len(self.placedin) > 0:
            self.remove_placein()
        if len(self.contains) > 0:
            self.remove_contains('all')
        self.save()
        self.collection.delete_one({'_id': self._id})

    def set_fields(self, *args, autosave=True, set_fixed=False,
                   update_chain=True, **kwargs):
        """
        edit fields from input dict and/or input keyword args
        save in db if autosave is not passed equal to false
        If field existed, it replaces the value
        It adds new entry in log with precedent values.

        Some values, in list_fixed, like SN, should not change and are not
        parsed by default. We can allow these to be changed with
        set_fixed=True
            (e.g. SN is found to be miss-typed and we want to preserve
             the document with same id, then we need to allow change)

        Parameters
        ----------
        autosave : Bool, optional
            Saves in db after setting changes in object. The default is True.
        set_fixed: Bool, optional
            Allows setting fields that are supposed to be fixed.
        Returns
        -------
        None.

        """
        print('Setting fields for item %s' % self._id)
        list_fields = self.list_variable_fields + self.list_factory_fields
        if set_fixed:
            list_fields = self.list_all_fields

        if len(args) > 1:
            # launch expection typeError
            print('Error: Takes one positional argument (%i given)' % len(args))
            return False
        else:
            try:
                fields = args[0]
            except:
                fields = {}
        if not isinstance(fields, dict):
            fields = {}
            print('Warning: Positional argument should be a dict. Ignored')

        for itkey, itval in kwargs.items():
            if itkey in fields.keys():
                print('Conflict with repeated fields to set.'
                      + ' Keeping the value passed as keyword')
            fields[itkey] = itval

        fields = self._enforce_fields(fields, list_fields,
                                      update_chain=update_chain)
        if len(fields) == 0:
            print('No arguments to set or wrong format')
            return False

        if 'other' in fields.keys():
            list_fixedfound = [kk for kk in fields['other']
                               if kk in self.list_fixed_fields]
            if len(list_fixedfound) > 0:
                print('Warning: Trying to set fixed fields?'
                      + ' set_fixed must be set to True')
        timestamp = datetime.now(tz=timezone.utc)
        fields['last_update'] = timestamp
        # print('...adding past state to log')
        # self.pastlog.append(self._compose_fields(self.list_variable_fields,
        # timestamp=timestamp))
        print('...setting attributes:')
        print(fields)
        self._parse_fields(fields)
        # self.list_changed += list(fields.keys())
        # self.list_changed = list(set(self.list_changed))

        if autosave:
            self.save(list_fields=list(fields.keys()), timestamp=timestamp)

    def get_fields(self):
        return self._compose_fields(self.list_all_fields, tolog=False)

    def _parse_fields(self, dict_args):
        """
        set dict items from argument to class attributes
        This method does not check or enforce a schema, only
        parses the fields as attributes

        Parameters
        ----------
        dict_args : dict
            dict of fields keys and values to set as class attributes.

        Returns
        -------
        None.

        """
        for itkey, itval in dict_args.items():
            if (itkey == 'contains') & isinstance(self.contains, dict):
                try:
                    for itpos, itit in itval.items():
                        self.contains[itpos] = itit
                except:
                    self.contains = itval
            else:
                self.__dict__[itkey] = itval

    def _ensure_fields(self):
        """
        ensures that all mandatory fixed fields have been provided.

        Returns
        -------
        bool
            False if any of the fixed fields is empty.

        """
        # flag_valid = True
        for field in self.list_fixed_fields:
            if self.__dict__[field] is None:
                return False
        return True

    def _compose_fields(self, list_fields, timestamp=None, tolog=True):
        """
        builds dict containing class attributes
        Either for upload to mongo db or for adding entry to log

        Parameters
        ----------
        list_fields: list
            List of fields to add in the composed dict
        timestamp: datetime. Default None
            Timestamp to add as last_update or time_end in changes log
            If None, it does not change last_update with tolog=False,
            it sets as datetime.now if tolog=True
        tolog: Bool. Default True
            If True, builds dict of past state for log entry. If false,
            build dict to save in db.
        Returns
        -------
        None.

        """

        dict_fields = {itkey: itval for itkey, itval in
                       self.__dict__.items()
                       if itkey in list_fields}

        # if len(kwargs)
        if tolog:
            if timestamp is None:
                timestamp = datetime.now(tz=timezone.utc)
            if len(dict_fields) > 0:
                dict_fields['time_ini'] = self.last_update
                dict_fields['time_end'] = timestamp
                try:
                    _ = dict_fields.pop('last_update')
                except:
                    pass
                try:
                    _ = dict_fields.pop('logs')
                except:
                    pass
        else:
            if timestamp is not None:
                dict_fields['last_update'] = timestamp
        return dict_fields

    def _enforce_fields(self, dict_fields, list_fields, update_chain=True):
        """
        We enforce the fields to be fixed schema.
        The non-matching fields will be saved under key 'other'

        Enforces presence of items that contain/contained by the item
        if included in the dict of fields

        Format of placedin : 2 dim list [x, y] where x is the container
        item id and y is the position within the container

        Format of contains: dict with pairs {x:y} where x is the position
        within the item and y the id of the item contained in that position
        (x can be integer or any keyword, like SN1)

        TODO: enforce datatypes
        TODO enforce position to predefined position labels

        Parameters
        ----------
        dict_fields : dict
            dict with pairs field key, values.

        list_fields: list
            list of keys of fields that are valid item attributes
        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        print('Enforcing fields format, schema and update chain..')
        print('In')
        print(dict_fields)
        dict_fields_out = {}
        dict_other = {}
        # item_contains = {}
        for itkey, itval in dict_fields.items():

            if itkey in list_fields:
                dict_fields_out[itkey] = itval
            elif 'contains' == itkey.split('.')[0]:
                print('Nested contains: ' + itkey)
                # item_contains = {itkey.split('.')[1]: itval}
                if 'contains' not in dict_fields_out:
                    dict_fields_out['contains'] = {}
                dict_fields_out['contains'][itkey.split('.')[1]] = itval
            else:
                dict_other[itkey] = itval
        if len(dict_other) > 0:
            dict_fields_out['other'] = dict_other

        if len(dict_fields_out) > 0:
            if 'placedin' in dict_fields_out.keys():
                # flag_skip = False
                # if serial instead of mongo id passed,
                # we assume str with prefix itemtype+'@'
                if ((not isinstance(dict_fields_out['placedin'], list))
                        | len(dict_fields_out['placedin']) != 2):
                    print('Wrong placedin format. Set as:'
                          + ' [id/serial, position]')
                    # flag_skip = True
                    # parent_id = None
                    dict_fields_out.pop('placedin')
                else:
                    parent_id = dict_fields_out['placedin'][0]

                    try:
                        parent = BettairItem(_id=parent_id,
                                             collection=self.collection)
                        flag_place = True
                    except:
                        # TODO add item automatically?
                        # it will only contain
                        # the serial number and (maybe) itemtype,
                        # will need to be
                        # externally updated later on.
                        # Possibly not wanted
                        print('Add %s in db first' % parent_id)
                        # _ = dict_fields_out.pop('placedin')
                        flag_place = False
                    if flag_place:
                        try:
                            already_item = parent.contains[
                                dict_fields_out['placedin'][1]]
                            if ((already_item != self._id)
                                    & (already_item is not None)):
                                flag_place = False
                                print('Position '
                                      + '%s' % dict_fields_out['placedin'][1]
                                      + ' in %s' % dict_fields_out[
                                          'placedin'][0]
                                      + ' already taken. Remove item '
                                      + '%s first' % already_item)
                        except:
                            pass
                    if flag_place:
                        if self.placedin is not None:
                            if ((parent._id == self.placedin[0])
                                    & (dict_fields_out['placedin'][1]
                                       == self.placedin[1])):
                                # _ = dict_fields_out.pop('placedin')
                                print('Item already in placedin to set.')
                                flag_place = False
                    if flag_place:
                        print('.. setting new placement')
                        dict_fields_out['placedin'][0] = parent._id
                        if update_chain:
                            if self.placedin is not None:
                                print('... preparing to update previous parent')
                                self.cache_updates.append(
                                    [BettairItem(_id=self.placedin[0],
                                                 collection=self.collection),
                                     'remove_contains',
                                     self.placedin[1]])
                            print('...preparing to update new parent')
                            self.cache_updates.append(
                                [parent, 'contains',
                                 dict_fields_out['placedin'][1]])
                    else:
                        _ = dict_fields_out.pop('placedin')
            if 'contains' in dict_fields_out.keys():
                dict_tmp = dict_fields_out['contains'].copy()
                for child_pos, child_id in dict_tmp.items():
                    child_id_filt = self._common_contains_enforce(
                        dict_fields_out,
                        child_pos, child_id,
                        update_chain=update_chain)
                    if child_id_filt is None:
                        _ = dict_fields_out['contains'].pop(child_pos)
                    else:
                        dict_fields_out['contains'][child_pos] = child_id_filt
        # while len(item_contains) > 0:
        #     child_pos, child_id = item_contains.popitem()
        #     dict_fields_out['contains'] = {}
        #     print('.. nested %s, %s'%(child_pos, child_id))
        #     self._common_contains_enforce(dict_fields_out,
        #                                      child_pos, child_id,
        #                                      nested=True,
        #                                      update_chain=update_chain)
        if 'contains' in dict_fields_out.keys():
            if len(dict_fields_out['contains']) == 0:
                _ = dict_fields_out.pop('contains')

        print('Done enforcing fields. Out:')
        print(dict_fields_out)
        return dict_fields_out

    def _common_contains_enforce(self, dict_fields_out,
                                 child_pos, child_id,
                                 update_chain=True):
        flag_place = False
        try:
            already_item = self.contains[child_pos]

            if already_item is None:
                flag_place = True
        except:
            flag_place = True
        if flag_place:

            try:
                child = BettairItem(_id=child_id,
                                    collection=self.collection)
            except:
                if '@' in child_id:
                    sserial = child_id.split('@')
                    childtype = sserial[0]
                    childserial = sserial[1]
                    try:
                        child = BettairItem(itemtype=childtype,
                                            collection=self.collection)
                        if not child.load_from_fields(serial=childserial):
                            raise ('Child item not found in db'
                                   + ' with serial %s' % childserial)
                    except:
                        # TODO add item automatically?
                        print('Add %s in db first' % child_id)
                        return None

            if child.placedin is not None:
                if len(child.placedin) == 0:
                    print('Remove %s first from current position' % child_id)
                    return None
            if update_chain:
                print('.. preparing update of new child')
                self.cache_updates.append([child, 'placedin', child_pos])
            return child._id

        if already_item != child_id:
            print('Position %s already taken.' % child_pos
                  + ' Remove item %s first' % already_item)
        else:
            print('Item already placed in given position.')
        return None

    def placein(self, parent_id, position, update_chain=True, autosave=True):
        self.set_fields(placedin=[parent_id, position],
                        update_chain=update_chain, autosave=autosave)

    def contain(self, child_id, position, update_chain=True, autosave=True):
        self.set_fields({'contains.' + position: child_id},
                        update_chain=update_chain, autosave=autosave)

    def remove_placein(self, update_chain=True, autosave=True):
        """
        Remove placedin from item

        Parameters
        ----------
        update_chain : Bool, optional
            If True, propagates changes to dependencies. The default is True.
        autosave : Bool, optional
            If true, saves to db. The default is True.

        Returns
        -------
        None.

        """
        timestamp = datetime.now(tz=timezone.utc)
        print('Removing placedin of item %s: ' % self._id
              + str(self.placedin))
        if self.placedin is not None:
            if update_chain:
                try:
                    item = BettairItem(_id=self.placedin[0],
                                       collection=self.collection)
                    if autosave:
                        item.remove_contains([self.placedin[1]],
                                             update_chain=False)
                    else:
                        self.cache_updates.append([item, 'remove_contains',
                                                   self.placedin[1]])
                except:
                    item = None
            # self.pastlog.append(self._compose_fields(self.list_variable_fields,
            #                                timestamp=timestamp))
            self.placedin = None
            self.list_changed += ['placedin']
            if autosave:
                self.save(list_fields=['placedin'])

    def remove_contains(self, positions, update_chain=True, autosave=True):
        """
        Remove contains for given positions

        Parameters
        ----------
        positions : list
            List of positions to remove from contains.
        update_chain : Bool, optional
            If True, propagates changes to dependencies. The default is True.
        autosave : Bool, optional
            If true, saves to db. The default is True.

        Returns
        -------
        None.

        """
        timestamp = datetime.now(tz=timezone.utc)
        if positions == 'all':
            positions = list(self.contains.keys())
        print('Removing contains positions of item %s: ' % self._id
              + ', '.join(positions))

        for position in positions:
            try:
                position_id = self.contains[position]
            except:
                continue

            if position_id is not None:
                print('Setting child placedin')
                if update_chain:
                    try:
                        item = BettairItem(_id=position_id,
                                           collection=self.collection)
                        if autosave:
                            item.remove_placein(update_chain=False)
                        else:
                            self.cache_updates.append([item, 'remove_placein',
                                                       position])
                    except:
                        item = None
            else:
                continue

            self.contains[position] = None
            # _ = self.contains.pop(position)
        # self.pastlog.append(self._compose_fields(self.list_variable_fields,
        #                                 timestamp=timestamp))

        self.list_changed += ['contains']
        if autosave:
            self.save(list_fields=['contains'])

    @staticmethod
    def _find_item(_id, list_types):
        """
        Find item in db, static  method that returns the object if found
        from serial and list of possible types
        Deprecated. It is not needed if we know the type of item and not
        recommended if we don't. If _id is mongoid then the search is
        useless.

        """
        for itemtype in list_types:
            item = BettairItem(itemtype=itemtype)
            if item.load(_id):
                return item
            elif isinstance(_id, str):
                if itemtype == _id.split('@')[0]:
                    if item.load_from_fields(
                            serial=_id.split('@')[1]):
                        return item
                elif item.load_from_fields(serial=_id):
                    # this allows only serial n to be used.
                    # assumes that load_from_fields already
                    # filters by itemtype (no need of prefix)
                    return item
        return None

    def find_history_place(self, ini=None, end=None):
        """
        build timeseries of placedin and contains data from logs

        Parameters
        ----------
        ini : datetime, optional
             The default is None.
        end : datetime, optional
             The default is None.

        Returns
        -------
        inserie : pd.DataFrame
            placedin ts df.
        contserie : pd.DataFrame
            contains df, with postions as columns.

        """
        if ini is None:
            ini = self.reception
        if end is None:
            end = self.last_update
        # inserie = pd.DataFrame(columns=['_id', 'position'])
        logsserie = pd.DataFrame(columns=['logindex'])
        # contserie = pd.DataFrame()
        for idlog, logentry in enumerate(self.logs):
            if (logentry['time_ini'] <= end) & (logentry['time_end'] >= ini):
                logsserie = pd.concat((logsserie,
                                       pd.DataFrame(index=[max(logentry['time_ini'], ini)],
                                                    data=[idlog])))
                # inserie = pd.concat((inserie,
                #     pd.DataFrame(index=[max(logentry['time_ini'], ini)],
                #                  data=logentry['placedin'])))
                # contserie = pd.concat((contserie,
                #     pd.DataFrame(index=[max(logentry['time_ini'], ini)],
                #                  data=logentry['contains'])))
        return logsserie  # inserie, contserie


class SensorBoard(BettairItem):
    horn_sensor_list = ['BME680', 'BME280', 'BMP280']

    def __init__(self, *args, db_name='Bettair_items_db',
                 collection_name='allitems', collection=None, **kwargs):
        if len(args) > 0:
            raise TypeError('__init__() takes no positional arguments')
        super().__init__(db_name=db_name, itemtype='sboard',
                         collection_name=collection_name,
                         collection=collection, **kwargs)
        if 'influx_connector' in self.list_all_fields:
            self._set_influx()
            # if influx connector config not passed in init, this will
            # use default parameters:
        if 'sensor_names' in self.list_all_fields:
            self.sensor_names = sensor_names_default
        self.dict_afes = {}
        self.dict_sensors = {}

    # influx methods go here, not from generic item
    def __repr__(self):
        return 'SensorBoard'

    # TODO: adapt to new reality of expanded sensor universe (pm1, pm2, co2)
    def list_sensors(self, return_item=False):
        dict_pos_2step = {}
        for possb, idafe in self.contains.items():
            try:
                afetmp = BettairItem(_id=idafe, collection=self.collection)
                for posafe, idsens in afetmp.contains.items():
                    try:
                        sensor = BettairItem(_id=idsens,
                                             collection=self.collection)
                    except:
                        sensor = None
                        pass

                    if return_item:
                        dict_pos_2step[possb + '_' + posafe] = sensor
                    else:
                        dict_pos_2step[possb + '_' + posafe] = sensor.serial
            except:
                pass

        return {self.sensor_names[pos2step]: serial for
                pos2step, serial in dict_pos_2step.items()}

    def list_afes(self, return_item=False):
        dict_out = {}
        for pos, idafe in self.contains.items():
            try:
                afe = BettairItem(_id=idafe, collection=self.collection)
                if return_item:
                    dict_out[pos] = afe
                else:
                    dict_out[pos] = afe.serial
            except:
                pass
        return dict_out

    def manage(self, return_item=True):
        self.dict_sensors = self.list_sensors(return_item=return_item)
        self.dict_afes = self.list_afes(return_item=return_item)

    def assemble_board(self, afe1_serial=None, pm1_serial=None, afe2_serial=None, pm2_serial=None,
                       co2_serial=None, horn_sensor=None, extra_T_sensor=None):

        if afe1_serial != None:
            afe1 = Afe()
            if not afe1.load_from_fields(serial=afe1_serial):
                print(f'Check that afe {afe1_serial} is in the database')
                print(f'afe {afe1_serial} was not placed in the sensory board')
            else:
                afe1.load_from_fields(serial=afe1_serial)
                afe1.placein(parent_id=self._id, position='1', update_chain=True, autosave=True)

        if afe2_serial != None:
            afe2 = Afe()
            if not afe2.load_from_fields(serial=afe2_serial):
                print(f'Check that afe {afe2_serial} is in the database')
                print(f'afe {afe2_serial} was not placed in the sensory board')
            else:
                afe2.load_from_fields(serial=afe2_serial)
                # TODO
                # self.contain(afe2._id, '2')
                afe2.placein(parent_id=self._id, position='2', update_chain=True, autosave=True)

        if pm1_serial != None:
            pm1 = BettairItem(itemtype='pmsensor')
            if not pm1.load_from_fields(itemtype='pmsensor', serial=pm1_serial):
                pm1.serial = pm1_serial
                pm1.reception = datetime.now(tz=timezone.utc)
                pm1.save(list_fields=['serial', 'itemtype', 'reception'])
            else:
                pm1.load_from_fields(itemtype='pmsensor', serial=pm1_serial)

            pm1.placein(self._id, position='pm1', update_chain=True, autosave=True)

        if pm2_serial != None:
            pm2 = BettairItem(itemtype='pmsensor')
            if not pm2.load_from_fields(itemtype='pmsensor', serial=pm2_serial):
                pm2.serial = pm2_serial
                pm2.reception = datetime.now(tz=timezone.utc)
                pm2.save(list_fields=['serial', 'itemtype', 'reception'])
            else:
                pm2.load_from_fields(itemtype='pmsensor', serial=pm2_serial)

            pm2.placein(self._id, position='pm2', update_chain=True, autosave=True)

        if co2_serial != None:
            co2 = BettairItem(itemtype='co2sensor')
            if not co2.load_from_fields(itemtype='co2sensor', serial=co2_serial):
                co2.serial = co2_serial
                co2.reception = datetime.now(tz=timezone.utc)
                co2.save(list_fields=['serial', 'itemtype', 'reception'])
            else:
                co2.load_from_fields(itemtype='co2sensor', serial=co2_serial)

            co2.placein(self._id, position='ghg', update_chain=True, autosave=True)

        fields_to_save = []
        if horn_sensor != None:
            if horn_sensor in self.horn_sensor_list:
                self.horn_sensor = horn_sensor
                fields_to_save += ['horn_sensor']

            else:
                print(f'horn sensor not in default list: {self.horn_sensor_list}')

        if extra_T_sensor != None:
            self.extra_T_sensor = extra_T_sensor
            fields_to_save += ['extra_T_sensor']

        self.save(list_fields=fields_to_save)

    def _set_influx(self):
        try:
            self.influxcon = InfluxManager(
                **self.influx_connector['influxConnectionInfo'])
        except:
            print('Error initializing influx db connector')

    def get_influx_data(self, t_ini, t_end, influx_fields=None):
        """
        Load data from influxdb
        TODO: check if influxconnector method admits datetime instead of pd.timestamp
        Parameters
        ----------
        t_ini : datetime
            initial datetime of data to retrieve.
        t_end : datetime
            final datetime to data to retrieve.
        influx_fields : dict, optional
            fields for influx query. The default is None.

        Returns
        -------
        df_t : pd.DataFrame
            retrieved data.

        """
        if 'influx_connector' not in self.list_all_fields:
            print('This item type is not connected to influx.'
                  + ' Try container %s' % self.placedin[0])
        flag_ini = True
        df_t = pd.DataFrame()
        try:
            while t_ini < t_end:
                df = self.influxcon.query(
                    self.influx_connector['measure'],
                    {self.influx_connector['tag_key']: self.influx_name},
                    t_ini=t_ini, t_end=t_end, batch_limit=20000,
                    fields=influx_fields)
                if (len(df) == 0) | (not isinstance(df, pd.DataFrame)):
                    break
                if flag_ini:
                    df_t = df
                    flag_ini = False
                else:
                    df_t = pd.concat((df_t, df), axis=0)
                t_ini = df.index[-1] + pd.Timedelta('0.1s')
        except AttributeError:
            print('Error getting data from influx: Connector not found')
        except Exception as e:
            print('Error getting data from influx:')
            print(e)
        if len(df_t) > 0:
            df_t.fillna(value=pd.np.nan, inplace=True)
            # rename columns??
        return df_t

    def get_ini_end_influx(self, option='both'):
        t_ini, t_end = self.influxcon.get_ini_end_measure(
            self.influx_connector['measure'],
            {self.influx_connector['tag_key']: self.influx_name}, )
        # option=option) # No se porqué, al lanzar esto desde el AnomalyTest no reconocía esta variable
        return t_ini.to_pydatetime(), t_end.to_pydatetime()


class Afe(BettairItem):
    def __init__(self, *args, db_name='Bettair_items_db',
                 collection_name='allitems', collection=None, **kwargs):
        if len(args) > 0:
            raise TypeError('__init__() takes no positional arguments')
        super().__init__(db_name=db_name, itemtype='afe',
                         collection_name=collection_name,
                         collection=collection, **kwargs)

    # influx methods go here, not from generic item
    def __repr__(self):
        return 'Afe'

    def set_factory_fields(self, file_name, add_contains=False, reset=False):
        """
        Sets factory data from file
        #tal como esta ahora la afe se usa como el objeto desde
        # el que se llama al get factory data, asi que si existe
        # en mongo db se deberia cargar antes o lo que se hara es
        # introducir una afe nueva en mongo.

        TODO: verificar que los datos de sensores son realmente independientes
        de la afe y si se colocan en otra siguen siendo validos

        Parameters
        ----------
        file_name : str (or io bytes)
            path to file (or to file in memory)
        add_contains: bool
            If True, adds sensors as placed in the afe as indicated in the data
            Default is False

        Returns
        -------
        None.

        """
        flag_found = False
        if self.itemtype != 'afe':
            print('Only AFE calibration files can be read')
            return -1
        # print('Loading factory fields and items from %s in db' % file_name)
        timestamp = datetime.now(tz=timezone.utc)
        data = pd.read_excel(file_name)
        data.rename(columns={col: col.split(' ')[-1]
                             for col in data.columns}, inplace=True)
        # TODO se podria hacer alguna verificación de que el formato es como
        # el que esperamos. Por ejemplo:
        if data['1'][6] != 'AFE Serial No':
            print('Excel file in different format than expected.')
            return -1
        # lo logico es generar un csv con la macro (si va en windows)
        # y sacar del csv
        # porque asi nos aseguramos que ellos parsean los datos
        # donde toca y si han cambiado la tabla no nos equivocamos
        # mejor si directamente les pedimos el csv
        serial = data['2'][6]
        # batch = serial.split('-')[0]

        if self.serial is not None:
            if self.serial != serial:
                print('Afe instance has an assigned serial different'
                      + 'than the one in the excel file.'
                      + ' Add from empty instance or change file.')
                return -1
        elif self.load_from_fields(serial=serial):
            flag_found = True
            if not reset:
                print('Afe %s already found in db. Set' % serial
                      + ' reset=True if you want to update/add'
                      + ' the factory data.')
                return 0

        if add_contains & (len(self.contains) > 0):
            if reset:
                print('Resetting contained sensors to factory settings')
                self.remove_contains(self.contains.keys())
            else:
                print('Afe already saved with contained sensors.'
                      + ' Set reset=True to overwrite.')
                return 0
        self.set_fields(serial=serial,
                        reception=timestamp,
                        afe_type=data['2'][9],
                        circuit_type=data['2'][10],
                        zero_cal_date=data['2'][5],
                        set_fixed=True,
                        sboardAssigned=False,
                        state='NOT_TESTED')  # , autosave=False)
        # list_afe_fields=['serial', 'afe_type', 'circuit_type',
        #                        'zero_cal_date']
        # self.contains = []

        # Iteration range depends on AFE size
        slots = {
            '810-0021':
                {
                    '00': {'1': 'NO2/O3', '2': 'NO2/O3'},
                    '01': {'1': 'NO2/O3', '2': 'NO'},
                    '02': {'1': 'NO2/O3', '2': 'CO/SO2/H2S'},
                    '03': {'1': 'NO', '2': 'CO/SO2/H2S'},
                    '04': {'1': 'CO/SO2/H2S', '2': 'CO/SO2/H2S'}
                },
            '810-0019':
                {
                    '00': {'1': 'NO2/O3', '2': 'NO2/O3', '3': 'CO/SO2/H2S'},
                    '01': {'1': 'NO2/O3', '2': 'NO2/O3', '3': 'NO'},
                    '02': {'1': 'NO2/O3', '2': 'CO/SO2/H2S', '3': 'CO/SO2/H2S'},
                    '03': {'1': 'NO2/O3', '2': 'CO/SO2/H2S', '3': 'NO'},
                    '04': {'1': 'CO/SO2/H2S', '2': 'CO/SO2/H2S', '3': 'CO/SO2/H2S'}
                },
            '810-0023':
                {
                    '00': {'1': 'NO2/O3', '2': 'NO2/O3', '3': 'CO/SO2/H2S', '4': 'CO/SO2/H2S'},
                    '01': {'1': 'NO2/O3', '2': 'NO2/O3', '3': 'NO', '4': 'CO/SO2/H2S'},
                    '02': {'1': 'NO2/O3', '2': 'CO/SO2/H2S', '3': 'CO/SO2/H2S', '4': 'CO/SO2/H2S'},
                    '03': {'1': 'NO2/O3', '2': 'CO/SO2/H2S', '3': 'NO', '4': 'CO/SO2/H2S'},
                    '04': {'1': 'CO/SO2/H2S', '2': 'CO/SO2/H2S', '3': 'NO', '4': 'CO/SO2/H2S'}
                },
        }
        if isinstance(self.circuit_type, str):
            self.set_fields({'slot_types': slots[self.afe_type][self.circuit_type]})

        slot_len = {'810-0021': 2, '810-0019': 3, '810-0023': 4}

        for i in range(slot_len[self.afe_type]):
            self.set_fields({f'pcb_gain_{i + 1}': data[str(i + 3)][26],
                             f'WE_{i + 1}_offset': data[str(i + 3)][17],
                             f'AE_{i + 1}_offset': data[str(i + 3)][20]},
                            set_fixed=True)  # , autosave=False)
            # list_afe_fields += [f'pcb_gain_{i+1}', f'WE_{i+1}_offset',
            #                     f'AE_{i+1}_offset']
            sensor_type = data[str(i + 3)][14]
            serial = str(data[str(i + 3)][15])
            # search for sensor in database
            sensor = Sensor(collection=self.collection)  # BettairItem(itemtype='sensor')
            if not sensor.load_from_fields(serial=serial):
                # Sensor not in database
                print('Adding new sensor %s in db' % serial)
                sensor.set_fields(serial=serial, reception=timestamp,
                                  set_fixed=True,
                                  # batch='afe_' + batch, El batch debe venir del archivo de sensores. No poner cuando viene de AFE
                                  autosave=False)
                list_sensor_fields = ['serial', 'reception']
            else:
                list_sensor_fields = []
            # even if the sensor existed, we load/reload the factory fields
            sensor.set_fields(
                sensor_type=sensor_type,
                WE_zero_from_afe=data[str(i + 3)][18] / data[str(i + 3)][26],
                AE_zero_from_afe=data[str(i + 3)][21] / data[str(i + 3)][26],
                sensitivity=data[str(i + 3)][24],
                autosave=False)
            if add_contains:
                sensor.set_fields(placedin=[self._id, str(i + 1)],
                                  autosave=False)
                list_sensor_fields += ['placedin']
            list_sensor_fields += ['sensor_type', 'WE_zero_from_afe',
                                   'AE_zero_from_afe', 'sensitivity']

            # Save automatically sensor data in db
            sensor.save(list_fields=list_sensor_fields)
        # self.save(list_fields=list_afe_fields)
        if flag_found:
            return 2
        else:
            return 1

    def substitute_sensor(self, old_sensor_serial, new_sensor_serial, old_sensor_status=None):
        """
        Substitutes sensor in AFE

        Parameters
        ----------
        old_sensor_serial : str (or io bytes)
            serial number of sensor in AFE to be substituted
        new_sensor_serial : str (or io bytes)
            serial number of sensor not in any AFE that will substitute old_sensor
        old_sensor_status: str
            new status of old sensor (can be used to specify the change reason)

        Returns
        -------
        None.

        """
        # All sensors should be already in the db
        # Check that sensor exists
        sensor_old, sensor_new = BettairItem(itemtype='sensor'), BettairItem(itemtype='sensor')
        if not sensor_old.load_from_fields(serial=old_sensor_serial):
            print(f'sensor with serial {old_sensor_serial} is not in database')
            print('Nothing was done')
        elif not sensor_new.load_from_fields(serial=new_sensor_serial):
            print(f'sensor with serial {new_sensor_serial} is not in database')
            print('Nothing was done')
        else:
            sensor_old.load_from_fields(serial=old_sensor_serial)
            sensor_new.load_from_fields(serial=new_sensor_serial)

            try:
                position = sensor_old.placedin[1]
            except:
                print(f'Sensor with serial {old_sensor_serial} is not placed in any AFE')
                return 0

            # Check that old sensor is in afe
            if sensor_old._id != self.contains[position]:
                print(f'Sensor with serial {old_sensor_serial} is not in AFE {self.serial}')
                print('Nothing was done')

            # Check that new sensor is in no afe
            elif sensor_new.placedin != None:
                afe2 = Afe()
                afe2.load_from_fields(_id=sensor_new.placedin[0])
                print(f'Sensor with serial {new_sensor_serial} is already in another AFE ({afe2.serial})')
                print('Nothing was done')

            else:
                self.remove_contains(positions=[position], update_chain=True, autosave=True)
                sensor_new.placein(self._id, position=position, update_chain=True, autosave=True)
                sensor_old.status = old_sensor_status
                sensor_old.placedin = None
                sensor_old.save(['status', 'placedin'])
                self.contains[position] = sensor_new._id
                self.save(['contains'])
                print(
                    f'Sensor {sensor_new.serial} substitutes sensor {sensor_old.serial} in position {position} of AFE {self.serial}')


class Sensor(BettairItem):
    def __init__(self, *args, db_name='Bettair_items_db',
                 collection_name='allitems', collection=None, **kwargs):
        if len(args) > 0:
            raise TypeError('__init__() takes no positional arguments')
        super().__init__(db_name=db_name, itemtype='sensor',
                         collection_name=collection_name,
                         collection=collection, **kwargs)

    # influx methods go here, not from generic item
    def __repr__(self):
        return 'Sensor'




class StorageBox(BettairItem):
    def __init__(self, *args, db_name='Bettair_items_db',
                 collection_name='allitems', collection=None, **kwargs):
        if len(args) > 0:
            raise TypeError('__init__() takes no positional arguments')
        super().__init__(db_name=db_name, itemtype='sbox',
                         collection_name=collection_name,
                         collection=collection, **kwargs)
        if 'influx_connector' in self.list_all_fields:
            self._set_influx()
            # if influx connector config not passed in init, this will
            # use default parameters:
        if 'sensor_names' in self.list_all_fields:
            self.sensor_names = sensor_names_default
        self.dict_afes = {}
        self.dict_sensors = {}

    # influx methods go here, not from generic item
    def __repr__(self):
        return 'StorageBox'

    def list_sensors(self, return_item=False):
        dict_pos_2step = {}
        for possb, idafe in self.contains.items():
            try:
                afetmp = BettairItem(_id=idafe, collection=self.collection)
                for posafe, idsens in afetmp.contains.items():
                    try:
                        sensor = BettairItem(_id=idsens,
                                             collection=self.collection)
                    except:
                        pass
                    if return_item:
                        dict_pos_2step[possb + '_' + posafe] = sensor
                    else:
                        dict_pos_2step[possb + '_' + posafe] = sensor.serial
            except:
                pass

        return {self.sensor_names[pos2step]: serial for
                pos2step, serial in dict_pos_2step.items()}

    def list_afes(self, return_item=False):
        dict_out = {}
        for pos, idafe in self.contains.items():
            try:
                afe = BettairItem(_id=idafe, collection=self.collection)
                if return_item:
                    dict_out[pos] = afe
                else:
                    dict_out[pos] = afe.serial
            except:
                pass
        return dict_out

    def manage(self, return_item=True):
        self.dict_sensors = self.list_sensors(return_item=return_item)
        self.dict_afes = self.list_afes(return_item=return_item)

    '''    
    def _set_influx(self):
        try:
            self.influxcon = InfluxManager(
                **self.influx_connector['influxConnectionInfo'])
        except:
            print('Error initializing influx db connector')

    def get_influx_data(self, t_ini, t_end, influx_fields=None):
        """
        Load data from influxdb
        TODO: check if influxconnector method admits datetime instead of pd.timestamp
        Parameters
        ----------
        t_ini : TYPE
            DESCRIPTION.
        t_end : TYPE
            DESCRIPTION.
        influx_fields : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        df_t : TYPE
            DESCRIPTION.

        """
        if 'influx_connector' not in self.list_all_fields:
            print('This item type is not connected to influx.'
                  + ' Try container %s' % self.placedin[0])
        flag_ini = True
        df_t = pd.DataFrame()
        try:
            while t_ini < t_end:
                df = self.influxcon.query(
                        self.influx_connector['measure'],
                        {self.influx_connector['tag_key']: self.influx_name},
                        t_ini=t_ini, t_end=t_end, batch_limit=20000,
                        fields=influx_fields)
                if (len(df) == 0) | (not isinstance(df, pd.DataFrame)):
                    break
                if flag_ini:
                    df_t = df
                    flag_ini = False
                else:
                    df_t = pd.concat((df_t, df), axis=0)
                t_ini = df.index[-1] + pd.Timedelta('0.1s')
        except AttributeError:
            print('Error getting data from influx: Connector not found')
        except Exception as e:
            print('Error getting data from influx:')
            print(e)
        if len(df_t) > 0:
            df_t.fillna(value=pd.np.nan, inplace=True)
            # rename columns??
        return df_t

    def get_ini_end_influx(self, option='both'):
        t_ini, t_end = self.influxcon.get_ini_end_measure(
            self.influx_connector['measure'],
            {self.influx_connector['tag_key']: self.influx_name},
            option=option)
        return t_ini.to_pydatetime(), t_end.to_pydatetime()
    '''


class ItemManager():
    def __init__(self, db_name='Bettair_items_db',
                 collection_name='allitems', date_ini=None):
        # init mongo connector
        self.db_name = db_name
        self.client = MongoClient(
            'mongodb://' + user + ':' + pw + '@' + host + ':'
            + port + '/?authSource=admin')
        self.db = self.client[self.db_name]
        self.collection_name = collection_name
        self.collection = self.db[self.collection_name]
        # fetch list of all items by type
        self.dict_list_items = {}
        self.dict_list_free = {}
        # for itemtype in ['sensor', 'afe', 'sboard']:
        #     self.dict_list_items[itemtype] = self.find_by_type(itemtype,
        #                                                        date_ini=date_ini)
        # for itemtype in ['sensor', 'afe']:
        #     self.dict_list_free[itemtype] = self.find_free(itemtype)

        self.get_influx_connectors()
        self.to_save = set()

    def get_influx_connectors(self):
        self.dict_influx = {}
        # self.dict_influx_names
        for itserial, itid in self.dict_list_items['sboard'].items():
            sboard = BettairItem(_id=itid, collection=self.collection)
            self.dict_influx[itserial] = {
                'influx_name': sboard.influx_name,
                'inlfux_conn': sboard.influx_connector}

    def find_by_type(self, _type, date_ini=None):
        """
        Returns all documents of itemtype _type

        """
        # allows a simplified type label : point, line, polygon_all
        if date_ini is None:
            filterquery = {'itemtype': _type}
        else:
            if not isinstance(date_ini, datetime):
                date_ini = pd.to_datetime(date_ini, utc=True).to_pydatetime()
            filterquery = {'itemtype': _type,
                           'reception': {'$gte': date_ini}
                           }
        return {xx['serial']: xx['_id'] for xx in
                self.collection.find(filterquery, {'serial': 1, })}

    def count_by_type(self, _type):
        """
        Returns the number of documents of itemtype _type

        """
        return self.collection.count_documents({'itemtype': _type})

    def find_free(self, itemtype):
        return {xx['serial']: xx['_id'] for xx in
                self.collection.find({'itemtype': itemtype},
                                     {'placedin': None})}

    def find_items(self, ini=None, end=None,
                   itemtypes=['sensor', 'afe', 'sboard']):
        for itemtype in itemtypes:
            for itkey, itid in self.dict_list_items[itemtype].items():
                print('todo')

    def add_to_save(self, item):
        self.to_save.add(item)

    def save(self):
        for item in self.to_save:
            item.save()
        self.to_save = []

    def set_sensor_factory_fields(self, file_name):
        """
        Sets factory data from file

        Parameters
        ----------
        file_name : str (or io bytes)
            path to file (or to file in memory)

        Returns
        -------
        None.

        """

        print('Loading factory fields and items from %s in db' % file_name)
        timestamp = datetime.now(tz=timezone.utc)
        data = pd.read_excel(file_name)
        data.rename(columns={col: col.split(' ')[-1]
                             for col in data.columns}, inplace=True)
        # Exception if file_name is of type UploadedFile
        if not isinstance(file_name,str):
            file_name = file_name.name
            
        # TODO se podria hacer alguna verificación de que el formato es como
        # el que esperamos. Por ejemplo:
        if data['1'][4] != 'Serial No':
            print('Excel file in different format than expected.')
            return -1

        sensor_type = data['2'][1]

        if '/' in file_name:
            file_name = file_name.split('/')[-1]
        batch = int(file_name.split('_')[0])

        for i in range(9, len(data)):
            
            sensor = Sensor()
            serial = str(data[str(1)][i])
            # search for sensor in database
            # sensor = BettairItem(itemtype='sensor')
            if not sensor.load_from_fields(serial=serial):
                # Sensor not in database
                print('Adding new sensor %s in db' % serial)
                sensor.set_fields(serial=serial, reception=timestamp, batch=batch,
                                set_fixed=True, autosave=False)
                list_sensor_fields = ['serial', 'batch']
            else:
                list_sensor_fields = []
            # even if the sensor existed, we load/reload the factory fields
            sensor.set_fields(
                sensor_type=sensor_type,
                WE_zero=data[str(2)][i],
                AE_zero=data[str(3)][i],
                sensitivity=data[str(4)][i] / 1000,
                autosave=False)
            list_sensor_fields += ['sensor_type', 'WE_zero',
                                   'AE_zero', 'sensitivity']

            # Save automatically sensor data in db
            sensor.save(list_fields=list_sensor_fields)

if __name__ == '__main__':
    afe = Afe(collection_name='allitems2')
    o = afe.set_factory_fields('/home/davidmatas/projects/bettair/data-structures/afes/25-000500.xls',
                               add_contains=True)
    # o = afe.set_factory_fields('/home/davidmatas/projects/bettair/data-structures/afes/25-000499.xls', add_contains=True, reset=True)
    # afe.contain('607971422b86b47b67b35b9f', '1')