import socket
import time
import crcmod
import threading

TIME_OUT = 5.0
# use vircom software to configure network parameters
# Gateway Address
# gw.anomaly.bettair.es
#IP_GATEWAY = "10.7.1.163"

# Front end address
# anomaly.bettair.es
#IP_FRONT_END = "10.7.1.164"
HOST_NAME = "gw.anomaly.bettair.es"
PORT_GATEWAY = 502
DEVICE_RELAYS = "10" # 0x10
COMPRESSOR = 0
FRIDGE_LIGHT = 1
HEATER = 2

OFFLINE = False

class Modbus():
    def __init__(self):
        self.clientSocket = None
        self.crc16 = crcmod.mkCrcFun(0x18005, rev=True, initCrc=0xFFFF, xorOut=0x0000)
    
    def is_mod_debug_mode(self):
        return OFFLINE
    
    def twos_comp(self, hex_str):
        bin_str = f"{int(hex_str, 16) : 016b}"[2:]
        val = int(bin_str,2)
        #print(val)
        bits = len(bin_str)
        if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
            val = val - (1 << bits)        # compute negative value
        return val           

    def connect_gateway(self):
        # Create a client socket
        if (OFFLINE):
            print("Offline Mode...not connected !!!!")
            return False
        
        self.clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clientSocket.settimeout(TIME_OUT)
        
        if (self.clientSocket):
            try:
                # Connect to the server
                host = socket.gethostbyname(HOST_NAME)
                print(host)
                self.clientSocket.connect((host, PORT_GATEWAY))
                
                return True
            except Exception as e:
                print(e)
                
        return False
    
    def close_gateway(self):
        try:
            if (self.clientSocket):
                self.clientSocket.shutdown(socket.SHUT_RDWR)
                self.clientSocket.close()
                self.clientSocket = None
        except Exception as e:
            print(e)
            return False
        
        return True

    def get_crc16(self, txt):
        tmp = str(hex(self.crc16(bytes.fromhex(txt))))
        while (len(tmp) < 6):
            tmp = tmp[:2] + "0" + tmp[2:]
        result = tmp[4:6] + " " + tmp[2:4]
        #print(result)
        return " " + result

    # Open all relays
    def close_all_relay(self):
        try:
            time.sleep(1)
            print(f"Closing Relay {relay}")
            # Send data to server
            data = f"{DEVICE_RELAYS} 05 00 FF 00 00" 
            #print(data + self.get_crc16(data))
            self.clientSocket.send(bytes.fromhex(data + self.get_crc16(data)))
            # Receive data from server
            #time.sleep(0.5)
            dataFromServer = self.clientSocket.recv(1024)
            #print(dataFromServer.hex())
            # Print to the console
            if  (str(dataFromServer.hex())[0:12].upper() ==  data.replace(" ", "")[0:12].upper()):
                print(f"All Relays Openned")
                return True
        except TimeoutError:
            print(f"Time out closing relay {relay}")
        except Exception as e:
            print(e)
        return False
    
    # Open relay from 1 to 8
    def open_relay(self, relay):
        MAX_TRIES = 10
        for i in range(MAX_TRIES):
            try:
                time.sleep(1)
                print(f"Activating Relay {relay}")
                # Send data to server
                data = f"{DEVICE_RELAYS} 05 00 {relay:02x} FF 00" 
                #print(data + self.get_crc16(data))
                self.clientSocket.send(bytes.fromhex(data + self.get_crc16(data)))
                # Receive data from server
            
                #time.sleep(0.5)
                dataFromServer = self.clientSocket.recv(1024)
                #print(dataFromServer.hex())
                # Print to the console
                if  (str(dataFromServer.hex())[0:12].upper() ==  data.replace(" ", "")[0:12].upper()):
                    print(f"Relay {relay} Activated")
                    return True
                print(f"({MAX_TRIES-i}) Retrying open relay {relay}...")
            except TimeoutError:
                print(f"Time out opening relay {relay}")
            except Exception as e:
                print(e)
        
        print(f"Fail opening relay {relay}")
        return False
       
    # Close relay from 1 to 8
    def close_relay(self, relay):
        MAX_TRIES = 10
        for i in range(MAX_TRIES):
            try:
                time.sleep(1)
                print(f"Stopping Relay {relay}")
                # Send data to server
                data = f"{DEVICE_RELAYS} 05 00 {relay:02x} 00 00" 
                #print(data + self.get_crc16(data))
                self.clientSocket.send(bytes.fromhex(data + self.get_crc16(data)))
                # Receive data from server
            
                #time.sleep(0.5)
                dataFromServer = self.clientSocket.recv(1024)
                # Print to the console
                #print(dataFromServer.hex())
                if  (str(dataFromServer.hex())[0:12] ==  data.replace(" ", "")[0:12]):
                    print(f"Relay {relay} Stopped")
                    return True
                print(f"({MAX_TRIES-i}) Retrying close relay {relay}...")
            except TimeoutError:
                print("Time out")
            except Exception as e:
                print(e)
        
        print(f"Fail closing relay {relay}")
        return False
    
    # Start and stop compressor
    # state = True --- running
    # state = False --- stop
    def set_compressor(self, state):
        if state:
            return self.open_relay(COMPRESSOR)
        else:
            return self.close_relay(COMPRESSOR)
    
    # Start and stop fridge light
    # state = True --- running
    # state = False --- stop
    def set_fridge_light(self, state):
        if state:
            return self.open_relay(FRIDGE_LIGHT)
        else:
            return self.close_relay(FRIDGE_LIGHT)
    
    # Start and stop Heater
    # state = True --- running
    # state = False --- stop
    def set_heater(self, state):
        if state:
            t = threading.Thread(target=self.heater_worker)
            t.start()
        else:
            return self.close_relay(HEATER)
        """
            return self.open_relay(HEATER)
        else:
            return self.close_relay(HEATER)
        """
        
    def heater_worker(self):
        for i in range(6):
            self.open_relay(HEATER)
            time.sleep(5)
            self.close_relay(HEATER)
            time.sleep(5)
        

           
    # Read temperature and humidity from sensor xy-MD02
    def get_temp_hum(self, device):
        MAX_TRIES = 10
        for i in range(MAX_TRIES):
            try:
                time.sleep(1)
                # Send data to server
                data = f"{device} 04 00 01 00 02" 
                #print(data + self.get_crc16(data))
                self.clientSocket.send(bytes.fromhex(data + self.get_crc16(data)))
                # Receive data from server
            
                #time.sleep(0.5)
                dataFromServer = self.clientSocket.recv(1024)
                # Print to the console
                #print(dataFromServer)
                #print(dataFromServer.hex())
                #temp = self.twos_comp("fff3")
                temp = self.twos_comp(dataFromServer.hex()[6:10])
                #print(temp)
                #print(f"{int(temp,16)/10} ºC")
                hum = self.twos_comp(dataFromServer.hex()[10:14])
                #print(hum)
                #print(f"{int(hum,16)/10} %")
                if (temp and hum):
                    return [temp/10, hum/10]
                print(f"({MAX_TRIES-i}) Retrying get temperature and humidity...")
            except Exception as e:
                print(e)
            except TimeoutError:
                print("Time out")
        
        print("Fail reading sensors")
        return []
    
    # Modify Device Address in sensor xy-MD02
    # REMEMBER TO RESET DEVICE
    # REMEMBER TO RESET DEVICE
    # REMEMBER TO RESET DEVICE
    def modify_address_device(self, origin_device, final_device):
        try:
            time.sleep(1)
            # Send data to server
            data = f"{origin_device} 06 01 01 00 {final_device}" 
            #print(data + self.get_crc16(data))
            self.clientSocket.send(bytes.fromhex(data + self.get_crc16(data)))
            # Receive data from server
            time.sleep(0.5)
            dataFromServer = self.clientSocket.recv(1024)
            # Print to the console
            #print(dataFromServer.hex())
            if data.replace(" ","") in dataFromServer.hex():
                return True
            
        except TimeoutError:
            print("Time out")
        except Exception as e:
            print(e)
        
        return False
    
    # Adjust temperature in sensor xy-MD02
    def adjust_temp(self, device, temp):
        try:
            time.sleep(0.5)
            if temp < -100:
                temp = 100
            elif temp > 100:
                temp = 100
            print(f"Device {device} set {temp/10} temperature offset")
            # Send data to server
            data = f"{device} 06 01 03 {temp:04x}" 
            data = data[:-2] + " " +data[-2:]
            #print(data + self.get_crc16(data))
            self.clientSocket.send(bytes.fromhex(data + self.get_crc16(data)))
            # Receive data from server
            time.sleep(0.5)
            dataFromServer = self.clientSocket.recv(1024)
            # Print to the console
            # print(dataFromServer.hex())
            #print(data[:-6].replace(" ",""))
            #print(str(dataFromServer))
            if data[:-6].replace(" ","") in dataFromServer.hex():
                print("Temperature adjusted succesful")
                return True
            
        except TimeoutError:
            print("Time out")
        except Exception as e:
            print(e)
        
        print("Fail adjusting temperature")
        return False
    
    # Adjust Humidity in sensor xy-MD02
    def adjust_hum(self, device, hum):
        try:
            time.sleep(0.5)
            if hum < -100:
                hum = 100
            elif hum > 100:
                hum = 100
            print(f"Device {device} set {hum/10} humidity offset")
            # Send data to server
            data = f"{device} 06 01 04 {hum:04x}" 
            data = data[:-2] + " " +data[-2:]
            #print(data + self.get_crc16(data))
            self.clientSocket.send(bytes.fromhex(data + self.get_crc16(data)))
            # Receive data from server
            time.sleep(0.5)
            dataFromServer = self.clientSocket.recv(1024)
            # Print to the console
            #print(dataFromServer.hex())
            if data[:-6].replace(" ","") in dataFromServer.hex():
                print("Humidity adjusted succesful")
                return True
            
        except TimeoutError:
            print("Time out")
        except Exception as e:
            print(e)
        
        print("Fail adjusting Humidity")
        return False
    
    # Modify Device Address in sensor xy-MD02
    def send_raw(self, data):
        # Send data to server
        #data = f"{origin_device} 06 01 01 00 {final_device}" 
        #print(data + self.get_crc16(data))
        self.clientSocket.send(bytes.fromhex(data + self.get_crc16(data)))
        # Receive data from server
        try:
            dataFromServer = self.clientSocket.recv(1024)
            # Print to the console
            #print(dataFromServer.hex())
           
            
        except TimeoutError:
            print("Time out")
        except Exception as e:
            print(e)
    
    # match both sensors
    def match_sensors(self):
        for i in range(5):
            if (self.match_sensor_temp()):
                for i in range(5):
                    if (self.match_sensor_hum()):
                        return True

        return False
    
    # match both sensors
    def match_sensor_temp(self):
        print("Matching temperature sensors")
        # initialize adjust to Zero offset
        if (not self.adjust_temp("01", 0)):
            print(f"Fail initializing to zero device 1 ")
            return False
        #time.sleep(1)
        if (not self.adjust_temp("02", 0)):
            print(f"Fail initializing to zero device 1 ")
            return False
        
        # Try for 5 times to match temperature        
        for i in range(5):
            #time.sleep(1)
            # Get primary temperature and humidity
            p_temp_hum = self.get_temp_hum("01")
            print(f"Main temp: {p_temp_hum}")
            if (not p_temp_hum):
                return False
            #time.sleep(1)
            # Get secondary temperature and humidity
            s_temp_hum = self.get_temp_hum("02")
            print(f"Backup temp: {s_temp_hum}")
            if (not s_temp_hum):
                return False
            print(s_temp_hum)
            
            if (not p_temp_hum or not s_temp_hum):
                print(f" ({10-i}) Retry to match sensors ... ")
                continue
            
            # match temperature
            if (p_temp_hum[0] > s_temp_hum[0]):
                diff = int((p_temp_hum[0] - s_temp_hum[0])*10)
                print(diff)
                if (self.adjust_temp("02", diff)):
                    return True
            else:
                diff = int((s_temp_hum[0] - p_temp_hum[0])*10)
                print(diff)
                if (self.adjust_temp("01", diff)):
                    return True
            
        print("Fail matching sensors")
        return False
    
    # match both sensors
    def match_sensor_hum(self):
        print("Matching humidity sensors")
        # initialize adjust to Zero offset
        if (not self.adjust_hum("01", 0)):
            print(f"Fail initializing to zero device 1 ")
            return False
        time.sleep(1)
        if (not self.adjust_hum("02", 0)):
            print(f"Fail initializing to zero device 1 ")
            return False
        
        for i in range(5):
            #time.sleep(1)
            # Get primary temperature and humidity
            p_temp_hum = self.get_temp_hum("01")
            print(p_temp_hum)
            #time.sleep(1)
            # Get secondary temperature and humidity
            s_temp_hum = self.get_temp_hum("02")
            print(s_temp_hum)
            
            if (not p_temp_hum or not s_temp_hum):
                print(f" ({10-i}) Retry to match sensors ... ")
                continue
            
            # match temperature
            if (p_temp_hum[1] > s_temp_hum[1]):
                diff = int((p_temp_hum[1] - s_temp_hum[1])*10)
                print(diff)
                return self.adjust_hum("02", diff)
            else:
                diff = int((s_temp_hum[1] - p_temp_hum[1])*10)
                print(diff)
                return self.adjust_hum("01", diff)
            
        return False
           
    def tohex(self, val, nbits):
        return hex((val + (1 << nbits)) % (1 << nbits))    
        

if __name__ == "__main__":
    obj = Modbus()
    obj.connect_gateway()
    
    #for i in range(10):
    #    obj.set_heater(True)
    #    time.sleep(60)
    
    #obj.match_sensors()
    """
    obj.adjust_temp("01", 0)
    obj.adjust_temp("02", 0)
    obj.adjust_hum("01", 0)
    obj.adjust_hum("02", 0)
    """
    """
    obj.close_all_relay()
    for i in range(8):
        print(f"Relay {i}")
        obj.open_relay(i)
        #time.sleep(5)
        obj.close_relay(i)
        #time.sleep(5)
    """
    
    """
    # Modify Address device
    #obj.modify_address_device("01", "02")
    
    # Send raw frames
    #obj.send_raw("01 03 01 01 00 01")
    #obj.send_raw("01 06 01 01 00 02")
    import datetime
    #state = False
    while True:
        # Get primary temperature and humidity
        print(f"{datetime.datetime.now()}")
        p_temp_hum = obj.get_temp_hum("01")
        print(p_temp_hum)
        # Get secondary temperature and humidity
        s_temp_hum = obj.get_temp_hum("02")
        print(s_temp_hum)
        time.sleep(60)
        #obj.set_heater(state)
        #state = not state
    
    #obj.match_sensors()
    """
    
    
    """
    #for i in range(5):
    time.sleep(1)
    # Get primary temperature and humidity
    p_temp_hum = obj.get_temp_hum("01")
    print(p_temp_hum)
    time.sleep(1)
    # Get secondary temperature and humidity
    s_temp_hum = obj.get_temp_hum("02")
    print(s_temp_hum)
    """    
    """
    if (p_temp_hum and s_temp_hum):
        if (str(p_temp_hum[0]).replace(".","").isdigit() and str(s_temp_hum[0]).replace(".","").isdigit()):
            # Adjust temp
            adj_temp = int((p_temp_hum[0] - s_temp_hum[0])*10)
            print(adj_temp)
            if (adj_temp < 0):
                print(adj_temp)
                adj_temp = -1 * adj_temp
                adj_temp = ~adj_temp* + 1
                print(adj_temp)
            print(adj_temp)
            print(obj.tohex(adj_temp, 16))
            print(type(obj.tohex(adj_temp, 16)))
            print(bytes.fromhex(obj.tohex(adj_temp, 16)))
            #adj_temp_a2_hex = hex(adj_temp_a2)
            #print(adj_temp_a2_hex)
            #print(type(adj_temp_hex))
            #adj_temp_hex_a2 = ~adj_temp_a2_hex + 1
            #print(adj_temp_hex_a2)
            #print(bytes.fromhex(adj_temp_hex))
            
            obj.adjust_temp("02", (obj.tohex(adj_temp, 16)))
            obj.adjust_temp("02", 0)
        """
    #print("Close gateway")
    #print(obj)
    #obj.close_gateway()
    #print("closed")
    """
    while True:
        print(obj.get_temp_hum("01"))
        print(obj.get_temp_hum("02"))
        time.sleep(60)
        obj.set_heater(True)
    """
    
    """
    for i in range(10):
        obj.set_heater(True)
        time.sleep(60)
    """

