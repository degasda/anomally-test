import mysql.connector
#from datetime import datetime, timedelta, timezone
import pandas as pd
import datetime
import numpy as np
from os import listdir
import json
import operator
import influx_test as flux
from utils import mide_tiempo
import _pickle as cPickle
import matplotlib.pyplot as plt
import datetime
import utils
import api_restful

DAYS_OUTDATE = 365 # 1 any

class SqlConnector():
    
    def __init__(self):
        self.msg = ""
    
    # ----------------- GENERAL -----------------------------
    def connect(self):
        return mysql.connector.connect(
            host="mysql.bettair.es",
            user="production",
            password="6KRnHSf4Pfj8VDAGuwWrRuJq",
            database="production"
            )
        
    def insert_field(self, table, field_check, value_check, field_insert, value_insert):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = f"UPDATE {table} SET {field_insert} = '{value_insert}' WHERE {field_check} = '{value_check}'"
            mycursor.execute(sql)
            mydb.commit()
            sql = f"SELECT {field_insert} from {table} WHERE {field_check} = '{value_check}'"
            mycursor.execute(sql)
            myresult = mycursor.fetchone()
            #print(myresult)
            print(f"Inserted {field_insert} to value {value_insert} in {table} database {field_check} value {value_check}")
            self.msg = f"Inserted {field_insert} to value {value_insert} in {table} database {field_check} value {value_check}"
            if (myresult):
                return True
        self.msg = f"FAIL inserting {field_insert} to value {value_insert} in {table} database {field_check} value {value_check}"
        return False
    
    # example dic for sensors: {'ae_zero' : '0.0', 'batch' : '1234', 'changes' : 'change1#change2#change3', 'date_reception' : '2022-03-28 06:30:59' , 'gas' : 'NO2', 'last_update' : '2022-03-28 06:30:59', 'observations' : '-', 'sensitivity_mv' : '0', 'sensitivity_na' : '0', 'sensitivity_no2_mv' : '0', 'sensitivity_no2_na' : '0', 'sn' : '212730222', 'state' : 'NOT_TESTED', 'we_zero' : '0', 'zero_cal_date' : '0'}
    # example dic for afes: {'ae_zero_sn1' : '0.0', 'ae_zero_sn2' : '0.0', 'ae_zero_sn3' : '0.0', 'ae_zero_sn4' : '0.0', 'batch' : '80212', 'changes' : 'change1#change2#change3', 'date_reception' : '2022-03-28 06:30:59', 'last_update' : '2022-03-28 06:30:59', 'location' : 'SBOX_4-1-1', 'observations' : '-', 'pcb_gain_sn1' : '0', 'pcb_gain_sn2' : '0', 'pcb_gain_sn3' : '0', 'pcb_gain_sn4' : '0', 'pn' : '810-0023-00', 'sn' : '25-001550', 'sn1' : '212730222', 'sn2' : '214170147', 'sn3' : '132731017', 'sn4' : '134730255', 'state' : '-', 'we_zero_sn1' : '0', 'we_zero_sn2' : '0', 'we_zero_sn3' : '0', 'we_zero_sn4' : '0', 'zero_cal_date' : '02/03/2023'}
    def create_entry(self, table, dic):
        mydb = self.connect()
        if (mydb):
            try:
                sn = dic['sn']
                mycursor = mydb.cursor()
                keys = (key for key in dic)
                val = [dic[key] for key in dic]
                sql = f"INSERT INTO {table} ({', '.join(keys)}) VALUES ({('%s, '*len(dic))[:-2]});"  
                #print(sql)
                #print(val)
                mycursor.execute(sql, val)
                mydb.commit()
                sql = f"SELECT sn from {table} WHERE sn = '{sn}'"
                #print(sql)
                mycursor.execute(sql)
                myresult = mycursor.fetchone()
                #print(myresult)
                self.msg +=  f"Inserted new {table} with serial {sn}\n"
                if (myresult):
                    return True
            except Exception as e:
                print(e)
                self.msg += f"ERROR: {e}"
            self.msg +=  f"FAIL inserting new {table} with serial {sn}\n"
            #self.msg +=  f"{dic}"
        return False
    
    # example dic for sensors: {'ae_zero' : '0.0', 'batch' : '1234', 'changes' : 'change1#change2#change3', 'date_reception' : '2022-03-28 06:30:59' , 'gas' : 'NO2', 'last_update' : '2022-03-28 06:30:59', 'observations' : '-', 'sensitivity_mv' : '0', 'sensitivity_na' : '0', 'sensitivity_no2_mv' : '0', 'sensitivity_no2_na' : '0', 'sn' : '212730222', 'state' : 'NOT_TESTED', 'we_zero' : '0', 'zero_cal_date' : '0'}
    # example dic for afes: {'ae_zero_sn1' : '0.0', 'ae_zero_sn2' : '0.0', 'ae_zero_sn3' : '0.0', 'ae_zero_sn4' : '0.0', 'batch' : '80212', 'changes' : 'change1#change2#change3', 'date_reception' : '2022-03-28 06:30:59', 'last_update' : '2022-03-28 06:30:59', 'location' : 'SBOX_4-1-1', 'observations' : '-', 'pcb_gain_sn1' : '0', 'pcb_gain_sn2' : '0', 'pcb_gain_sn3' : '0', 'pcb_gain_sn4' : '0', 'pn' : '810-0023-00', 'sn' : '25-001550', 'sn1' : '212730222', 'sn2' : '214170147', 'sn3' : '132731017', 'sn4' : '134730255', 'state' : '-', 'we_zero_sn1' : '0', 'we_zero_sn2' : '0', 'we_zero_sn3' : '0', 'we_zero_sn4' : '0', 'zero_cal_date' : '02/03/2023'}
    def update_entry(self, table, dic):
        mydb = self.connect()
        if (mydb):
            try:
                sn = dic['sn']
                dic.pop('sn')
                mycursor = mydb.cursor()
                keys = (key for key in dic)
                val = [dic[key] for key in dic]
                keys_str = ""
                for key in keys:
                    keys_str += f"{key}=%s,"
                keys_str = keys_str[:-1]      
                #print(keys_str)
                sql = f"UPDATE {table} SET {keys_str} WHERE sn = '{sn}';"  
                #print(sql)
                #print(val)
                mycursor.execute(sql, val)
                mydb.commit()
                sql = f"SELECT sn from {table} WHERE sn = '{sn}'"
                #print(sql)
                mycursor.execute(sql)
                myresult = mycursor.fetchone()
                #print(myresult)
                #self.msg +=  f"Updated {table[:-1]} with serial {sn}\n"
                if (myresult):
                    return True
            except Exception as e:
                print(e)
                self.msg += f"ERROR: {e}\n"
            self.msg +=  f"FAIL inserting new {table} with serial {sn}\n"
            #self.msg +=  f"{dic}"
        return False
    
    # example dic for sensors: {'ae_zero' : '0.0', 'batch' : '1234', 'changes' : 'change1#change2#change3', 'date_reception' : '2022-03-28 06:30:59' , 'gas' : 'NO2', 'last_update' : '2022-03-28 06:30:59', 'observations' : '-', 'sensitivity_mv' : '0', 'sensitivity_na' : '0', 'sensitivity_no2_mv' : '0', 'sensitivity_no2_na' : '0', 'sn' : '212730222', 'state' : 'NOT_TESTED', 'we_zero' : '0', 'zero_cal_date' : '0'}
    # example dic for afes: {'ae_zero_sn1' : '0.0', 'ae_zero_sn2' : '0.0', 'ae_zero_sn3' : '0.0', 'ae_zero_sn4' : '0.0', 'batch' : '80212', 'changes' : 'change1#change2#change3', 'date_reception' : '2022-03-28 06:30:59', 'last_update' : '2022-03-28 06:30:59', 'location' : 'SBOX_4-1-1', 'observations' : '-', 'pcb_gain_sn1' : '0', 'pcb_gain_sn2' : '0', 'pcb_gain_sn3' : '0', 'pcb_gain_sn4' : '0', 'pn' : '810-0023-00', 'sn' : '25-001550', 'sn1' : '212730222', 'sn2' : '214170147', 'sn3' : '132731017', 'sn4' : '134730255', 'state' : '-', 'we_zero_sn1' : '0', 'we_zero_sn2' : '0', 'we_zero_sn3' : '0', 'we_zero_sn4' : '0', 'zero_cal_date' : '02/03/2023'}
    def add_change(self, txt, sn, date=None):
        mydb = self.connect()
        if (mydb):
            try:
                mycursor = mydb.cursor()
                table = 'afes' if '-' in sn else 'sensors'
                if not date:
                    now = str(datetime.datetime.now(datetime.timezone.utc)).split(' ')[0]
                else:
                    now = date
                txt_add = f"{now} {txt}#"
                sql = f"UPDATE {table} SET changes = CONCAT (changes,'{txt_add}') WHERE sn = '{sn}';"  
                mycursor.execute(sql)
                mydb.commit()
                sql = f"SELECT changes from {table} WHERE sn = '{sn}'"
                print(sql)
                mycursor.execute(sql)
                myresult = mycursor.fetchone()
                print(myresult)
                if (myresult[0]):
                    if (txt_add in myresult[0]):
                        #self.msg +=  f"Updated {table[:-1]} changes with serial {sn}\n"
                        #for t in myresult[0].split('#'):
                        #    print(t)
                        return True
                else:
                    if (self.insert_field(table, 'sn', sn, 'changes', txt + "#")):
                        return True
            except Exception as e:
                print(e)
                self.msg += f"ERROR: {e}\n"
            self.msg +=  f"FAIL inserting new {table} with serial {sn}\n"
            #self.msg +=  f"{dic}"
        return False
    
    # Exemple of input dics
    # is a list of dictionaries
    """
    [
    {
        "ae_zero_sn1": 288.0,
        "ae_zero_sn2": 287.0,
        "batch": "80212",
        "pcb_gain_sn1": 0.8,
        "pcb_gain_sn2": 0.8,
        "pn": "810-0021-03",
        "sensors": {
            "SN1": {
                "ae_zero": 17.0,
                "gas": "NO-A4",
                "sensitivity_mv": 0.382,
                "sensitivity_na": 0.478,
                "sensitivity_no2_mv": 0.0,
                "sensitivity_no2_na": "",
                "sn": "130730027",
                "we_zero": 16.0
            },
            "SN2": {
                "ae_zero": -8.0,
                "gas": "VOC-A4",
                "sensitivity_mv": 0.258,
                "sensitivity_na": 0.322,
                "sensitivity_no2_mv": 0.0,
                "sensitivity_no2_na": "",
                "sn": "217270139",
                "we_zero": 79.0
            }
        },
        "sn": "13-000201",
        "we_zero_sn1": 277.0,
        "we_zero_sn2": 281.0,
        "zero_cal_date": "03/03/2023"
    },
    ....
]
    """
    # example dic for sensors: {'ae_zero' : '0.0', 'batch' : '1234', 'changes' : 'change1#change2#change3', 'date_reception' : '2022-03-28 06:30:59' , 'gas' : 'NO2', 'last_update' : '2022-03-28 06:30:59', 'observations' : '-', 'sensitivity_mv' : '0', 'sensitivity_na' : '0', 'sensitivity_no2_mv' : '0', 'sensitivity_no2_na' : '0', 'sn' : '212730222', 'state' : 'NOT_TESTED', 'we_zero' : '0', 'zero_cal_date' : '0'}
    # example dic for afes: {'ae_zero_sn1' : '0.0', 'ae_zero_sn2' : '0.0', 'ae_zero_sn3' : '0.0', 'ae_zero_sn4' : '0.0', 'batch' : '80212', 'changes' : 'change1#change2#change3', 'date_reception' : '2022-03-28 06:30:59', 'last_update' : '2022-03-28 06:30:59', 'location' : 'SBOX_4-1-1', 'observations' : '-', 'pcb_gain_sn1' : '0', 'pcb_gain_sn2' : '0', 'pcb_gain_sn3' : '0', 'pcb_gain_sn4' : '0', 'pn' : '810-0023-00', 'sn' : '25-001550', 'sn1' : '212730222', 'sn2' : '214170147', 'sn3' : '132731017', 'sn4' : '134730255', 'state' : '-', 'we_zero_sn1' : '0', 'we_zero_sn2' : '0', 'we_zero_sn3' : '0', 'we_zero_sn4' : '0', 'zero_cal_date' : '02/03/2023'}
    def create_entry_from_file(self, dic):
        #print(dic)
        self.msg = ""
        now = str(datetime.datetime.now(datetime.timezone.utc)).split(' ')[0]
        #print(now)
        sensors = dic['sensors']
        #print(sensors)
        for sensor in sensors:
            #print(sensor)
            #print(sensors[sensor])
            if 'sn' in sensors[sensor]:
                sensor_add = sensors[sensor]
                #print(sensor_add)
                sensor_add['gas'] = sensor_add['gas'].split('-')[0].upper().replace('OX','O3')
                sensor_add['date_reception'] = now
                sensor_add['last_update'] = now
                sensor_add['state'] = 'NOT_TESTED'
                sensor_add['zero_cal_date'] = dic['zero_cal_date'].replace('/','-')
                sensor_add['changes'] = f"{now} Entry Sensor from excel file assembled with {dic['sn']}#"
                sensor_add['batch'] = dic['batch']
                #print("insert SENSOR")
                #print(sensor_add)
                if (not self.create_entry('sensors', sensor_add)):
                    print(self.msg)
                    return False
            
        dic['zero_cal_date'] = dic['zero_cal_date'].replace('/','-')
        sn1 = ''
        sn2 = ''
        sn3 = ''
        sn4 = ''
        if 'SN1' in dic['sensors'] and 'sn' in dic['sensors']['SN1']:
            sn1 = dic['sensors']['SN1']['sn']
            dic['sn1'] = sn1
        if 'SN2' in dic['sensors'] and 'sn' in dic['sensors']['SN2']:
            sn2 = dic['sensors']['SN2']['sn']
            dic['sn2'] = sn2
        if 'SN3' in dic['sensors'] and 'sn' in dic['sensors']['SN3']:
            sn3 = dic['sensors']['SN3']['sn']
            dic['sn3'] = sn3
        if 'SN4' in dic['sensors'] and 'sn' in dic['sensors']['SN4']:    
            sn4 = dic['sensors']['SN4']['sn']
            dic['sn4'] = sn4
        #print(f"{now} Entry AFE from excel file with sensors [{sn1},{sn2},{sn3},{sn4}]#")
        dic['changes'] = f"{now} Entry AFE from excel file with sensors [{sn1},{sn2},{sn3},{sn4}]#"
        dic['date_reception'] = now
        dic['last_update'] = now
        dic['state'] = 'NOT_TESTED'
        dic.pop('sensors')
        #print('insert AFE')
        #print(dic)
        if (not self.create_entry('afes', dic)):
            #print(self.msg)
            return False
        
        #print(self.msg)
        return True

    # dic example : {'add_afe_sn' : '26-000123, 'add_afe_pn' : '01', 'add_sensor_sn1' : '0130570012', 'add_sensor_sn1' : '0130570015', 'add_sensor_sn1' : '0130570014', 'add_sensor_sn1' : '0130780811'}
    #dic = {'gas' : 'NO2', 'sn' : '212120005', 'state' : 'NOT_TESTED'}
    def create_entry_manual(self, dic):
        self.msg = ""
        now = str(datetime.datetime.now(datetime.timezone.utc)).split(' ')[0]
        #print(now)
        afe = {}
        for i in range(4):
            sn = dic[f'add_sensor_sn{i+1}'].lstrip("0")
            afe[f'sn{i+1}'] = sn
            if (sn  and not self.get_row('sensors' ,sn)):
                sensor_add = {}
                sensor_add['sn'] = sn
                sensor_add['gas'] = self.get_sensor_type(sn)
                sensor_add['date_reception'] = now
                sensor_add['last_update'] = now
                sensor_add['state'] = 'NOT_TESTED'
                #print("insert SENSOR")
                #print(sensor_add)
                if (not self.create_entry('sensors', sensor_add)):
                    #print(self.msg)
                    return False
            else:
                d = {'sn' : sn, 'state' : 'NOT_TESTED'}
                self.update_entry('sensors', d)
        sn = dic['add_afe_sn'].lstrip("0")
        if (sn.startswith("2")):
            afe['pn'] = f"810-0023-{dic['add_afe_pn']}"
        elif (sn.startswith("1")):
            afe['pn'] = f"810-0021-{dic['add_afe_pn']}"
        if (not self.get_row('afes', sn)):
            afe['sn'] = sn
            afe['date_reception'] = now
            afe['last_update'] = now
            afe['state'] = 'NOT_TESTED'
            afe['location'] = dic['location']
            #print('insert AFE')
            #print(afe)
            if (not self.create_entry('afes', afe)):
                #print(self.msg)
                return False
        
        #print(self.msg)
        return True
    
    def get_column_names(self, table):
        mydb = self.connect()
        mycursor = mydb.cursor()
        # obtenim els noms de les columnes
        sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = %s"
        val = (table,)
        mycursor.execute(sql, val)
        result = mycursor.fetchall()
        #print(result)
        #print(list(col[0] for col in result))
        if result:
            return [col[0] for col in result]
        print(f"Fail getting column names from table : {table}")
        return []
    
    # 143, 144, 161 and 154 are Sensorix sensors
    # Others are from AlphaSense
    def get_sensor_type(self, sn_sensor):
        sn_sensor = sn_sensor.lstrip("0")
        if sn_sensor.startswith("212"):
            return "NO2"
        elif sn_sensor.startswith("214"):
            return "O3"
        elif sn_sensor.startswith("130"):
            return "NO"
        elif sn_sensor.startswith("132"):
            return "CO"
        elif sn_sensor.startswith("133") or sn_sensor.startswith("143") or sn_sensor.startswith("144"):
            return "H2S"
        elif sn_sensor.startswith("134"):
            return "SO2"
        elif sn_sensor.startswith("217"):
            return "VOC"
        elif sn_sensor.startswith("1001") or sn_sensor.startswith("161"):
            return "HCL"
        else:
            return "NH3"
    
    def get_sensor_fabrication(self, sn_sensor):
        code = ""
        if sn_sensor:
            sn_sensor = sn_sensor.lstrip("0")
            months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            years = [['14', '96', '58', '16', '64', '88', '86', '50', '47', '84', '36', '32'],  #2018
                    ['70', '48', '87', '34', '66', '81', '33', '90', '42', '02', '89', '44'],  #2019
                    ['46', '55', '01', '15', '77', '72', '51', '20', '63', '25', '53', '79'],  #2020
                    ['13', '61', '05', '75', '07', '68', '37', '85', '24', '74', '08', '67'],  #2021
                    ['41', '35', '94', '04', '65', '76', '54', '78', '82', '80', '17', '49'],  #2022
                    ['27', '73', '11', '19', '38', '60', '39', '12', '62', '57', '45', '92'],  #2023
                    ['23', '71', '30', '83', '18', '40', '91', '43', '06', '95', '03', '26'],  #2024
                    ['93', '09', '56', '69', '31', '29', '52', '22', '21', '10', '28', '59'],  #2025
                    ['14', '96', '58', '16', '64', '88', '86', '50', '47', '84', '36', '32'],  #2026
                    ['70', '48', '87', '34', '66', '81', '33', '90', '42', '02', '89', '44'],  #2027
                    ['46', '55', '01', '15', '77', '72', '51', '20', '63', '25', '53', '79'],  #2028
                    ['13', '61', '05', '75', '07', '68', '37', '85', '24', '74', '08', '67'],  #2029
                    ['41', '35', '94', '04', '65', '76', '54', '78', '82', '80', '17', '49'],  #2030
                    ['27', '73', '11', '19', '38', '60', '39', '12', '62', '57', '45', '92']]  #2031

            code = sn_sensor[-6:-4]
            for i,y in enumerate(years):
                if code in y:
                    return months[y.index(code)] + f" {18+int(i)}"
            
        return f"({code}) ?"
    
    def get_afe_from_sensor(self, sensor):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            columns = self.get_column_names('afes')
            sensor = sensor.lstrip('0')
            sql = f"SELECT {','.join(columns)} from afes WHERE sn1 LIKE '%{sensor}%' or sn2 LIKE '%{sensor}%' or sn3 LIKE '%{sensor}%' or sn4 LIKE '%{sensor}%'"
            mycursor.execute(sql)
            myresult = mycursor.fetchone()
            #print(myresult)
            if (myresult):
                result = dict(zip(columns, myresult))
                return result['sn']
        return None
        
    
    # Return a dicctionary with values of each column
    def get_row(self, table, sn):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            columns = self.get_column_names(table)
            sql = f"SELECT {','.join(columns)} from {table} WHERE sn = '{sn}'"
            mycursor.execute(sql)
            myresult = mycursor.fetchone()
            #print(myresult)
            if (myresult):
                result = dict(zip(columns, myresult))
                return result
        return None
    
    # Return a list of dicctionaries with values of each column. Filter some columns.
    def get_all_rows(self, table, del_columns = None):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            columns = self.get_column_names(table)
            if (del_columns):
                for col in del_columns:
                    if (col in columns):
                        columns.remove(col)
            sql = f"SELECT {','.join(columns)} from {table}"
            mycursor.execute(sql)
            myresult = mycursor.fetchall()
            #print(myresult)
            result_list = []
            for row in myresult:
                result_list.append(dict(zip(columns, row)))
            return result_list
        return None
    
    # Return a list of dicctionaries with values of each selected column.
    def get_all_rows_columns(self, table, columns, result='dictionay'):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            if (columns):
                sql = f"SELECT {','.join(columns)} from {table}"
            else:
                sql = f"SELECT * from {table}"
            mycursor.execute(sql)
            myresult = mycursor.fetchall()
            #print(myresult)
            if (result == 'dictionay'):
                result_list = []
                for row in myresult:
                    result_list.append(dict(zip(columns, row)))
                return result_list
            elif (result == 'list'):
                myresult.insert(0, columns)
                return myresult
        return None
    
    # Return a list of dicctionaries with values of each selected column.
    def get_row_columns(self, table, sn, columns, result='dictionay'):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            if (columns):
                sn_txt = 'sn' if table != 'test_bettair' else 'sn_cartridge'
                sql = f"SELECT {','.join(columns)} from {table} where {sn_txt} = '{sn}'"
            else:
                sql = f"SELECT * from {table}"
            mycursor.execute(sql)
            myresult = mycursor.fetchall()
            #print(myresult)
            if (result == 'dictionay'):
                result_list = []
                for row in myresult:
                    result_list.append(dict(zip(columns, row)))
                return result_list
            elif (result == 'list'):
                myresult.insert(0, columns)
                return myresult
        return None
      
    # ----------------- TEST BETTAIR -----------------------------
    # table: test_bettair
    # ['afes', 'assembler', 'board_id', 'external_id', 'fw', 'fw_modem', 'imei', 'interface_id', 'mac', 'pn_cartridge', 'pn_communication', 'pn_device', 'pn_external', 'pn_interface', 'pn_main', 'pn_sensor', 'requisition', 'sensors', 'sn_cartridge', 'sn_communication', 'sn_device', 'sn_external', 'sn_interface', 'sn_main', 'sn_sensor', 'test_date', 'test_time', 'test_traces', 'test_type']
        
    def get_bettair_test(self, cartridge):
        return self.get_bettair_test_generic(cartridge)
            
    def get_all_bettair_test(self):
        return self.get_bettair_test_generic(cartridge = 0)
    
    def get_bettair_test_generic(self, cartridge = 0):
        condition = "where sn_cartridge = %s" if cartridge != 0 else ""
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = f"""SELECT requisition, assembler, sn_device, pn_device, sn_cartridge, pn_cartridge, sensors,
                                        sn_main, pn_main, sn_communication, pn_communication, sn_sensor, pn_sensor, sn_external, 
                                        pn_external, sn_interface, pn_interface, sn_pm, sn_co2, mac, board_id, fw_modem, imei, interface_id, external_id,
                                        fw, test_date, test_time, afes, test_traces, test_final from test_bettair {condition}"""
            val = (cartridge,)
            if (cartridge != 0):
                mycursor.execute(sql, val)
            else:
                mycursor.execute(sql)
            myresult = mycursor.fetchall()
            if myresult:
                if len(myresult) == 1:
                    return myresult
                else:
                    my_new_result = []
                    for row in myresult:
                        r = list(row)
                        if (r[3]):
                            r.insert(7, self.get_node_extras(r[3]))
                        else:
                            r.insert(7, '')
                        my_new_result.append(r)
                    return my_new_result
            
        return []
    
    
    
    def get_node_extras(self, pn_e):
        hex = pn_e[5:7]
        bin = utils.hex2bin(hex)
        list_extras = []
        if (bin[0] == "1"):
            list_extras.append("Panel Solar")
        if (bin[1] == "1"):
            list_extras.append("Anemómetro mecánico")
        if (bin[2] == "1"):
            list_extras.append("Anemómetro ultrasónico DAVIS")
        if (bin[3] == "1"):
            list_extras.append("Alimentación externa")
        if (bin[4] == "1"):
            list_extras.append("Estación meteorológica")
        if (bin[5] == "1"):
            list_extras.append("PM+")
        if (bin[6] == "1"):
            list_extras.append("Anemómetro ultrasónico CALYPSO")
        if (bin[7] == "1"):
            list_extras.append("SOUND+")
        #print(f"{'Extras:':10}{', '.join(list_extras):>33}")
        return ', '.join(list_extras)
    
    def get_assembling_alerts(self, pn_e):
        list_extras = ["General"]
        list_extras.append("    - Solder capacitors Main Board")
        # Eval power
        if (pn_e[3] == 'E'):
            list_extras.append("POE power supply")
            list_extras.append("    - Solder the POE chip")
        elif (pn_e[3] == 'A'):
            list_extras.append("VAC power supply")
            list_extras.append("    - Solder TRACCO power supply")
            list_extras.append("    - Solder the inductance")
            list_extras.append("    - Solder the varistor")
        # Eval extras
        
        hex = pn_e[5:7]
        bin = utils.hex2bin(hex)
        if (bin[6] == "1"):
            list_extras.append("Anemómetro ultrasónico CALYPSO")
            list_extras.append("    - Change trasceiver to RS485")
            list_extras.append("    - Change CALYPSO configuration")
        if (bin[5] == "1" and bin[7] == "1"):
            #list_extras.append("BETTAIR+ (PM+, SOUND+)")    
            list_extras.append("RS485")
            list_extras.append("    - Change in Main R11 = 45R3")
            list_extras.append("    - Change in Main R12 = 1K")
            list_extras.append("    - Change trasceiver to RS485")
            list_extras.append("    - Double Battery")
            list_extras.append("    - Change battery connector")
        elif (bin[5] == "1"):
            list_extras.append("RS485")
            list_extras.append("    - Change in Main R11 = 45R3")
            list_extras.append("    - Change in Main R12 = 1K")
            list_extras.append("    - Change trasceiver to RS485")
            list_extras.append("    - Double Battery")
            list_extras.append("    - Change battery connector")
        elif (bin[7] == "1"):
            list_extras.append("RS485")
            list_extras.append("    - Change trasceiver to RS485")
                    
        #print(list_extras)
        return list_extras
    
    # ----------------- DEBUG -----------------------------
    # CSV columns for sensors:
    # AE_zero_from_afe,AE_zero_from_sensor,WE_zero_from_afe,WE_zero_from_sensor,
    # _id,batch,itemtype,last_update,reception,sensitivity,sensor_type,serial,status
    
    def get_sensors_from_csv(self):
        #result = []
        with open(r'C:\Users\Daniel Gaspar\Downloads\allitems_sensors.csv') as f:
            col_names = f.readline().replace('\n','').split(',')
            #print(col_names)
            ae_zero_index = col_names.index('AE_zero_from_sensor')
            we_zero_index = col_names.index('WE_zero_from_sensor')
            batch_index = col_names.index('batch')
            last_update_index = col_names.index('last_update')
            date_reception_index = col_names.index('reception')
            gas_index = col_names.index('sensor_type')
            sn_index = col_names.index('serial')
            observations_index = col_names.index('status\n')
            item_type_index = col_names.index('itemtype')
            for line in f:
                l = line.replace('\n','').split(',')
                if (len(l) > 12 and l[item_type_index] == 'sensor'):
                    dic = {}
                    dic['ae_zero'] = l[ae_zero_index]
                    dic['we_zero'] = l[we_zero_index]
                    dic['batch'] = l[batch_index]
                    dic['last_update'] = l[last_update_index]
                    dic['date_reception'] = l[date_reception_index]
                    dic['gas'] = l[gas_index].split('-')[0].upper().replace('X','3')
                    dic['sn'] = l[sn_index]
                    #print(l[sn_index])
                    dic['observations'] = l[observations_index] if l[observations_index] != '\n' else ''
                    #print(dic)
                    #result.append(dic)
                    self.create_entry('sensors', dic)
                    print(self.msg)
                    
        #return result
        
    def clean_zero_from_sn_sensors(self):
        table = 'sensors'
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            columns = self.get_column_names(table)
            sql = f"SELECT {','.join(columns)} from {table} WHERE sn like '0%'"
            mycursor.execute(sql)
            myresult = mycursor.fetchall()
            sn_index = columns.index('sn')
            for row in myresult:
                try:
                    sn = row[sn_index]
                    print(f"Deleting {sn}")
                    sql = f"delete from {table} WHERE sn = '{sn}'"
                    mycursor.execute(sql)
                    mydb.commit()
                    print(f"Deleted {mycursor.rowcount} {sn} ")
                    row_changed = list(row)
                    row_changed[sn_index] = sn.lstrip('0')
                    sql = f"INSERT INTO {table} ({', '.join(columns)}) VALUES ({('%s, '*len(row_changed))[:-2]});"
                    mycursor.execute(sql, row_changed)
                    mydb.commit()
                    print(f"Replaced {mycursor.rowcount} {sn} for {sn.lstrip('0')}")
                except Exception as e:
                    print(e)
                    print(f"Skip {sn}")
            
        return myresult
    
    def clean_zero_from_sn_afes(self):
        table = 'afes'
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            columns = self.get_column_names(table)
            sql = f"SELECT {','.join(columns)} from {table}"
            mycursor.execute(sql)
            myresult = mycursor.fetchall()
            sn_sensors_index = [columns.index('sn1'), columns.index('sn2'), columns.index('sn3'), columns.index('sn4')]
            sn_afe_index = columns.index('sn')
            for row in myresult:
                try:
                    sn_afe = row[sn_afe_index]
                    print(f"Deleting {sn_afe}")
                    sql = f"delete from {table} WHERE sn = '{sn_afe}'"
                    mycursor.execute(sql)
                    mydb.commit()
                    print(f"Deleted {mycursor.rowcount} {sn_afe} ")
                    row_changed = list(row)
                    for i in range(4):
                        if row_changed[sn_sensors_index[i]]:
                            row_changed[sn_sensors_index[i]] = row_changed[sn_sensors_index[i]].lstrip('0')
                    sql = f"INSERT INTO {table} ({', '.join(columns)}) VALUES ({('%s, '*len(row_changed))[:-2]});"
                    mycursor.execute(sql, row_changed)
                    mydb.commit()
                    print(f"Replaced {mycursor.rowcount} {row[sn_sensors_index[0]:sn_sensors_index[3]]} for {row_changed[sn_sensors_index[0]:sn_sensors_index[3]]}")
                except Exception as e:
                    print(e)
                    print(f"Skip {sn}")
            
        return myresult
    
    def clean_gas_assigned(self):
        table = 'sensors'
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            columns = self.get_column_names(table)
            sql = f"SELECT {','.join(columns)} from {table}"
            mycursor.execute(sql)
            myresult = mycursor.fetchall()
            sn_index = columns.index('sn')
            gas_index = columns.index('gas')
            for row in myresult:
                try:
                    sn = row[sn_index]
                    row_changed = list(row)
                    gas = row[gas_index]
                    real_gas = self.get_sensor_type(sn) 
                    if (gas != real_gas):
                        self.update_entry(table, {'sn': sn, 'gas' : real_gas})
                        print(f"Gas {gas} replaced for {real_gas} in sensor sn {sn}")
                    #else:
                    #    print(f"Gas {gas} NOT replaced for {real_gas} in sensor sn {sn}")
                except Exception as e:
                    print(e)
                    print(f"Skip {sn}")
            
        return myresult
    
        
    
    # CSV columns for afes:
    # AE_1_offset,AE_2_offset,AE_3_offset,AE_4_offset,WE_1_offset,WE_2_offset,WE_3_offset,WE_4_offset,
    # _id,afe_type,circuit_type,contains,contains.1,contains.2,contains.3,contains.4,itemtype,
    # lastStateUpdateDate,last_update,location,orderUnitID,pcb_gain_1,pcb_gain_2,pcb_gain_3,pcb_gain_4,
    # reception,serial,state,stateDescription,status,zero_cal_date
    
    def get_afes_from_csv(self):
        #result = []
        dic_id_sn = self.get_dic_id_to_sn()
        with open(r'C:\Users\Daniel Gaspar\Downloads\allitems_afes.csv') as f:
            col_names = f.readline().replace('\n','').split(',')
            #print(col_names)
            ae_zero_sn1_index = col_names.index('AE_1_offset')
            ae_zero_sn2_index = col_names.index('AE_2_offset')
            ae_zero_sn3_index = col_names.index('AE_3_offset')
            ae_zero_sn4_index = col_names.index('AE_4_offset')
            we_zero_sn1_index = col_names.index('WE_1_offset')
            we_zero_sn2_index = col_names.index('WE_2_offset')
            we_zero_sn3_index = col_names.index('WE_3_offset')
            we_zero_sn4_index = col_names.index('WE_4_offset')
            afe_type_index = col_names.index('afe_type')
            circuit_type_index = col_names.index('circuit_type')
            sn1_index = col_names.index('contains.1')
            sn2_index = col_names.index('contains.2')
            sn3_index = col_names.index('contains.3')
            sn4_index = col_names.index('contains.4')
            last_update_index = col_names.index('lastStateUpdateDate')
            location_index = col_names.index('location')
            pcb_gain_1_index = col_names.index('pcb_gain_1')
            pcb_gain_2_index = col_names.index('pcb_gain_2')
            pcb_gain_3_index = col_names.index('pcb_gain_3')
            pcb_gain_4_index = col_names.index('pcb_gain_4')
            reception_index = col_names.index('reception')
            sn_index = col_names.index('serial')
            state_index = col_names.index('state')
            observations_index = col_names.index('stateDescription')
            zero_cal_date_index = col_names.index('zero_cal_date')
            itemtype_index = col_names.index('itemtype')
            for line in f:
                try:
                    l = line.replace('\n','').split(',')
                    if (len(l) > 20 and l[itemtype_index] == 'afe'):
                        dic = {}
                        dic['sn'] = l[sn_index]
                        if dic['sn']:
                            dic['ae_zero_sn1'] = l[ae_zero_sn1_index]
                            dic['ae_zero_sn2'] = l[ae_zero_sn2_index]
                            if l[ae_zero_sn2_index]:
                                dic['ae_zero_sn3'] = l[ae_zero_sn3_index]
                            if l[ae_zero_sn4_index]:
                                dic['ae_zero_sn4'] = l[ae_zero_sn4_index]
                            dic['we_zero_sn1'] = l[we_zero_sn1_index]
                            dic['we_zero_sn2'] = l[we_zero_sn2_index]
                            if l[we_zero_sn3_index]:
                                dic['we_zero_sn3'] = l[we_zero_sn3_index]
                            if l[we_zero_sn4_index]:
                                dic['we_zero_sn4'] = l[we_zero_sn4_index]
                            
                            dic['pn'] = l[afe_type_index] + '-' + l[circuit_type_index]
                            dic['sn1'] = dic_id_sn[l[sn1_index]]
                            dic['sn2'] = dic_id_sn[l[sn2_index]]
                            dic['sn3'] = dic_id_sn[l[sn3_index]]
                            dic['sn4'] = dic_id_sn[l[sn4_index]]
                            dic['last_update'] = l[last_update_index]
                            dic['location'] = l[location_index]
                            dic['pcb_gain_sn1'] = l[pcb_gain_1_index]
                            dic['pcb_gain_sn2'] = l[pcb_gain_2_index]
                            dic['pcb_gain_sn3'] = l[pcb_gain_3_index]
                            dic['pcb_gain_sn4'] = l[pcb_gain_4_index]
                            dic['date_reception'] = l[reception_index]
                            
                            dic['state'] = l[state_index]
                            dic['observations'] = l[observations_index]
                            if len(l) > zero_cal_date_index:
                                dic['zero_cal_date'] = l[zero_cal_date_index]
                            #print(dic)
                            self.create_entry('afes', dic)
                            print(self.msg)
                            
                except Exception as e:
                    print(f"ERROR: {e}")
                    print(f"{l}")
                    print(f"{dic}")
                    print(l[sn3_index])
                    print(type(l[sn3_index]))
                    print(dic_id_sn[l[sn2_index]])
                    print(dic_id_sn['null'])
                #print(dic)
                #result.append(dic)
                
    # CSV columns for sensors:
    # AE_zero_from_afe,AE_zero_from_sensor,WE_zero_from_afe,WE_zero_from_sensor,
    # _id,batch,itemtype,last_update,reception,sensitivity,sensor_type,serial,status    
    
    def get_dic_id_to_sn(self):
        dic = {' ' : None, 'null' : None, '' : None}
        with open(r'C:\Users\Daniel Gaspar\Downloads\allitems_sensors.csv') as f:
            col_names = f.readline().split(',')
            #print(col_names)
            id_index = col_names.index('_id')
            serial_index = col_names.index('serial')
            item_type_index = col_names.index('itemtype')
            for line in f:
                l = line.split(',')
                if (len(l) > 12 and l[item_type_index] == 'sensor'):
                    dic[l[id_index]] = l[serial_index]
        return dic
    # ----------------- AFES -----------------------------
    # table: afes
    # ['ae_zero_sn1', 'ae_zero_sn2', 'ae_zero_sn3', 'ae_zero_sn4', 'afe_type', 'batch', 'changes', 'date_reception', 'last_update', 'location', 'observations', 'pcb_gain_sn1', 'pcb_gain_sn2', 'pcb_gain_sn3', 'pcb_gain_sn4', 'pn', 'sn', 'sn1', 'sn2', 'sn3', 'sn4', 'state', 'we_zero_sn1', 'we_zero_sn2', 'we_zero_sn3', 'we_zero_sn4', 'zero_cal_date']
    
    def get_all_afes(self, include_outdated = False, include_assembled = False):
        #del_columns = [f'ae_zero_sn{i+1}' for i in range(4)]
        #del_columns += [f'we_zero_sn{i+1}' for i in range(4)]
        #del_columns += [f'pcb_gain_sn{i+1}' for i in range(4)]
        #del_columns += ['batch', 'changes']
        #return self.get_all_rows('afes', del_columns)
        columns = ['order_id', 'sn', 'pn']
        columns += [f'sn{i+1}' for i in range(4)]
        columns += ['location', 'state', 'date_reception', 'last_update', 'observations', 'changes']
        afes = self.get_all_rows_columns('afes', columns)
        response = []
        for afe in afes:
            for i in range(4):
                serial = afe[f'sn{i+1}']
                if (serial):
                    afe[f'sn{i+1}'] = self.get_sensor_type(serial) + f" ({self.get_sensor_fabrication(serial)}) - {serial}"
                afe['date_reception'] = afe['date_reception'].split('T')[0] if 'T' in afe['date_reception'] else afe['date_reception']
                afe['last_update'] = afe['last_update'].split('T')[0] if 'T' in afe['last_update'] else afe['last_update']
                    # Afegim les afes que tenen ubicació
                date_reception = afe['last_update'] if afe['state'] == 'NOT_TESTED' and afe['last_update'] else afe['date_reception']
                date_reception = pd.to_datetime(date_reception, utc=True)
                afe['conditioning'] =  str(datetime.datetime.now(datetime.timezone.utc) - date_reception).split(' ')[0]
            if (afe['location']):
                if (include_outdated):
                    if ('BOX' in afe['location'].upper() or afe['location'].upper() == "OUTDATED"):
                        response.append(afe)
                        #print(f"({i}) {afe}")
                        i += 1
                if (include_assembled):
                    if (not 'BOX' in afe['location'].upper() and not afe['location'].upper() == "OUTDATED"):
                        response.append(afe)
                        #print(f"({i}) {afe}")
                        i += 1
                elif (not include_outdated and not include_assembled): 
                    if ('BOX' in afe['location'].upper()):
                        response.append(afe)
                        #print(f"({i}) {afe}")
                        i += 1
        return response
    
    # Return a list of all afes in a SBOX
    #@mide_tiempo
    def get_afes_sbox(self, sbox):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = f"SELECT sn from afes WHERE location like '%{sbox}%' and (state = 'NOT_TESTED' or state = 'TESTING')"
            mycursor.execute(sql)
            myresult = mycursor.fetchall()
            if (myresult):
                return [i[0] for i in myresult]
        return []
    
    def get_all_sensors(self, column_sort = '', reverse_in = False):
        mydb = self.connect()
        if (mydb):
            cols = self.get_column_names('sensors')
            cols.remove("zero_cal_date")
            cols.remove("ae_zero")
            cols.remove("we_zero")
            cols.remove("sensitivity_na")
            cols.remove("sensitivity_mv")
            cols.remove("sensitivity_no2_na")
            cols.remove("sensitivity_no2_mv")
            cols_txt = ",".join(cols)
            #print(cols_txt)
            cols.insert(2,"date")
            mycursor = mydb.cursor()
            sql = f"SELECT {cols_txt} from sensors where state = 'SBOX_1_2'"
            mycursor.execute(sql)
            sensors = list(mycursor.fetchall())
            sensors_out = []
            sensors_out.insert(0, cols)
            for i in range(len(sensors)):
                sensor_out = list(sensors[i])
                #print(sensor_out)
                # Restem 1 al index de 'state' perque hem afegit el camp 'date'
                if (sensor_out[cols.index("state")-1] == "SBOX_1_2"):
                    #print(sensor_out[cols.index("state")])
                    #print(sensors[i])
                    date = self.get_sensor_fabrication(sensors[i][0])
                    sensor_out.insert(2, date)
                    #print(sensor_out)
                    #if ("23" in date or "24" in date):
                    sensors_out.append(sensor_out)
                    #print(f"*{sensor_out[cols.index('state')]}*")
                    #if ("NOT_TESTED" in sensor_out[cols.index("state")]):
                    #    print(f"obsoletar {sensors[i][0]}")
                    #    self.insert_field('sensors', 'sn', sensors[i][0], 'state', 'OBSOLETE')
                    #else:
                    #    print(f"NO obsoletar {sensors[i][0]}")
        return sensors_out
    
    def get_sensor_info(self, sn):
        if (sn):
            dic = {'sn' : sn}
            row = self.get_row('sensors', sn)
            #print(row)
            if row:
                dic['gas'] = self.get_sensor_type(sn)
                dic['afe'] = self.get_afe_from_sensor(sn)
                dic['date'] = self.get_sensor_fabrication(sn)
                dic['state'] = row['state']
                dic['changes'] = row['changes'] if 'changes' in row.keys() else "None"
                return dic
        
        return None
    
    def get_sensor_location(self, mydb, sn):
        #mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = f"SELECT state from sensors WHERE sn = '{sn}'"
            mycursor.execute(sql)
            myresult = mycursor.fetchone()
            if (myresult):
                return myresult[0]
        
        return None
    
    def set_sensor_location(self, sn, location):
        dic = {'sn' : sn.lstrip('0'), 'state' : location}
        if (self.update_entry('sensors', dic)):
            self.msg = f"Set sensor loation to {location}"
            return True
        self.msg = f"FAIL seting sensor loation to {location}"
        return False
        
    def create_sensor(self, sn):
        type_entry = self.get_sensor_type(sn)
        if (type_entry != '?'):
            dic_sens = {'gas' : type_entry, 'sn' : sn.lstrip('0'), 'state' : 'NOT_TESTED'}
            if (self.create_entry('sensors', dic_sens)):
                self.msg = f"Created new sensor {type_entry} with serial {sn.lstrip('0')} in database\n"
                return True
        self.msg = f"FAIL Creating new sensor {type_entry} with serial {sn.lstrip('0')} in database"
        return False
    
    def get_sensors_stock(self):
         # Get all afes
        sensors = self.get_all_sensors()
        # Convert into Dataframe to manage easylly
        data = pd.DataFrame(sensors[1:], columns = sensors[0])
        #print(data.head())
        #print(data.dtypes)
        count = data['gas'].value_counts()
        #print(count)
        #print(dict(count))
        return dict(count)
    
    def get_afe_info(self, sn):
        columns = ['sn', 'pn']
        columns += [f'sn{i+1}' for i in range(4)]
        columns += ['location', 'state', 'date_reception', 'last_update', 'observations', 'changes']
        afes = self.get_row_columns('afes', sn, columns)
        #print(afes)
        for afe in afes:
            for i in range(4):
                serial = afe[f'sn{i+1}']
                if (serial):
                    afe[f'sn{i+1}'] = self.get_sensor_type(serial) + f" ({self.get_sensor_fabrication(serial)}) - {serial}"
                afe['date_reception'] = afe['date_reception'].split('T')[0] if 'T' in afe['date_reception'] else afe['date_reception']
                afe['last_update'] = afe['last_update'].split('T')[0] if 'T' in afe['last_update'] else afe['last_update']
                    # Afegim les afes que tenen ubicació
                date_reception = afe['last_update'] if afe['state'] == 'NOT_TESTED' and afe['last_update'] else afe['date_reception']
                date_reception = pd.to_datetime(date_reception, utc=True)
                afe['conditioning'] =  str(datetime.datetime.now(datetime.timezone.utc) - date_reception).split(' ')[0]
                return afe
        return []    
    def sort_table(self, table, col=0, reverse_in = False):
        return sorted(table, key=operator.itemgetter(col), reverse = reverse_in)    
    
    def get_afe_stocks(self, grouped = False):
        # Get all afes
        afes = self.get_all_afes(include_outdated = True, include_assembled = True)
        # Convert into Dataframe to manage easylly
        values = [list(afe.values()) for afe in afes]
        columns = afes[0].keys()
        data = pd.DataFrame(values, columns = columns)
        #print(data.head())
        #print(data.dtypes)
        
        # Eval obsolete Afes
        limit_date = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days = DAYS_OUTDATE)
        #print(data['last_update'])
        #print(data['date_reception'])
        #print(str(datetime.datetime.now()).split(' ')[0])
        #print(np.datetime64(data['date_reception'].to_string()))
        data['date_reception'] = np.where(data['state'] == 'NOT_TESTED', data['last_update'], data['date_reception'])
        data['date_reception_str'] = data.date_reception.str.split('T').str[0]
        data['date_reception'] = pd.to_datetime(data['date_reception'], utc=True)
        #print(data['date_reception'])
        #data['conditioning'] =  datetime.datetime.now(datetime.timezone.utc) - data['date_reception']
        #print(data.dtypes)
        data_outdate = data.loc[(data['date_reception'] < limit_date) | (data['location'] == "OUTDATED")].copy()
        data_outdate['date_reception'] = data['date_reception_str']
        data_outdate.drop(['date_reception_str'], inplace=True, axis=1)
        data_outdate_list = data_outdate.values.tolist()
        data_outdate_list.insert(0, list(data_outdate.columns))
        
        data['date_reception'] = data['date_reception_str']
                
        # get assembled afes
        data_assembled = data[~data['location'].str.startswith("SBOX") & ~data['location'].str.startswith("TESTING")].copy()
        data_assembled = data_assembled[data_assembled['location'] != "OUTDATED"].copy()
        data_assembled_list = data_assembled.values.tolist()
        data_assembled_list.insert(0, list(data_assembled.columns))
        #print(data_assembled_list)

        # Get valid AFES
        data['order_id'] = np.where(data['order_id'], data['order_id'], '')
        data = data[(data['location'].str.startswith("SBOX") | data['location'].str.startswith("TESTING")) & ~data['order_id'].str.startswith('B')].copy()
        #print(data[~data['order'].str.startswith('B')])
        #print(data[data['order'].str.startswith('B')])
        # Drop unnecessary fields from header
        data.drop(['order_id', 'location', 'pn', 'date_reception', 'last_update', 'observations','changes', 'date_reception_str'], inplace=True, axis=1)
        for i in range(4):
            sensor = data[f'sn{i+1}']
            #afe4 = data['sn'].str
            #print(afe4)
            #print([s.split(' ')[0] if s else ' ' for s in sensor])
            data[f'sn{i+1}'] = [s.split(' ')[0] if s else '' for s in sensor]
        data['sn'] = ['AFE4' if s.startswith('2') else 'AFE2' for s in data['sn'] ]
        #print(data['sn'])
        #data.drop(['sn'], inplace=True, axis=1)
        # Unify sensors as OK or NOT_OK
        data.replace(to_replace = 'NOT_TESTED', value = 'OK', inplace = True)
        data.replace(to_replace = 'TESTING', value = 'OK', inplace = True)
        # Agrupem els sensors en una nova columna
        data["sensors"] = data["sn1"] + "," + data["sn2"] + "," + data["sn3"] + "," + data["sn4"]
        data["sensors"] = [d.strip(',') for d in data["sensors"]]
        if grouped:
            data['sensors'] = np.where(data['sensors'] == 'SO2,H2S', 'H2S,SO2', data['sensors'])
            data['sensors'] = np.where(data['sensors'] == 'CO,VOC', 'VOC,CO', data['sensors'])
            data['sensors'] = np.where(data['sensors'] == 'O3,NO2', 'NO2,O3', data['sensors'])
        data.drop(['sn1', 'sn2', 'sn3', 'sn4'], inplace=True, axis=1)
        # Creem dos dataframes, un per les afes bones i un altre per les dolentes
        data_ok = data.loc[data['state'] == 'OK'].copy()
        data_ok.drop(['state'], inplace=True, axis=1)
        #data_ok.drop(['conditioning'], inplace=True, axis=1)
        #print(data_ok.dtypes)
        # Agrupem tots els sensors que han estat mes de 21 dies acondicionant
        data_ok.conditioning = data_ok.conditioning.astype('int16')
        if not grouped:
            data_ok.loc[data_ok['conditioning'] > 21, 'conditioning'] = -1
        else:
            data_ok.loc[data_ok['conditioning'] >= 0, 'conditioning'] = -1
        #print(data_ok)
        # Sumem els grups de sensors
        data_ok = data_ok.value_counts()
        #print(data_ok)
        #print(data_ok.index)
        data_ok_index = [sensors for sensors in data_ok.index]
        #print(data_ok_index)
        data_ok = list(zip(data_ok_index, data_ok.values.tolist()))
        #print(data_ok)
        #data_conditioning = [[field[0][1], field[1]] for field in data_ok if field[0][0] != -1]
        if not grouped:
            data_conditioning = [[field[0][0], field[0][1], field[0][2], field[1]] for field in data_ok if field[0][1] != -1]
            #print(data_conditioning)
            data_ok = [[field[0][0], field[0][2], field[1]] for field in data_ok if field[0][1] == -1]
        else:
            data_ok = [[field[0][0], field[0][2], field[1]] for field in data_ok]
        #print(data_ok)        
        data_ko = data.loc[data['state'] == 'NOT_OK'].copy()
        data_ko.drop(['state', 'conditioning'], inplace=True, axis=1)
        #print(data_ko)
        data_ko = data_ko.value_counts()
        data_ko_index = [sensors for sensors in data_ko.index]
        data_ko = list(zip(data_ko_index, data_ko.values.tolist()))
        data_ko = [[field[0][0], field[0][1], field[1]] for field in data_ko]
        #data_ok.insert(0, ['sensors', 'units'])
        #data_ko.insert(0, ['sensors', 'units'])
        #data_conditioning.insert(0, ['sensors', 'conditioning', 'units'])
        if not grouped:
            result = [data_ok, data_conditioning, data_ko, data_outdate_list, data_assembled_list]
        else:
            result = [data_ok, data_ko]
        #result = [data_ok, data_conditioning, data_ko]
        #print(result[0:3])
        return result 
    
    #Example input dic: {'repair2_filter': 'State', 'repair_afe': '26-999999', 'repair_state': 'NOT_OK', 'repair_status': 'hola'}
    def set_afe_state(self, dic):
        self.msg = ""
        if not 'repair_afe' in dic or not 'repair_status' in dic or not 'repair_state' in dic:
            self.msg = f"Bad diccionary passed: {dic}"
            print("FAIL")
            return False
        d = dict()
        sn = d['sn'] = dic['repair_afe']
        d['state'] = dic['repair_state']
        d['observations'] = dic['repair_status']
        print(d)
        if (self.update_entry('afes', d)):
            print(f"Set afe {sn} state {d['state']}. {d['observations']}")
            if (self.add_change(f"Set afe {sn} state {d['state']}. {d['observations']}", sn)):
                print('2')
                self.msg += f"Set afe {sn} state {d['state']}"
                return True
            
        self.msg += f"Fail updating state {d['state']}"
        return False
    
    def get_afe_state(self, sn):
        row = self.get_row('afes', sn)
        if row and 'state' in row:
            return row['state']
        return ''
    
    #Example input dic: {'repair2_filter': 'Location', 'repair_afe': '26-999999', 'repair_location': 'SBOX_4-3-2'}
    def set_afe_location(self, dic):
        self.msg = ""
        if not 'repair_afe' in dic or not 'repair_location' in dic:
            self.msg = f"Bad diccionary passed: {dic}"
            #print("FAIL")
            return False
        d = dict()
        sn = d['sn'] = dic['repair_afe']
        d['location'] = dic['repair_location']
        if (self.update_entry('afes', d)):
            self.msg += f"Update afe {sn} location to {d['location']}"       
            return True
        self.msg += f"Fail updating location to afe {sn}"
        return False
    
    def get_afe_location(self, sn):
        row = self.get_row('afes', sn)
        if row and 'location' in row:
            return row['location']
        return ''
    
    #Example input dic: {'repair2_filter': 'Sensors', 'repair_afe': '26-999999', 'repair_sens_serial_1': '999999999', 
    #                    'repair_sens_type_1': '', 'repair_sens_serial_2': '999999998', 'repair_sens_type_2': '', 
    #                     'repair_sens_serial_3': '999999997', 'repair_sens_type_3': '', 'repair_sens_serial_4': '999999996', 'repair_sens_type_4': ''}
    def set_afe_sensors(self, dic):
        self.msg = ""
        if not 'repair_afe' in dic:
            self.msg = f"Bad diccionary passed: {dic}"
            print("FAIL")
            return False
        d = dict()
        sn = d['sn'] = dic['repair_afe']
        for i in range(4):
            if (f'repair_sens_serial_{i+1}' in dic and f'repair_sens_type_{i+1}' in dic):
                # update sensors
                if (f'repair_sens_type_{i+1}' != '--' and dic[f'repair_sens_serial_{i+1}']):
                    type_evaluate = self.get_sensor_type(dic[f'repair_sens_serial_{i+1}'])
                    if (type_evaluate == '?'):
                        self.msg =  f"Unknown sensor type for serial {dic[f'repair_sens_serial_{i+1}']} for serial {dic[f'repair_sens_serial_{i+1}']}"
                        return False
                    type_entry = dic[f'repair_sens_type_{i+1}']
                    if (type_evaluate == type_entry):
                        d[f'sn{i+1}'] = dic[f'repair_sens_serial_{i+1}']
                        if (not self.get_sensor_info(d[f'sn{i+1}'])):
                            dic_sens = {'gas' : type_entry, 'sn' : d[f'sn{i+1}'].lstrip('0'), 'state' : 'NOT_TESTED'}
                            if (self.create_entry('sensors', dic_sens)):
                                self.msg += f"Created new sensor {type_entry} with serial {d[f'sn{i+1}']} in database"
                        else:
                            self.set_sensor_location(dic[f'repair_sens_serial_{i+1}'], '')
                    else:
                        self.msg = f"Sensor serial {dic[f'repair_sens_serial_{i+1}']} is type {type_evaluate} not type {type_entry}" 
                        return False
                elif (dic[f'repair_sens_type_{i+1}'] == '--'):
                    d[f'sn{i+1}'] = ''
                    
        #print(d)            
        if (self.update_entry('afes', d)):
            sensors_list = [f"{key}:{d[key]}" for key in d]
            sensors_list = str(sensors_list).replace("'","")
            if (self.add_change(f"Changed sensors {sensors_list}", sn)):
                self.msg += f"\nChanged sensors {sensors_list}"
                return True
            
    
    # Retorna un diccionary amb els sensors
    # {'sn': '26-000354', 'sn1': '0212801413', 'sn2': '0214820757', 'sn3': '0130190106', 'sn4': '0132380434'}
    def get_afe_sensors(self, sn): 
        #print(sn)
        row = self.get_row('afes', sn)
        d = dict()
        if row:
            for key in row:
                if key.startswith('sn'):
                    d[key] = row[key]
        return d
    
    # Retorna una llista amb el tipus de sensor:
    # ['NO2', 'O3', 'NO', 'CO']
    # ['H2S', 'SO2', '', '']
    def get_afe_sensors_type(self, sn):
        s1 = self.get_afe_sensors(sn)
        #print(s1)
        if sn.startswith('2'):
            l = ['','','','']
            for i in range(4):
                if s1[f"sn{i+1}"]:
                    l[i] = self.get_sensor_type(s1[f"sn{i+1}"])
        elif sn.startswith('1'):
            l = ['','']
            for i in range(2):
                if s1[f"sn{i+1}"]:
                    l[i] = self.get_sensor_type(s1[f"sn{i+1}"])
        else:
            l = ['','','','']
        #print(l)
        return l
    
    # -------------------------------------------------------
    # ----------------- ANOMALY -----------------------------
    # -------------------------------------------------------
    
    # afes_list_txt es un string amb totes les afes separades per un espai
    # "00-000101 00-000102 00-000103 ..."
    def add_anomaly(self, date_start, afes_list, dataframe, dataframe_amb):
        #print(date_start)
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = "INSERT INTO anomaly (date_start, afes_list, df, df_amb) VALUES (%s, %s, %s, %s)"
            afes_sensors = [self.get_afe_sensors(afe) for afe in afes_list]
            afes_sensors_json = json.dumps(afes_sensors)
            val = (date_start, afes_sensors_json, dataframe, dataframe_amb)
            mycursor.execute(sql, val)
            mydb.commit()
            #print(afes_list)
            for afe in afes_list:
                #print(afe)
                #print(f"({date_start}) Anomaly Test Done")
                sensors_dic = self.get_afe_sensors(afe)
                for s in sensors_dic.keys():
                    if s != 'sn':
                        if sensors_dic[s]:
                            self.add_change(f"Anomaly Test Done with afe {afe}", sensors_dic[s], date_start) 
                sensors = str(sensors_dic).replace("'","")
                self.add_change(f"Anomaly Test Done with sensors {sensors}", afe, date_start)
            return True
        return False
    
    # return a dictionary
    def get_anomaly(self, afe):
        result_list = []
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            # Obtenim la entrada
            sql = "SELECT * FROM anomaly WHERE afes_list LIKE %s;"
            val = (f"%{afe}%",)
            mycursor.execute(sql, val)
            myresult = mycursor.fetchall()
            if myresult:
                #print(myresult)
                cols = self.get_column_names("anomaly")
                #print(cols)
                for r in myresult:
                    result_dict = {}
                    for i in range(len(cols)):
                        result_dict[cols[i]] = r[i]
                    result_list.append(result_dict)
                        
        return result_list
    
    # Return a list of afes tested in database
    def get_afes_tested_list(self):
        result = []
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            # Obtenim la entrada
            sql = "SELECT date_start, afes_list FROM anomaly"
            mycursor.execute(sql)
            myresult = mycursor.fetchall()
            if myresult:
                #print(myresult)
                for row in myresult:
                    date = row[0]
                    afes = json.loads(row[1])
                    # split afes list
                    afes_list = [afe['sn'] for afe in afes if afe]
                    #print(afes_list)
                    for afe in afes_list:
                        result.append(f"{afe} ..... {date}")
                        
                        # asign date for each afe
                    #    result_list[j] += f" {i[0]}"
                        #print(result_list[j])
                    #print(result_list)
                    #result += result_list
        #print(result)
        return sorted(result)
    
    # Start anomaly and set each sensor to a "ABOX_x_x" position and state to "TESTING"
    # afes: {'0076AF57': ['25-000706', ''], '0077034B': ['', ''], '00770733': ['26-000491', ''], '0076AE51': ['', ''], '00772214': ['', ''], '00770871': ['26-000313', ''], '00769732': ['', ''], '00768E4F': ['', ''], '00770BB9': ['', ''], '0076CD84': ['', '']}
    def start_anomaly(self, afes):
        print(afes)
        msg = ""
        for key in afes:
            if afes[key][0] or afes[key][1]:
                pos = flux.CARTRIDGE.index(key)+1
                for afe in afes[key]:
                    if afe:
                        abox = '2' if (pos > 5 ) else '1'
                        print(f"set {afe} to ABOX_{abox}-{afe[0]}-{pos if pos <= 5 else pos-5}")
                        d = {'repair_afe': afe, 'repair_location': f'ABOX_{abox}-{afe[0]}-{pos if pos <= 5 else pos-5}'}
                        if (self.set_afe_location(d)):
                            msg += self.msg + "\n"
                            d = {'repair_afe': afe, 'repair_state': 'TESTING', 'repair_status': ''}
                            if not self.set_afe_state(d):
                                return False
                            msg += self.msg + "\n"
        self.msg = msg
        return True
        
    # return dataframe of a saved afe test
    def get_dataframes(self, date, afe):
        date_time_obj = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
        #print(date_time_obj)
        #print(type(date_time_obj))
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            # Obtenim la entrada
            sql = "SELECT df, df_amb FROM anomaly WHERE afes_list LIKE %s and date_start = %s;"
            val = (f"%{afe}%", date_time_obj)
            mycursor.execute(sql, val)
            myresult = mycursor.fetchall()
                                   
            return myresult[0]
        return None
    
    def import_local_anomaly_tests(self):
        mypath = './results/'
        folders = [f for f in listdir(mypath)]
        print(folders)
        with open(f"./results/{folders}/") as f:
            for line in f:
                l = line.replace('\n','').split(',')
                print(l)
        
    # -------------------------------------------------------
    # ----------------- CO2 -----------------------------
    # ------------------------------------------------------- 
    
    def insert_anomaly_to_db(self, date_start, co2_list, df, df_temp, df_hum):
        #print(date_start)
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            co2_list_txt = " ".join(co2_list)
            df_pickle = cPickle.dumps(df)
            df_temp_pickle = cPickle.dumps(df_temp)
            df_hum_pickle = cPickle.dumps(df_hum)
            sql = "INSERT INTO co2_test (date_start, co2_list, df, df_temp, df_hum) VALUES (%s, %s, %s, %s, %s)"
            val = (date_start, co2_list_txt, df_pickle, df_temp_pickle, df_hum_pickle)
            mycursor.execute(sql, val)
            mydb.commit()
            return True
        return False
    
    # return a dictionary
    def get_anomaly_from_db(self, co2):
        result_list = []
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            # Obtenim la entrada
            sql = "SELECT * FROM co2_test WHERE co2_list LIKE %s;"
            val = (f"%{co2}%",)
            mycursor.execute(sql, val)
            myresult = mycursor.fetchall()
            if myresult:
                #print(myresult)
                cols = self.get_column_names("co2_test")
                #print(cols)
                for r in myresult:
                    result_dict = {}
                    for i in range(len(cols)):
                        result_dict[cols[i]] = r[i]
                    result_list.append(result_dict)
                        
        return result_list
    
    # Return a list of co2_sensors tested in database
    def get_co2_tested_list(self):
        result = []
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            # Obtenim la entrada
            sql = "SELECT date_start, co2_list FROM co2_test"
            mycursor.execute(sql)
            myresult = mycursor.fetchall()
            if myresult:
                #print(myresult)
                for row in myresult:
                    date = row[0]
                    co2s = row[1].split(" ")
                    # split co2s list
                    co2_list = [co2 for co2 in co2s if co2]
                    #print(co2_list)
                    for co2 in co2_list:
                        result.append(f"{co2} ..... {date}")
                
        return sorted(result)
    
    # return dataframe of a saved afe test
    def get_co2_dataframes(self, df, date, co2):
        date_time_obj = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
        #print(date_time_obj)
        #print(type(date_time_obj))
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            # Obtenim la entrada
            if (df == 'df'):
                sql = "SELECT df FROM co2_test WHERE co2_list LIKE %s and date_start = %s;"
            elif (df == 'df_temp'):
                sql = "SELECT df_temp FROM co2_test WHERE co2_list LIKE %s and date_start = %s;"
            elif (df == 'df_hum'):
                sql = "SELECT df_hum FROM co2_test WHERE co2_list LIKE %s and date_start = %s;"
            val = (f"%{co2}%", date_time_obj)
            mycursor.execute(sql, val)
            myresult = mycursor.fetchall()
            #print(myresult)
            if (myresult):
                df = cPickle.loads(myresult[0][0])
                #df['time'] = pd.to_datetime(df['time'])
                for col in df.columns[1:]:
                    df[[col]] = df[[col]].apply(pd.to_numeric)
                return df
        return None
    
    # -------------------------------------------------------
    # ----------------- STOCKS -----------------------------
    # ------------------------------------------------------- 
    
    def insert_stock(self, pn, description, stock, picture, package_list_txt):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            #package_list_txt = " ".join(package_list)
            sql = "INSERT INTO stocks (pn, description, stock, picture, package) VALUES (%s, %s, %s, %s, %s)"
            val = (pn, description, stock, picture, package_list_txt[:-1])
            mycursor.execute(sql, val)
            mydb.commit()
            return True
        return False

    def update_stock(self, dic):
        #print(dic)
        mydb = self.connect()
        if (mydb):
            try:
                mycursor = mydb.cursor()
                pn = dic['pn']
                dic.pop('pn')
                keys = (key for key in dic)
                val = [dic[key] for key in dic]
                keys_str = ""
                for key in keys:
                    keys_str += f"{key}=%s,"
                keys_str = keys_str[:-1]      
                sql = f"UPDATE stocks SET {keys_str} WHERE pn = '{pn}';"  
                mycursor.execute(sql, val)
                mydb.commit()
                print("Change Done")
                return True
            except Exception as e:
                print(e)
        return False
    
    # return a dictionary
    def get_stock(self, pn):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            columns = self.get_column_names('stocks')
            sql = f"SELECT {','.join(columns)} from stocks WHERE pn = '{pn}'"
            mycursor.execute(sql)
            myresult = mycursor.fetchone()
            #print(myresult)
            if (myresult):
                result = dict(zip(columns, myresult))
                return result
        return None
    
    # return a dictionary
    def get_all_stock(self):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            columns = ['picture', 'pn', 'description', 'stock', 'package', 'edit']
            sql = f"SELECT {','.join(columns)} from stocks"
            mycursor.execute(sql)
            result = mycursor.fetchall()
            myresult = []
            for row in result:
                picture = row[columns.index('picture')]
                if (picture):
                    with open(f".\\tmp\\{row[columns.index('pn')].replace('/','_')}.jpg", 'wb') as file:
                        file.write(picture)
                myresult.append(row[1:])
            #print(myresult)
            if (myresult):
                myresult.insert(0, columns[1:])
                return myresult
        return None
    
    def delete_stock(self, pn):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = f"DELETE from stocks WHERE pn = '{pn}'"
            mycursor.execute(sql)
            mydb.commit()
            if (not self.get_stock(pn)):
                return True
        return False
    
    # -------------------------------------------------------
    # ----------------- ORDERS -----------------------------
    # -------------------------------------------------------     
    
    def get_orders_processing_columns(self):
        return ['order_number', 'state', 'orderConfig', 'extras', 'changes', 'test_bettair', 'test_final', 'afeSerial', 'boardSerials', 'cartridgeSerial', 'deviceSerial', 'sensorBoardId']
    
    
    def update_orders_database(self):
        api = api_restful.Api()
        columns = api.get_orders_processing_columns()
        types = ['VARCHAR(1024)'] * len(columns)
        keys = []
        for i in range(len(columns)):
            keys.append(f"{columns[i]} {types[i]}")
        #print(keys)
        mydb = self.connect()
        if (mydb):
            try:
                # Check for orders table
                print('Check for existing orders table')
                mycursor = mydb.cursor()
                sql = f"SHOW TABLES LIKE 'orders';"  
                mycursor.execute(sql, )
                result = mycursor.fetchall()
                #print(result)
                if result:
                    # Delete orders table
                    mycursor = mydb.cursor()
                    sql = f"DROP TABLE orders;"  
                    mycursor.execute(sql, )
                    mydb.commit()
                    print('Delete orders table')
                else:
                    print('Not exist orders table')
                # Create orders table
                sql = f"CREATE TABLE orders ({', '.join(keys)});"  
                mycursor.execute(sql, )
                mydb.commit()
                print("Table orders created")
                # Fill orders table
                orders = api.get_orders_processing()
                for order in orders:
                    sql = f"INSERT INTO orders ({', '.join(columns)}) VALUES ({('%s, '*len(order))[:-2]});" 
                    #print(sql)
                    mycursor.execute(sql, order)
                    mydb.commit()
            except Exception as e:
                print(e)
                
    def get_orders_processing(self):
        mydb = self.connect()
        if (mydb):
            try:
                # Check for orders table
                print('Check for existing orders table')
                mycursor = mydb.cursor()
                sql = f"SELECT * FROM orders;"  
                mycursor.execute(sql, )
                result = mycursor.fetchall()
                return result
            except Exception as e:
                print(e)
        return []
    
    def get_test_passed(self, sn_cartridge):
        tests = self.get_row_columns('test_bettair', sn_cartridge, ['test_traces', 'test_final'], result='dictionay')
        #print(tests)
        if tests:
            tests = tests[0]
            for key in tests:
                if tests[key]:
                    tests[key] = True
                else:
                    tests[key] = False
        else:
            tests = {'test_traces' : False, 'test_final' : False}
        return tests
    
    def get_test_changes_pcb(self, sn_cartridge):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = f"SELECT changes_pcb from test_bettair WHERE sn_cartridge = {sn_cartridge};"
            mycursor.execute(sql, )
            result = mycursor.fetchone()
            if result:
                return result[0]
        return ''
    
    def get_all_test_changes_pcb(self):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = f"SELECT sn_cartridge, changes_pcb from test_bettair;"
            mycursor.execute(sql, )
            result = mycursor.fetchall()
            if result:
                return dict(result)
        return {}
    
    def set_test_changes_pcb(self, changes_pcb, sn_cartridge):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = "UPDATE test_bettair SET changes_pcb = %s WHERE sn_cartridge = %s;"
            print(f"UPDATE changes_pcb of {sn_cartridge} to '{changes_pcb}'")
            #print(changes_pcb)
            val = (changes_pcb, sn_cartridge)
            mycursor.execute(sql, val)
            mydb.commit()
            return True
        return False
    
    def set_all_test_changes_pcb(self, changes_pcb, order):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = "UPDATE test_bettair SET changes_pcb = %s WHERE requisition = %s;"
            print(f"UPDATE changes_pcb of {order} to {changes_pcb}")
            #print(changes_pcb)
            val = (changes_pcb, order)
            mycursor.execute(sql, val)
            mydb.commit()
            return True
        return False
    
    def add_test_changes_pcb(self, changes_pcb, sn_cartridge):
        changes_pcb_actual = self.get_test_changes_pcb(sn_cartridge)
        if (self.set_test_changes_pcb(changes_pcb_actual + '\n' + changes_pcb, sn_cartridge)):
            return True
        return False
    
    def get_test_extras(self, sn_cartridge):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = f"SELECT extras from test_bettair WHERE sn_cartridge = {sn_cartridge};"
            mycursor.execute(sql, )
            result = mycursor.fetchone()
            if result:
                return result[0]
        return ''           
    
    def get_all_test_extras(self):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = f"SELECT sn_cartridge, extras from test_bettair;"
            mycursor.execute(sql, )
            result = mycursor.fetchall()
            if result:
                return dict(result)
        return {}
    
    def set_test_extras(self, extras, sn_cartridge):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = "UPDATE test_bettair SET extras = %s WHERE sn_cartridge = %s;"
            print(f"UPDATE extras of {sn_cartridge} to '{extras}'")
            val = (extras, sn_cartridge)
            mycursor.execute(sql, val)
            mydb.commit()
            return True
        return False
    
    def set_all_test_extras(self, extras, order):
        mydb = self.connect()
        if (mydb):
            mycursor = mydb.cursor()
            sql = "UPDATE test_bettair SET extras = %s WHERE requisition = %s;"
            print(f"UPDATE extras of {order} to {extras}")
            #print(changes_pcb)
            val = (extras, order)
            mycursor.execute(sql, val)
            mydb.commit()
            return True
        return False
    
    def add_test_extras(self, extras, sn_cartridge):
        extras_actual = self.get_test_extras(sn_cartridge)
        if (self.set_test_extras(extras_actual + '\n' + extras, sn_cartridge)):
            return True
        return False
    
    def debug_remove_sensors_from_sbox_1_2(self):
        sensors = self.get_all_sensors()[1:]
        #print(sensors)
        for sensor in sensors:
            #print(sensor[0])
            info = self.get_sensor_info((sensor[0]))
            if (info['afe']):
                print(f"SI {sensor[0]} afe {info['afe']}")
                self.set_sensor_location(sensor[0], 'OK')
            else:
                print(f'NO {sensor[0]}')
               
if __name__ == "__main__":
    db = SqlConnector()
    # column names
    #print(db.get_column_names('test_bettair'))
    #print(db.get_column_names('afes'))
    #print(db.get_column_names('sensors'))
    
    # Insert values in the tables
    #dic = {'gas' : 'NO2', 'sn' : '212120005', 'state' : 'NOT_TESTED'}
    #db.create_entry('sensors', dic)
    #dic = {'ae_zero_sn1' : '0.0', 'ae_zero_sn2' : '0.0', 'ae_zero_sn3' : '0.0', 'ae_zero_sn4' : '0.0', 'batch' : 'test', 'changes' : '', 'date_reception' : '2022-12-29 9:12:00', 'last_update' : '2022-12-29 9:12:00', 'location' : 'SBOX_4-3-9', 'observations' : 'New BAFE for Create Masters', 'pcb_gain_sn1' : '0', 'pcb_gain_sn2' : '0', 'pcb_gain_sn3' : '0', 'pcb_gain_sn4' : '0', 'pn' : '', 'sn' : '26-000009B', 'sn1' : '0212120564', 'sn2' : '0214120554', 'sn3' : '0130620018', 'sn4' : '0132620152', 'state' : 'NOT_TESTED', 'we_zero_sn1' : '0', 'we_zero_sn2' : '0', 'we_zero_sn3' : '0', 'we_zero_sn4' : '0', 'zero_cal_date' : '29/12/2023'}
    #db.create_entry('afes', dic)
    #dic = {'date_reception' : '2023-07-25', 'last_update' : '2023-07-25', 'location' : 'SBOX_4-8-0', 'pn' : '810-0023-01B', 'sn' : '26-000003B'}
    #db.create_entry('afes', dic)
    
    # get row from table
    #print(db.get_row('afes', '25-001550'))
    #print(db.get_row('sensors', '212730222'))
    
    # change or add a value of a row
    #db.insert_field('sensors', 'sn', '212800202', 'sn', '214800202')
    #db.insert_field('sensors', 'sn', '212730222', 'ae_zero', '-2')
    #db.insert_field('afes', 'sn', '25-001550', 'ae_zero_sn1', '281')
    #db.insert_field('afes', 'sn', '25-001550', 'we_zero_sn1', '294')
    
    # import data from mongo using a CSV file
    #db.get_dic_id_to_sn()
    #print(db.get_sensors_from_csv())
    #print(db.get_afes_from_csv())
    
    # get all afes
    #afes = db.get_all_afes()
    #print(afes[0])
    #print(list(afes[0].keys()))
    #for afe in afes:
    #    print(list(afe.values()))
    #print(db.get_sensor_fabrication('214070622'))
    
    #stocks
    """
    stock = db.get_afe_stocks(grouped=True)
    for s in stock:
        for s2 in s:
            print(s2)
    print("-------------")
    """
    """
    stock = db.get_afe_stocks(grouped=True)
    print(stock)
    print(stock[0])
    print("-------------")
    print(stock[1])
    print("-------------")
    print(stock[2])
    """
    """
    stock = [['AFE4', 'NO2,O3,CO,SO2', 42], ['AFE2', '', 32], ['AFE4', 'NO2,O3,NO,CO', 27], ['AFE2', 'SO2,H2S', 25], ['AFE2', 'H2S,SO2', 23], ['AFE4', '', 9], ['AFE2', 'VOC,CO', 8], ['AFE2', 'CO,VOC', 3], ['AFE2', 'NO2,O3', 3], ['AFE2', 'VOC,H2S', 2], ['AFE2', 'H2S', 1], ['AFE4', 'HCL,NH3,HCL,H2S', 1]]
    result = stock
    for i in stock:
        l = i[1].split(',')
        l.reverse()
        print(i[1])
        l_str = ",".join(l)
        print(l_str)
    """    
        
    
    
    # Swap sensors position
    """
    afe = '26-000293'
    row = db.get_row('afes', afe)
    #print(row)
    sn1 = row['sn3']
    sn2 = row['sn4']
    db.insert_field('afes', 'sn', afe, 'sn3', sn2)
    db.insert_field('afes', 'sn', afe, 'sn4', sn1)
    #row = db.get_row('afes', '26-000274')
    #print(row)
    """
    
    # Test Entry new AFE from file
    """
    afes = [
    {
        "ae_zero_sn1": 288.0,
        "ae_zero_sn2": 287.0,
        "batch": "80212",
        "pcb_gain_sn1": 0.8,
        "pcb_gain_sn2": 0.8,
        "pn": "810-0021-03",
        "sensors": {
            "SN1": {
                "ae_zero": 17.0,
                "gas": "NO-A4",
                "sensitivity_mv": 0.382,
                "sensitivity_na": 0.478,
                "sn": "999999999",
                "we_zero": 16.0
            },
            "SN2": {
                "ae_zero": -8.0,
                "gas": "VOC-A4",
                "sensitivity_mv": 0.258,
                "sensitivity_na": 0.322,
                "sn": "999999998",
                "we_zero": 79.0
            }
        },
        "sn": "13-999999",
        "we_zero_sn1": 277.0,
        "we_zero_sn2": 281.0,
        "zero_cal_date": "03/03/2023"
    },
    {
        "ae_zero_sn1": 296.0,
        "ae_zero_sn2": 410.0,
        "ae_zero_sn3": 283.0,
        "ae_zero_sn4": 296.0,
        "batch": "73234",
        "pcb_gain_sn1": -0.73,
        "pcb_gain_sn2": -0.73,
        "pcb_gain_sn3": 0.8,
        "pcb_gain_sn4": 0.8,
        "pn": "810-0023-01",
        "sensors": {
            "SN1": {
                "ae_zero": 0.0,
                "gas": "NO2-A43F",
                "sensitivity_mv": 0.299,
                "sensitivity_na": -0.41,
                "sn": "999999997",
                "we_zero": 1.0
            },
            "SN2": {
                "ae_zero": 2.0,
                "gas": "OX-A431",
                "sensitivity_mv": 0.274,
                "sensitivity_na": -0.375,
                "sn": "999999996",
                "we_zero": 7.0
            },
            "SN3": {
                "ae_zero": 11.0,
                "gas": "NO-A4",
                "sensitivity_mv": 0.406,
                "sensitivity_na": 0.508,
                "sensitivity_no2_mv": 15,
                "sn": "999999995",
                "we_zero": 11.0
            },
            "SN4": {
                "ae_zero": -1.0,
                "gas": "CO-A4",
                "sensitivity_mv": 0.274,
                "sensitivity_na": 0.343,
                "sensitivity_no2_mv": 15,
                "sn": "999999994",
                "we_zero": 40.0
            }
        },
        "sn": "26-999999",
        "we_zero_sn1": 295.0,
        "we_zero_sn2": 404.0,
        "we_zero_sn3": 277.0,
        "we_zero_sn4": 274.0,
        "zero_cal_date": "23/09/2022"
    }
]
    
    db.create_entry_from_file(afes)
    print(db.msg)
    """
    
    # test repairs
    #d = {'repair2_filter': 'State', 'repair_afe': '26-999999', 'repair_state': 'OK', 'repair_status': 'fdsfdsf'}
    #db.set_afe_state(d)
    #print(db.msg)
    
    #d = {'repair2_filter': 'Location', 'repair_afe': '13-000196', 'repair_location': 'SBOX_2-1-17'}
    #db.set_afe_location(d)
    #print(db.msg)
    #print(db.get_afe_location('25-000706'))
    
    #d = {'repair2_filter': 'Sensors', 'repair_afe': '26-999999', 'repair_sens_serial_1': '212999999', 'repair_sens_type_1': 'NO2', 'repair_sens_serial_2': '214999998', 'repair_sens_type_2': 'O3', 'repair_sens_serial_3': '130999997', 'repair_sens_type_3': 'NO', 'repair_sens_serial_4': '132999996', 'repair_sens_type_4': 'CO'}
    #db.set_afe_sensors(d)
    #print(db.msg)
      
    # Test anomaly
    #print(db.get_afes_tested_list())
    #db.import_local_anomaly_tests()
    #for i in range(10):
    #    print(db.get_afe_sensors('26-000317'))
    
    #afes_list = ['26-000354', '26-000366', '25-000702']
    #afes_sensors = [db.get_afe_sensors(afe) for afe in afes_list]
    #print(afes_sensors)
    #afes_sensors_json = json.dumps(afes_sensors)
    #print(afes_sensors_json)
    
    # Add changes every time modify AFE or Sensor or make a test
    #db.add_change('Nou canvi pero mes llarg', '13-999999')
    #print(db.msg)
    
    # Getting afe knowing the sensor
    #print(db.get_afe_from_sensor('212610320'))
    # columns = ['sn', 'gas', 'manufacturer', 'date_reception', 'last_update', 'observations', 'changes']
    #table = db.get_all_sensors('sn', True)
    #for i,t in enumerate(table):
    #    print(f"({i}) {t}")
    #print(table[0])
    
    # Celan Database
    #db.clean_zero_from_sn_sensors()
    #db.clean_zero_from_sn_afes()
    #db.clean_gas_assigned()
    
    #print(db.get_sensor_info('132250549'))
    
    """
    afes_list = ["25-000711", "26-000504", "25-000702", "26-000431", "26-000263"]
    date_start = '2023-05-30 12:22:16'
    for afe in afes_list:
        print(f"-- {afe}")
        sensors_dic = db.get_afe_sensors(afe)
        print(sensors_dic)
        for s in sensors_dic.keys():
            #print(s)
            if s != 'sn':
                if sensors_dic[s]:
                    print(sensors_dic[s])
                    #self.add_change(f"Anomaly Test Done with afe {afe}", sensors_dic[s], date_start) 
    """
    
    #print(db.get_afes_sbox('SBOX_4-1'))
    
    #print(db.get_afe_state('18-000190'))
    """
    with open('C:\\Users\\Daniel Gaspar\\Downloads\\node-ac-bom.csv', 'r') as file:
        for line in file:
            try:
                l = line.replace('"','').split(",")
                pn = l[1]
                description = l[2]
                stock = "0"
                package_list = []
                picture = None
                db.insert_stock(pn, description, stock, picture, package_list)
            except Exception as e:
                print(l)
                print(e)
    """        
    """
    #filename = "c:\\Users\Daniel Gaspar\\Downloads\\Pulse-W6113B0100-image.jpg"
    filename = "c:\\Users\Daniel Gaspar\\Downloads\\ws_0001.jpg"
    with open(filename, 'rb') as file:
        picture = file.read()
    pn = "ws_0001"
    #description = "Antenna FPC 4.2dBi Gain 960MHz/1510.9MHz/1610MHz/2200MHz/2700MHz/3600MHz Bag"
    stock = "339"
    package_list = []
    dic = {'pn' : pn, 'picture' : picture, 'stock' : stock, 'last_update' : datetime.datetime.now()}
    db.update_stock(dic)
    
    #row = db.get_stock('ws_0001')
    #with open(filename_copy, 'wb') as file:
    #    file.write(row['picture'])
    """
    
    #print(db.get_all_stock())
    
    #print(db.delete_stock('BET2001_SENSORS_C'))
    #print(db.get_sensor_fabrication('130630558'))
    #sensors = db.get_all_sensors(column_sort = '', reverse_in = False)
    #db.insert_field('sensors', 'sn', '75190056', 'state', 'NOT_TESTED')
        
    #sens_txt = """ 0130570012  0130570015  0130570014  0130780811  0130190138  0133170508  0130600208  
    #                0214040446  0214040428  0130650056  0130780801  0130190133  0132731014  0132620116  
    #                0132620137  0132620145  0132620122  0132620124  0132620138  132620114  0130620027  
    #                0130620033  0217570004  75190046    75190042    75190050    75190049    75190045    
    #                0217270122  0130620050  0133170535  0133170560  0133170545  0133170539  0133170522  0133170532  0133120012  0133120010  
    #                0100120427  0100120422  0133540059  0133540064  0212820609  0217570006  0217570008  0133540057  0214040348  0100120423  
    #                0133120042  0133120005  0133120004  0133170549  0133120028  0133170551  0133120011  0133120025  0133170553  0133120027  """
    """
    sens_list = sens_txt.replace(" 0"," ").split()
    #print(sens_list)
    sens_list = [int(i) for i in sens_list]
    sens_list = sorted(sens_list)
    #print(sens_list)
    for sn in sens_list:
        print(sn)
    #    db.insert_field('sensors', 'sn', sn, 'state', 'SBOX_1_2')
    
    i = 1
    for sn in sens_list:
        row = db.get_row('sensors', sn)
        if (row):
            print(f"{i} -- {row['sn']} {row['state']}")
            #print(f"{i} -- {sn}")
        else:
            print(f"{i} -- {sn} NOT FOUND")
            gas = db.get_sensor_type(sn)
            dic = {'gas' : gas, 'sn' : sn, 'state' : 'SBOX_1_2'}
            print(dic)
            #db.create_entry('sensors', dic)
        i += 1
    """
    #db.insert_field('sensors', 'sn', '132620114', 'state', 'SBOX_1_2')
    
        
    #print(db.get_afe_sensors_type('26-000664'))
    #print(db.get_afe_sensors_type('14-001198'))
    
    #print(db.get_sensors_stock())
    
    #table = db.get_all_bettair_test()
    #for i in table:
    #    print(i[:-3])
    
    #db.get_node_extras('E26LG80')
    """
    afes = db.get_all_afes(include_outdated = False, include_assembled = True) 
    mydb = db.connect()
    for afe in afes:
        sn1 = afe['sn1'].split('-')[1].strip() if afe['sn1'] else ''
        sn2 = afe['sn2'].split('-')[1].strip() if afe['sn2'] else ''
        sn3 = afe['sn3'].split('-')[1].strip() if afe['sn3'] else ''
        sn4 = afe['sn4'].split('-')[1].strip() if afe['sn4'] else ''
        print(f"{afe['sn']} - {afe['location']} -{sn1}, {sn2}, {sn3}, {sn4}") 
        if sn1:
            sensor = db.get_sensor_location(mydb,sn1)
            print(sensor)
            if (sensor == 'SBOX_1_2'):
                print(f"Sensor {sn1} change to OK")
        if sn2:
            sensor = db.get_sensor_location(mydb,sn2)
            print(sensor)
            if (sensor == 'SBOX_1_2'):
                print(f"Sensor {sn2} change to OK")
        if sn3:
            sensor = db.get_sensor_location(mydb,sn3)
            print(sensor)
            if (sensor == 'SBOX_1_2'):
                print(f"Sensor {sn3} change to OK")
        if sn4:
            sensor = db.get_sensor_location(mydb,sn4)
            print(sensor)
            if (sensor == 'SBOX_1_2'):
                print(f"Sensor {sn4} change to OK")
    """
    """
    sensors = db.get_all_sensors()[1:]
    #print(sensors)

    for sensor in sensors:
        #print(sensor[7])
        info = db.get_sensor_info((sensor[7]))
        if (info['afe']):
            print(f"SI {sensor[7]} afe {info['afe']}")
            db.set_sensor_location(sensor[7], 'OK')
        else:
            print(f'NO {sensor[7]}')
    """
    
    #print(db.get_test_passed('7000566'))    
    #print(db.get_test_passed('7000534')) 
    #print(db.get_test_passed('7000579'))
    #print(db.get_test_passed('7000800'))
    
    #print(db.get_test_changes_pcb('7000566'))
    
    #db.update_orders_database()
    #db.get_orders_processing()
    
    #print(db.get_test_changes_pcb('7000602'))
    #print(db.get_test_extras('7000602'))
    
    """
    orders = db.get_orders_processing()
    print(orders)
    print("------------------------")
    orders_columns = db.get_orders_processing_columns()
    print(orders)
    print("------------------------")
    cartridge_serial_index = orders_columns.index('cartridgeSerial')
    cartridge_serials = {i[cartridge_serial_index] for i in orders if i[cartridge_serial_index]}
    #print(cartridge_serials)
    changes_pcb = {i:db.get_test_changes_pcb(i) for i in cartridge_serials if i}
    extras = {i:db.get_test_extras(i) for i in cartridge_serials if i}
    print(changes_pcb)
    print("------------------------")
    print(extras)
    print("------------------------")
    """
    #print(db.get_all_test_changes_pcb())
    #db.debug_remove_sensors_from_sbox_1_2()