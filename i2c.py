
import struct
import os
import subprocess
import time
import crcmod

MAX_WAIT = 10

DEVICE = '61'
SPEED48 = '48000'
SPEED400 = '400000'

# SDC30 commands
CMD_START = '00,10,00,00'
CMD_STOP = '01,04'
CMD_INTERVAL = '46,00'
CMD_DATA_READY = '02,02'
CMD_READ_MEASUREMENT = '03,00'
CMD_FIRMWARE = 'd1,00'
CMD_RESET = 'd3,04'
CMD_FRC = '52,04'
CMD_ASC = '53,06'
CMD_COMPENSATE_TEMP = '54,03'

# SHT85 commands
DEVICE_SHT85 = '44'
CMD_SHT85_STATUS = 'F3,2D'
CMD_SHT85_CLEAR_STATUS = '30,41'
CMD_SHT85_MEASURE_HIGH = '24,00'
CMD_SHT85_MEASURE_MEDIUM = '24,0B'
CMD_SHT85_MEASURE_LOW = '24,16'

def init():
    if os.name == 'nt':
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW

def le_hex_to_co2_ppm(le_hex):
    ba = bytearray.fromhex(le_hex) # Conversion string -> bytearray
    ba.reverse() # Conversion big endian -> little endian
    tup = struct.unpack("f", ba) # Conversion 4 octets -> float
    return tup[0]

def get_crc8(txt):
        crc8 = crcmod.mkCrcFun(0x131, rev=False, initCrc=0xFF, xorOut=0x00)
        tmp = str(hex(crc8(bytes.fromhex(txt))))
        return tmp.replace("0x","")

def is_co2_test_available():
    s = subprocess.run([r".\MCP2221CLI\MCP2221CLI.exe", "-devices"], 
                        capture_output=True, shell=True)
    #print(str(s.stdout.decode()))
    #print(str(s.stdout))
    time.sleep(0.003)
    if (not "No devices connected" in str(s.stdout)):
        return True
    return False  

def _write_i2c_generic(cmd, device):
    l_cmd = cmd.split(",")
    crc = ""
    if len(l_cmd) >= 4:
        t_cmd = "".join(l_cmd[-2:])
        crc = "," + get_crc8(t_cmd)
    #print(f"MCP2221CLI.exe -i2cW={cmd + crc} -slave7={device}")
    s = subprocess.run([r".\MCP2221CLI\MCP2221CLI.exe ", f"-i2cW={cmd + crc} ", f"-slave7={device}", f"-speed={SPEED48}"], 
        capture_output=True, shell=True)
    #print(str(s.stdout.decode()))
    time.sleep(0.003)
    if ("I2C Write Successful." in str(s.stdout)):
        print("Write ok")
        return True
    #print(str(s.stdout.decode()))
    #print("Fail writing i2c")
    return False

def _write_i2c(cmd):
    return _write_i2c_generic(cmd, DEVICE)

def _write_i2c_sht85(cmd):
    return _write_i2c_generic(cmd, DEVICE_SHT85)

def _read_i2c_generic(nbytes, device):
    s = subprocess.run([r".\MCP2221CLI\MCP2221CLI.exe", f"-i2cr={nbytes}", f"-slave7={device}", f"-speed={SPEED48}"], 
                        capture_output=True, shell=True)
    #print(str(s.stdout.decode()))
    time.sleep(0.003)
    if (f"I2C Read {nbytes} bytes" in str(s.stdout)):
        return s.stdout.decode().split("\r\n")[-2].strip().replace(' ','').replace('0x','').split(',')
    return []  

def _read_i2c(nbytes):
    return _read_i2c_generic(nbytes, DEVICE)
    
def _read_i2c_sht85(nbytes):
    return _read_i2c_generic(nbytes, DEVICE_SHT85)

def soft_reset():
    print("\n* Reset")
    return _write_i2c(CMD_RESET)

def trigger_continuos_measurement():
    print("\n* Start Measurement")
    return _write_i2c(CMD_START)

def stop_continuos_measurement():
    print("\n* Stop Measurement")
    return _write_i2c(CMD_STOP)

def set_interval(interval):
    print(f"\n* Set interval to value {str(hex(interval))}")
    str_interval = str(hex(interval)).replace('0x','')
    if len(str_interval) < 2:
        str_interval = "0"+str_interval
    return _write_i2c(CMD_INTERVAL + ',00,' + str_interval)

def get_interval():
    print("\n* Get interval")
    if (_write_i2c(CMD_INTERVAL)):
        return _read_i2c(3)
        
def is_data_ready():
    print(f"\n* Data ready")
    if (_write_i2c(CMD_DATA_READY)):
        time.sleep(0.003)
        s = _read_i2c(3)
        if (s and s[1] == '1'):
            return True
    #print(s)
    print("NOT Ready !!!")
    return False

def read_firmware():
    print(f"\n* Read Firmware")
    if (_write_i2c(CMD_FIRMWARE)):
        #time.sleep(0.004)
        return _read_i2c(3)

def read_co2_temp_hum():
    print(f"\n* Get Measurement")
    if (_write_i2c(CMD_READ_MEASUREMENT)):
        #time.sleep(0.004)
        return _read_i2c(18)

def read_ASC():
    print(f"\n* Read ASC status")
    if (_write_i2c(CMD_ASC)):
        #time.sleep(0.004)
        return _read_i2c(3)

def write_ASC(enable):
    str_txt = "Enable" if enable else "Disable"
    print(f"\n* {str_txt} Automatic self calibration Mode")
    str_enable = "01" if enable else "00"
    return _write_i2c(CMD_ASC + ',00,' + str_enable)

def read_FRC():
    print(f"\n* Read FRC status")
    if (_write_i2c(CMD_FRC)):
        #time.sleep(0.004)
        return _read_i2c(3)

def write_FRC(co2):
    print(f"\n* Force Recalibration value to {str(hex(int(co2)))} ppm")
    str_co2 = str(hex(int(co2))).replace('0x','')
    #print(str_co2)
    if co2 == 0:
        str_co2 = "0000"
    elif (len(str_co2) % 2) != 0:
        str_co2 = "0"+str_co2
    #print(str_co2)
    return _write_i2c(CMD_FRC + ',' +str_co2[0:2] + ',' + str_co2[2:4])


def convert_co2(l_i2c_orig):
    #l_i2c_orig = i2c_response.strip(' ').replace('0x','').split(', ')
    l_i2c = [i if len(i) >= 2 else "0"+i for i in l_i2c_orig]
    #print(l_i2c)
    co2 = le_hex_to_co2_ppm("".join(l_i2c[0:2])+"".join(l_i2c[3:5]))
    if get_crc8("".join(l_i2c[0:2])) == l_i2c_orig[2] and get_crc8("".join(l_i2c[3:5])) == l_i2c_orig[5]:
        return co2
    print("FAIL READING CO2")
    print(f"{get_crc8(''.join(l_i2c[0:2]))} --> {l_i2c_orig[2]} - {get_crc8(''.join(l_i2c[3:5]))} --> {l_i2c_orig[5]}")
    print()
    return ""

def convert_temp(l_i2c_orig):
    #l_i2c_orig = i2c_response.strip(' ').replace('0x','').split(', ')
    l_i2c = [i if len(i) >= 2 else "0"+i for i in l_i2c_orig]
    #print(l_i2c)
    temp = le_hex_to_co2_ppm("".join(l_i2c[6:8])+"".join(l_i2c[9:11]))
    if get_crc8("".join(l_i2c[6:8])) == l_i2c_orig[8] and get_crc8("".join(l_i2c[9:11])) == l_i2c_orig[11]:
        return temp
    print("FAIL READING TEMPERATURE")
    print(f"{get_crc8(''.join(l_i2c[6:8]))} --> {l_i2c_orig[8]} - {get_crc8(''.join(l_i2c[9:11]))} --> {l_i2c_orig[11]}")
    return ""

def convert_hum(l_i2c_orig):
    #l_i2c_orig = i2c_response.strip(' ').replace('0x','').split(', ')
    l_i2c = [i if len(i) >= 2 else "0"+i for i in l_i2c_orig]
    #print(l_i2c)
    hum = le_hex_to_co2_ppm("".join(l_i2c[12:14])+"".join(l_i2c[15:17]))
    if get_crc8("".join(l_i2c[12:14])) == l_i2c_orig[14] and get_crc8("".join(l_i2c[15:17])) == l_i2c_orig[17]:
        return hum
    print("FAIL READING HUMIDITY")
    print(f"{get_crc8(''.join(l_i2c[12:14]))} --> {l_i2c_orig[14]} - {get_crc8(''.join(l_i2c[15:17]))} --> {l_i2c_orig[17]}")
    return ""

def convert_temp_sht85(l_i2c_orig):
    l_i2c = [i if len(i) >= 2 else "0"+i for i in l_i2c_orig]
    txt = ''.join(l_i2c[0:2])
    temp = -45+(175*(int(txt,16)/(65535)))
    if get_crc8("".join(l_i2c[0:2])) == l_i2c_orig[2]:
        return round(temp,2)
    print("FAIL READING TEMPERATURE SHT85")
    #print(f"{get_crc8(''.join(l_i2c[0:2]))} --> {l_i2c_orig[2]} - {get_crc8(''.join(l_i2c[3:5]))} --> {l_i2c_orig[5]}")
    return ""

def convert_hum_sht85(l_i2c_orig):
    l_i2c = [i if len(i) >= 2 else "0"+i for i in l_i2c_orig]
    txt = ''.join(l_i2c[3:5])
    hum = 100*(int(txt,16)/(65535))
    if get_crc8("".join(l_i2c[3:5])) == l_i2c_orig[5]:
        return round(hum,2)
    print("FAIL READING HUMIDITY SHT85")
    #print(f"{get_crc8(''.join(l_i2c[0:2]))} --> {l_i2c_orig[2]} - {get_crc8(''.join(l_i2c[3:5]))} --> {l_i2c_orig[5]}")
    return ""

"""
>MCP2221CLI.exe -i2cw=01 -slave7=70 -speed=100000
Connected to device 0
I2C Write Successful.

>MCP2221CLI.exe -i2cr=1 -slave7=70 -speed=50000
Connected to device 0
I2C Read 1 bytes, Slave = 0xe1 
    0x1    
"""

def select_i2c_channel(channel):
    ch_bin = 1 << (channel-1)
    #print(f"{channel} ----- {ch_bin} ---- {hex(ch_bin)} ---- {str(hex(ch_bin)).replace('0x','')}")
    #print(f"MCP2221CLI.exe -i2cW={str(hex(ch_bin)).replace('0x','')} -slave7=70")
    s = subprocess.run([r".\MCP2221CLI\MCP2221CLI.exe", f"-i2cW={str(hex(ch_bin)).replace('0x','')}", f"-slave7=70", f"-speed={SPEED400}"], 
        capture_output=True, shell=True)
    #print(s.stdout.decode())
    if "Successful" in str(s.stdout):
        time.sleep(0.01)
        s = subprocess.run([r".\MCP2221CLI\MCP2221CLI", f"-i2cr=1", f"-slave7=70", f"-speed={SPEED400}"], 
            capture_output=True, shell=True)
        #print(s.stdout.decode())
        #print(f" 0x{ch_bin} == {s.stdout.decode()}")
        if f"{hex(ch_bin)}" in str(s.stdout.decode()):
            print(f"Channel {channel} Selected")
            return True
    
    print(f"Fail selecting i2c channel {channel} from hub")
    return False

def get_temp_hum_average():
    temp = []
    hum = []
    for i in range(8):
        if (i != 4):
            if (select_i2c_channel(i+1)):
                _write_i2c_sht85(CMD_SHT85_MEASURE_HIGH)
                time.sleep(0.1)
                resp = _read_i2c_sht85(6)
                #print(resp)
                if (resp):
                    temp.append(convert_temp_sht85(resp))
                    hum.append(convert_hum_sht85(resp))
                #time.sleep(0.1)
    
    if temp and hum:
        return [round(sum(temp)/len(temp),2), round(sum(hum)/len(hum),2)]
    print("No data received for eval temperature and humidity")
    return []

def set_temperature_offset(offset):
    print(f"\n* Set temperature offset to {offset}")
    val_str = str(hex(int(offset*100))).replace('0x','')
    if (len(val_str) == 1):
        offset_str = f'00,0{val_str}'
    elif (len(val_str) == 2):
        offset_str = f'00,{val_str}'
    elif (len(val_str) == 3):
        offset_str = f'0{val_str[0]},{val_str[1:3]}'
    elif (len(val_str) == 4):
        offset_str = f'{val_str[0:2]},{val_str[2:4]}'
    else:
        offset_str = ''
    #print(CMD_COMPENSATE_TEMP + ',' + offset_str)
    if (_write_i2c(CMD_COMPENSATE_TEMP + ',' + offset_str)):
        #print("done")
        return True

def read_temperature_offset():
    print(f"\n* Reading temperature offset")
    if (_write_i2c(CMD_COMPENSATE_TEMP)):
        return _read_i2c(3)
    
    return ''



